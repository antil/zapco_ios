//
//  FreeHistoryData.swift
//  ZAPCO
//
//  Created by vinove on 28/03/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import Foundation

class FreeHistoryData: NSObject
{
    var name = NSString()
    var Id = NSNumber()
    var image = NSString()
    var category = NSString()
    var reqList = NSDictionary()
    var latitude = NSNumber()
    var longitude = NSNumber()
    var dateTime = NSString()
    var time = NSString()
    var task = NSString()
    var amount = NSNumber()
    var comments = NSString()
    var paymentStatus = NSString()
    var rating = NSString()
    var status = NSNumber()
    var reuestId = NSNumber()
}
