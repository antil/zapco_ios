//
//  CustomerRequest.swift
//  ZAPCO
//
//  Created by vinove on 1/4/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class CustomerRequest: NSObject {
    
    var requestDict = NSDictionary()
    var CatID = NSInteger()
    var customerReviewList = NSArray()
    var distance = NSString()
    var licenseNumber = NSString()
    var name = NSString()
    var phone = NSString()
    var imageStr = NSString()
    var rating = NSInteger()
    var subCat = NSInteger()
    var techID = NSInteger()
    var techType = NSString()
    var reqId = NSNumber()
     var userId = NSNumber()
    var lat = NSNumber()
    var long = NSNumber()
    var location = ["Latitude":28.980955, "Longitude":77.180955 ]
    
}
