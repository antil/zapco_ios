//
//  CustomerQuotesList.swift
//  ZAPCO
//
//  Created by Priyanka Antil on 5/24/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class CustomerQuotesList: NSObject {
    
    var location = String()
    var amount = NSNumber()
    var category = String()
    var dateTime = String()
    var descrip = String()
    var reqImage = String()
    var comment = String()
    var name = String()
    var offset = NSNumber()
    var requestId = NSNumber()
    var quotesDict = NSDictionary()
    var techID = NSNumber()
    var rating = NSNumber()
    var time = String()
}
