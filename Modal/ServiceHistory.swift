//
//  ServiceHistory.swift
//  ZAPCO
//
//  Created by vinove on 3/21/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class ServiceHistory: NSObject {
    
    var amount = NSString()
    var category = NSString()
    var comments = NSString()
    var dateTime = NSString()
    var time = NSString()
    var eta = NSString()
    var id = NSInteger()
    var image = NSString()
    var latitude = NSNumber()
    var longitude = NSNumber()
    var name = NSString()
    var rating = NSInteger()
    var reqID = NSInteger()
    var status = NSInteger()
    var task = NSString()
    var techId = NSInteger()

}
