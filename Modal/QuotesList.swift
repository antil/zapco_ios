//
//  QuotesList.swift
//  ZAPCO
//
//  Created by vinove on 12/12/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class QuotesList: NSObject {
    
    var address = NSString()
    var amount = NSNumber()
    var category = NSString()
    var email = NSString()
    var dateTime = NSString()
    var descrip = NSString()
    var reqImage = NSString()
    var limit = NSNumber()
    var name = NSString()
    var time = NSString()
    var offset = NSNumber()
    var requestId = NSNumber()
    var status = NSNumber()
    var task = NSString()
    var quotesDict = NSDictionary()
    var paymentStatus = NSString()

}
