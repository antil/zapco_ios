//
//  TechReqList.swift
//  ZAPCO
//
//  Created by vinove on 12/11/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class TechReqList: NSObject {
    
    var address = NSString()
    var amount = NSNumber()
    var category = NSString()
    var dateTime = NSString()
    var time = NSString()
    var descrip = NSString()
    var image = NSString()
    var limit = NSNumber()
    var id = NSInteger()
    var name = NSString()
    var offset = NSNumber()
    var requestId = NSNumber()
    var reqType = NSString()
    var status = NSNumber()
    var task = NSString()
    var latitude = NSNumber()
    var longitude = NSNumber()
    var reqDict = NSDictionary()
      
}
