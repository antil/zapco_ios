//
//  FreeProfile.swift
//  ZAPCO
//
//  Created by vinove on 28/03/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import Foundation

class FreeProfile : NSObject
{
    var name = NSString()
    var licenseNo = NSString()
    var status = NSString()
    var expertise = NSString()
    var serviceType = NSString()
    var email = NSString()
    var phoneNo = NSString()
    var city = NSString()
    var documents = NSArray()
    var data = NSDictionary()
}
