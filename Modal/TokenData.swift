//
//  TokenData.swift
//  ZAPCO
//
//  Created by vinove on 10/31/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class TokenData: NSObject {
    
    var tokenType = NSString()
    var accesstoken = NSString()
    var userID = NSString()
}
