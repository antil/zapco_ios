//
//  PaymentData.swift
//  ZAPCO
//
//  Created by Priyanka Antil on 4/30/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class PaymentData: NSObject
{
    var paymentDict = NSDictionary()
    var name = String()
    var techId = NSNumber()
    var image = String()
    var category = String()
    var reqList = NSDictionary()
   // var latitude = NSNumber()
    var location = String()
    var dateTime = String()
    var date = String()
    var time = String()
    var desc = String()
    var amount = NSNumber()
    var comments = String()
    var paymentStatus = String()
    var rating = NSNumber()
    var status = NSNumber()
    var reuestId = NSNumber()

}
