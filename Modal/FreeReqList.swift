//
//  FreeReqList.swift
//  ZAPCO
//
//  Created by Priyanka Antil on 5/29/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import Foundation

class FreeReqList: NSObject
{
    var name = NSString()
    var Id = NSNumber()
    var image = NSString()
    var category = NSString()
    var reqList = NSDictionary()
    var latitude = NSNumber()
    var longitude = NSNumber()
    var dateTime = NSString()
    var task = NSString()
    var amount = NSNumber()
    var comments = NSString()
    var paymentStatus = NSString()
    var rating = NSString()
    var status = NSNumber()
    var reuestId = NSNumber()
}
