//
//  NotificationData.swift
//  ZAPCO
//
//  Created by Priyanka Antil on 08/08/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import Foundation
class NotificationData: NSObject
{
    var notifyDict = NSDictionary()
    var notifyId = NSNumber()
    var image = String()
    var notifyTitle = String()
    var dateTime = String()
    var reqId = String()
    var status = NSNumber()
}
