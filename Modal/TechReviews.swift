//
//  TechReviews.swift
//  ZAPCO
//
//  Created by vinove on 1/4/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class TechReviews: NSObject {

    var comment =  NSString()
    var imageStr = NSString()
    var name = NSString()
    var rating = NSInteger()
    var techID = NSInteger()
}
