//
//  Shared.swift
//  Flight Load
//
//  Created by vinove on 9/19/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class Shared: NSObject {
    
    var token_Data : TokenData!
    var userDetails : Userdata!
    
    var tokenDict = [String:Any]()
    class var sharedInstance:Shared {
        struct Singleton {
            static let instance = Shared()
        }
        return Singleton.instance
}
}
