//
//  LocationManager.swift
//  Places
//
//  Created by vinove on 05/04/17.
//  Copyright © 2017 Razeware LLC. All rights reserved.
// 283371


import Foundation
import CoreLocation
protocol locationDelegates: NSObjectProtocol
{
    func updatedAddress(currentLocation: CLLocation)
}

class LocationManager:NSObject
{
    static let manager:LocationManager = LocationManager()
    let locationManager = CLLocationManager()
    var currentLocation:CLLocation = CLLocation()
    var currentAddress:String?
    var delegates: locationDelegates?
    
    override init() {
        super.init()
        self.setLocationManager()
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
    }
    
    func updateLocation(){
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
    }
    func updateLocationManually(controller: AppDelegate){
        
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
    }
    func updateHeading(){
        locationManager.startUpdatingHeading()
    }
    func stopUpdateHeading(){
        locationManager.stopUpdatingHeading()
    }
    
    func setLocationManager()  {
        locationManager.delegate = self
    }
}


extension LocationManager: CLLocationManagerDelegate {
    
    func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool {
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading){
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        self.currentLocation = locationObj
        var index: Int = 0
        //User.main.latitude = locationObj.coordinate.latitude
        //User.main.longitude = locationObj.coordinate.longitude
        CLGeocoder().reverseGeocodeLocation(locationObj) { (parkers, error) in
            if error == nil {
                print()
                
                let placemark = parkers![0]
                let address = "\(placemark.thoroughfare ?? ""),\(placemark.subLocality ?? ""), \(placemark.administrativeArea ?? ""),  \(placemark.locality ?? ""), \(placemark.postalCode ?? ""), \(placemark.country ?? "")"
                    print("\(address)")
            
                if placemark.locality != nil{
                    let city = placemark.locality
                    print(city!)
                    if city != nil{
                        UserDefaults.standard.set(city, forKey: "city")
                    }
                    else{
                        UserDefaults.standard.set("", forKey: "city")
                    }
                    UserDefaults.standard.synchronize()
                }
                
                if placemark.postalCode != nil{
                    let zipcode = placemark.postalCode
                    print(zipcode!)
                    if zipcode != nil{
                        UserDefaults.standard.set(zipcode, forKey: "zipcode")
                    }
                    else{
                        UserDefaults.standard.set("", forKey: "zipcode")
                    }
                    UserDefaults.standard.synchronize()
                }
                
                let addressArray =  (parkers![0] as CLPlacemark).addressDictionary!["FormattedAddressLines"]! as! Array<String>
                self.currentAddress = addressArray.joined(separator: ",")
                print(self.currentAddress!)
           
                index += 1
                
                UserDefaults.standard.set(self.currentAddress! + "\(index)", forKey: "address")
                
                UserDefaults.standard.synchronize()
                self.delegates?.updatedAddress(currentLocation: self.currentLocation)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
           manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:

            break
        default:
            break
        }
    }
    

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print(error.localizedDescription)
    }
}
