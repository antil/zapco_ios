//
//  Constant.swift
//  HeartNRib
//
//  Created by vinove software on 22/12/16.
//  Copyright © 2016 vinove. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Defining URLs
//Base URL: https://180.151.232.92:124
//CommonAPI URL : http://180.151.232.92:124/api/account/Relative URL
//Company Technician API URL :http://180.151.232.92:124/api/companyTechnician/Relative URL
//http://180.151.232.92:124/api/customer/
//http://180.151.232.92:124/api/account/forgotPassword

var requestIdForPayment:String = "000"
var ammountForPayment:String = "0"
var transactionId:String = "0"
var transactionTime:String = "00000"

let merchantKey:String = "7rnFly"
let merchantSalt:String = "pjVQAWpA"

struct ServiceUrl
{
    static let baseUrl = "http://180.151.232.92:124"
    static let CommonUrl = "http://180.151.232.92:124/api/account/"
    static let customerUrl = "http://180.151.232.92:124/api/customer/"
    static let companyTechnicianUrl = "http://180.151.232.92:124/api/companyTechnician/"
    static let signUpURL = "http://180.151.232.92:124/api/account/"
    static let freelanceUrl = "http://180.151.232.92:124/api/freelancer/"
}

struct stringConstants
{
    static let userData = "USER_DATA"
    static let tokenData = "TOKEN_DATA"
    
}

class URLManager
{
    static let manager = URLManager()
    private init() {}
    
    let baseURL = "http://10.0.1.53/api/"
    func loginURL() -> String {
        return "\(baseURL)users"
    }
    
}


//MARK: - Progress Indicator
class ProgressHud
{
    static let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    static let loadingView: UIView = UIView()
    
    static  func progressBarDisplayer(indicator:Bool,view : UIView )
    {
        if indicator == true {
            let x = (UIApplication.shared.keyWindow?.frame.size.width)!/2 - 40
            let y = (UIApplication.shared.keyWindow?.frame.size.height)!/2 - 40
            loadingView.frame = CGRect(x : x,y :  y,width :  80,height :  80)
            loadingView.backgroundColor = UIColor.black
            loadingView.alpha = 0.7
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            actInd.frame = CGRect(x : 0.0,y: 0.0,width : 40.0,height : 40.0);
            actInd.activityIndicatorViewStyle =
                UIActivityIndicatorViewStyle.whiteLarge
            actInd.center = CGPoint(x :loadingView.frame.size.width / 2,
                                    y : loadingView.frame.size.height / 2);
            //actInd.color = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
            loadingView.addSubview(actInd)
            view.addSubview(loadingView)
            loadingView.isHidden = false
            actInd.startAnimating()
            view.isUserInteractionEnabled = false
        }
        else{
            view.isUserInteractionEnabled = true
            actInd.stopAnimating()
            loadingView.removeFromSuperview()
   
        }
        
    }

}


//MARK: - DateTimeMethods

struct DateTimeConversion
{
   static func giveTime(time:String) -> String
   {
    var str = time
    if let dotRange = str.range(of: ".") {
        str.removeSubrange(dotRange.lowerBound..<str.endIndex)
    }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm a"
        // let date = Date(timeIntervalSince1970: TimeInterval(time)!)
        let strStartdate: Date? = dateFormatterGet.date(from: String(describing:str))
        let dateStart = timeFormatter.string(from: strStartdate!)
        return dateStart
    }
    
  static  func dateConvert(date:String)->String
  {
    var str = date
    if let dotRange = str.range(of: ".") {
        str.removeSubrange(dotRange.lowerBound..<str.endIndex)
    }
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "MMM dd,yyyy"
    //let date1 = Date(timeIntervalSince1970: TimeInterval(date)!)
    let strStartdate: Date? = dateFormatterGet.date(from:String(describing: str))
    let dateStart = dateFormatterPrint.string(from: strStartdate!)
    return dateStart
    }
}

struct ShowTime
{
    static func dateConvert(date:String)->String {
        var str = date
        if let dotRange = str.range(of: ".") {
            str.removeSubrange(dotRange.lowerBound..<str.endIndex)
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        let strStartdate: Date? = dateFormatterGet.date(from:String(describing: str))
        let dateStart = dateFormatterPrint.string(from: strStartdate!)
        return dateStart
    }
   

    static func timeConvert(date:String)->String
    {
        var str = date
        if let dotRange = str.range(of: ".") {
            str.removeSubrange(dotRange.lowerBound..<str.endIndex)
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        dateFormatterGet.timeZone = NSTimeZone.local
        let dateFormatterPrint = DateFormatter()
        //dateFormatterPrint.dateFormat = "HH:mm"
        dateFormatterPrint.dateFormat = "h:mm a"
        let strStartdate: Date? = dateFormatterGet.date(from:str)
        let dateStart = dateFormatterPrint.string(from: strStartdate!)
        return dateStart
    }
    
    static func showTime(time:String)->String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "HH:mm:ss"
        
        dateFormatterGet.timeZone = NSTimeZone.local
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "h:mm a"
        let strStartTime: Date? = dateFormatterGet.date(from:time)
        let timeStart = dateFormatterPrint.string(from: strStartTime!)
        return timeStart
    }
}



//MARK: - ChangingDesign
struct Shadow
{
    static func viewShadow(view:UIView)
    {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
        view.layer.shadowRadius = 2.0
        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
        view.layer.shouldRasterize = true
    }
    
    static func textfieldShadow(textfield: UITextField)
    {
        textfield.layer .masksToBounds = true
        textfield.layer.borderColor = UIColor.init(red: 241.0/255.0, green: 241.0/255.0, blue: 241.0/255.0, alpha: 1.0).cgColor
        textfield.layer.borderWidth = 1.5
    }
    
    static func textShadow(view: UIView)
    {
        view.layer.masksToBounds = false
        view.layer.shadowRadius = 3.0
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        view.layer.shadowOpacity = 0.5
    }
    
    static func buttonShadow(view: UIButton)
    {
        view.layer.masksToBounds = false
        view.layer.shadowRadius = 3.0
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        view.layer.shadowOpacity = 0.5
    }
}

struct btnCornerChange
{
    static func changeCorner(button:UIButton)
    {
        let rectShape = CAShapeLayer()
        rectShape.bounds = button.frame
        rectShape.position = button.center
        rectShape.path = UIBezierPath(roundedRect: button.bounds, byRoundingCorners: [.bottomLeft , .topLeft], cornerRadii: CGSize(width: 20, height: 20)).cgPath
        button.layer.mask = rectShape
    }
    
   static func changeCorner2(button:UIButton)
   {
        let rectShape = CAShapeLayer()
        rectShape.bounds = button.frame
        rectShape.position = button.center
        rectShape.path = UIBezierPath(roundedRect: button.bounds, byRoundingCorners: [.bottomRight , .topRight], cornerRadii: CGSize(width: 20, height: 20)).cgPath
        button.layer.mask = rectShape
    }
}

struct TextfieldImage
{
    static func textfieldImageRight(txt : UITextField) -> UITextField
    {
        let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 17, height: 17))
        let image = UIImage(named: "dropdown")
        imgV.image = image
        txt.rightViewMode = UITextFieldViewMode.always
        txt.rightView = imgV
        return txt
    }
    
}


struct AlertControl
{
    
    static func alert(appmassage:String,view:UIViewController){
        
        let alert = UIAlertController(title: "ZAPCO", message: appmassage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
}




public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}








