//
//  customButton.swift
//  ZAPCO
//
//  Created by vinove on 9/13/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit


@IBDesignable
class customButton: UIButton {

    
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var keyPath: CGFloat = 0 {
        didSet {
           // layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var normalBorderColor: UIColor? {
        didSet {
            self.layer.borderColor = normalBorderColor?.cgColor
        }
    }

    @IBInspectable var normalBackgroundColor: UIColor? {
        didSet {
            self.layer.borderColor = normalBackgroundColor?.cgColor
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = layer.frame.height / 2
        clipsToBounds = true
        
     }

   

}
