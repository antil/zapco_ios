//
//  CustomView.swift
//  ZAPCO
//
//  Created by vinove on 9/13/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit


@IBDesignable
class CustomView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var normalBorderColor: UIColor? {
                didSet {
                    self.layer.borderColor = normalBorderColor?.cgColor
                }
            }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }

    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
    }



    override func draw(_ rect: CGRect) {
        
        self.layer.cornerRadius = rect.height/2
        self.layer.masksToBounds = true
        

    }
   
}
