//
//  TrackTechViewController.swift
//
//
//  Created by Priyanka Antil on 5/2/18.
//

import UIKit
import MapKit
import GoogleMaps


class TrackTechViewController: UIViewController, MKMapViewDelegate
{
    var lat = NSNumber()
    var long = NSNumber()
    var mapView = GMSMapView()
    
    //MARK: - ViewLifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let closeButton = UIButton.init(frame: CGRect(x: self.view.frame.size.width - 40, y: self.view.frame.origin.y + 50, width: 30, height: 30))
      //  let zoomButton = UIButton.init(frame: CGRect(x: self.view.frame.size.width - 40, y: self.view.frame.size.height - 100, width: 30, height: 30))

        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long), zoom: 15.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView

        closeButton.setImage(UIImage.init(named: "close_red"), for: .normal)
        closeButton.addTarget(self, action: #selector(btnClose(_:)), for: .touchUpInside)
        mapView.addSubview(closeButton)
        
//        zoomButton.setImage(UIImage.init(named: "direction"), for: .normal)
//        zoomButton.addTarget(self, action: #selector(tapZoom(_:)), for: .touchUpInside)
//        mapView.addSubview(zoomButton)
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude:CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
        marker.title = "Technician"
        marker.icon = UIImage.init(named: "location_marker")
        marker.map = mapView
        
        let marker2 = GMSMarker()
        marker2.position = CLLocationCoordinate2D(latitude: LocationManager.manager.currentLocation.coordinate.latitude
            , longitude: LocationManager.manager.currentLocation.coordinate.longitude)
        marker2.title = "MyLocation"
         marker2.icon = UIImage.init(named: "location_marker")
        marker2.map = mapView
        mapView.isMyLocationEnabled = true
        self.DrawPoly()
      
    }
    
//    @IBAction func tapZoom(_ sender: UIButton)
//    {
//        if sender.isSelected == true
//        {
//            sender.isSelected = false
//            mapView.animate(toZoom: 15.0)
//        }
//        else
//        {
//            sender.isSelected = true
//            mapView.animate(toZoom: 40.0)
//        }
//    }
    
    override func didReceiveMemoryWarning()  {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func DrawPoly()
    {
        let userCoordinates: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: CLLocationDegrees(LocationManager.manager.currentLocation.coordinate.latitude), longitude: CLLocationDegrees(LocationManager.manager.currentLocation.coordinate.longitude))
        
        let origin = "\(lat),\(long)"
        let destination = "\(userCoordinates.latitude),\(userCoordinates.longitude)"
        //let destination = "28.9134,77.0334"

        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyB8ln5VrlFR2gAhmxeyWzPu5XYkORcvJ7c"
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    let routes = json["routes"] as! NSArray
                
                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeWidth = 5
                            polyline.strokeColor = UIColor.red
                            
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                            
                            polyline.map = self.mapView
                            
                        }
                    })
                }catch let error as NSError{
                    print("error:\(error)")
                }
            }
        }).resume()
        
    }
    
    
   
    
    //MARK: - IBActions
    @IBAction func btnClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
