//
//  QuotesViewController.swift
//  ZAPCO
//
//  Created by vinove on 10/23/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Toast_Swift

class QuotesViewController: UIViewController,UITextFieldDelegate

{
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var tableView: UITableView!
    var arrShowQuotes = NSMutableArray()
    var style = ToastStyle()
    @IBOutlet weak var heightFilter: NSLayoutConstraint!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    var datePicker = UIDatePicker()
    var textfieldDate = UITextField()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.txtFrom.delegate = self
        self.txtTo.delegate = self
        Shadow.textShadow(view: self.viewTop)
        self.heightFilter.constant = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
     
        self.arrShowQuotes.removeAllObjects()
        self.getQuotes()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    
   
    //MARK: - Web Service Methods
    func getQuotes()
    {
        ///technicianQuotations
       //{ offset=30,limit=15, "startDate":"MM-dd-YYYY", "endDate":"MM-dd-YYYY"}
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        self.arrShowQuotes.removeAllObjects()
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(0, forKey: "offset")
        param.setValue(15, forKey: "limit")
        param.setValue(self.txtFrom.text!, forKey: "startDate")
        param.setValue(self.txtTo.text!, forKey: "endDate")
          
        let Url = ServiceUrl.companyTechnicianUrl + "technicianQuotations"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success)
            {
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                    if let arrData:NSArray = response["Data"] as? NSArray
                   {
                    for dict in arrData{
                        let dictData = dict as! NSDictionary
                        let techReq = QuotesList()
                        techReq.quotesDict = dictData
                        techReq.category = dictData.value(forKey: "Category") as! NSString
                        if let dt = dictData.value(forKey: "DateTime") as? NSString
                        {
                            techReq.dateTime = dt
                        }
                        if let time = dictData.value(forKey: "TimeLog") as? NSString
                        {
                            techReq.time = time
                        }
                        if let m = dictData.value(forKey: "Email") as? NSString{
                            techReq.email = m
                        }
                        if let m = dictData.value(forKey: "Amount") as? NSNumber{
                            techReq.amount = m
                        }
                        else
                        {
                            techReq.amount =  0
                        }
                        if let m = dictData.value(forKey: "PaymentStatus") as? NSString
                        {
                            techReq.paymentStatus = m
                        }
                        
                        if let add = dictData.value(forKey: "Description") as? NSString{
                            techReq.descrip = add
                        }
                        if let add = dictData.value(forKey: "Address") as? NSString{
                            techReq.address = add
                        }
                        if let img = dictData.value(forKey: "RequesterImagePath") as? NSString{
                            techReq.reqImage = img
                        }
                        else{
                            techReq.reqImage = ""
                        }
                        techReq.name = dictData.value(forKey: "Name") as! NSString
                        techReq.requestId = dictData.value(forKey: "RequestId") as! NSNumber
                        if let tas:NSString = dictData.value(forKey: "Task") as? NSString
                        {
                            techReq.task = tas
                        }
                        self.arrShowQuotes.add(techReq)
                    }}
                    
                    if self.arrShowQuotes.count == 0{
                        self.tableView.reloadData()
                        self.view.makeToast("There is No quotes to show!")
                        
                    }else{
                        
                       self.tableView.reloadData()
                    }
  
                }
                else if (response["Code"] as AnyObject).integerValue == 202
                {
                    self.view.makeToast("No Quotations exist between specified time period", duration: 0.5, position: .center, style: self.style)
                    
                }
                }
            else{
                AlertControl.alert(appmassage: "Try Again Later!", view: self)
            }
            
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
}
    
    
    //MARK: - UITextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        if textField == self.txtFrom{
            
            self.textfieldDate = self.txtFrom
            self.pickUpDate(self.textfieldDate)
            
        }
        else if textField == self.txtTo{
            self.textfieldDate = self.txtTo
            self.pickUpDate(self.textfieldDate)
            
        }
        
    }
    
    
    //MARK: - UIPickerviewMethods
    func pickUpDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.dateFormat = "MM-dd-yyyy"
        self.textfieldDate.text = dateFormatter1.string(from: datePicker.date)
        self.textfieldDate.resignFirstResponder()
    }
    func cancelClick()
    {
        self.textfieldDate.resignFirstResponder()
    }
   
    
    //MARK: - IBActions
     @IBAction func btnViewDetail(_ sender: UIButton)
     {
        let objects = self.arrShowQuotes.object(at: sender.tag) as! QuotesList
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "QuotesDetailsViewController") as! QuotesDetailsViewController
        controller.quotesDetails = objects.quotesDict
        self.navigationController?.pushViewController(controller, animated: true)
    
    }
    
    @IBAction func btnAddClick(_ sender: UIButton)
    {
        let objects = self.arrShowQuotes.object(at: sender.tag) as! QuotesList
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddQuotesViewController") as! AddQuotesViewController
        controller.strReqN0 = String(describing: objects.requestId)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
  
    @IBAction func btnReqClick(_ sender: Any)
    {
        let searchResult = self.storyboard?.instantiateViewController(withIdentifier: "RequestListViewController") as! RequestListViewController
        self.navigationController?.pushViewController(searchResult, animated: true)
        
    }
    
    @IBAction func btnAccClick(_ sender: Any)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnFilter(_ sender: Any)
    {
        
         self.heightFilter.constant = 94.0
    }
    
    @IBAction func btnFilterApply(_ sender: Any)
    {
        self.heightFilter.constant = 0.0
        self.getQuotes()
        self.tableView.reloadData()
    }

}


extension QuotesViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrShowQuotes.count > 0
        {
            return self.arrShowQuotes.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuotesTableViewCell") as! QuotesTableViewCell
        if(arrShowQuotes.count > 0 && arrShowQuotes.count > indexPath.row){

        let objects = self.arrShowQuotes[indexPath.row] as! QuotesList
        
        cell.lblName.text = objects.name as String
        cell.lblCategory.text = objects.category as String
        cell.lblRequstID.text = "Req Id: " + String(describing: objects.requestId)
        //cell.lblAmount.text =  String(describing: objects.amount)
        
        let date = ShowTime.dateConvert(date:objects.dateTime as String)
        cell.lblDate.text = date as String
        if objects.time != ""{
            cell.lblTime.text = ShowTime.showTime(time:objects.time as String)
        }
       
        
        if objects.reqImage != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.reqImage as String)
            if profileUrl != nil
            {
                cell.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.imgUser.image = placeholderImage
            }
            
        }
        
        cell.btnView.tag = indexPath.item
        cell.btnView.addTarget(self, action: #selector(btnViewDetail(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //
    //        let objects = self.arrShowQuotes.object(at: indexPath.row) as! QuotesList
    //        let controller = self.storyboard?.instantiateViewController(withIdentifier: "QuotesDetailsViewController") as! QuotesDetailsViewController
    //        controller.quotesDetails = objects.quotesDict
    //        self.navigationController?.pushViewController(controller, animated: true)
    //    }
    
}
