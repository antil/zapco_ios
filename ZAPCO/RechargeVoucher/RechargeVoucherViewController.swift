//
//  RechargeVoucherViewController.swift
//  ZAPCO
//
//  Created by vinove on 12/26/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class RechargeVoucherViewController: UIViewController, UITextFieldDelegate
{
    
    //MARK: - Variables
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var txtEnterCode: UITextField!
    @IBOutlet weak var viewVoucher: UIView!
    
    //MARK: - ViewLiifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Shadow.textShadow(view: self.viewVoucher)
        Shadow.textShadow(view: self.viewTop)
        txtEnterCode.delegate = self
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool){
        txtEnterCode.text = ""
 
        if let bal:String = UserDefaults.standard.value(forKey: "balanceServices") as? String
        {
            if bal == "0"
            {
                let tabBarControllerItems = self.tabBarController?.tabBar.items
                if let tabArray = tabBarControllerItems {
                    let tabBarItem1: UITabBarItem = tabArray[1]
                    let  tabBarItem2: UITabBarItem = tabArray[2]
                    
                    tabBarItem1.isEnabled = false
                    tabBarItem2.isEnabled = false
                    
                    AlertControl.alert(appmassage: "Voucher Code Services Expire.", view: self)
                    
                }}else{
                let tabBarControllerItems = self.tabBarController?.tabBar.items
                if let tabArray = tabBarControllerItems {
                    let tabBarItem1: UITabBarItem = tabArray[1]
                    let  tabBarItem2: UITabBarItem = tabArray[2]
                    
                    tabBarItem1.isEnabled = true
                    tabBarItem2.isEnabled = true
                }
            }}
    }
    
    
    //MARK: - IBActions
    @IBAction func btnClickSubmit(_ sender: Any)
    {
        if txtEnterCode.text ==  ""
        {
            AlertControl.alert(appmassage: "Please Enter Voucher Code", view: self)
        }
        else
        {
            applyRechargeVoucher()
        }
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    @IBAction func btnClickAgain(_ sender: Any)
    {
        getRechargeVoucher()
    }
    
    
    //MARK: - UITextfieldDelgates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
    //MARK: - WebServiceMethods
    func applyRechargeVoucher()
    {
        txtEnterCode.text = ""
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            //param.setValue(String(describing: reqData.value(forKey: "RequestId")!), forKey: "requestId")
            param.setValue(("3779"), forKey: "FreelancerId")
            param.setValue((txtEnterCode.text), forKey: "VoucherCode")
            
            let Url = ServiceUrl.freelanceUrl + "AddVoucher"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        AlertControl.alert(appmassage: (response["Message"] as? String)!, view: self)
                        //self.navigationController?.popViewController(animated: true)
                        let tabBarControllerItems = self.tabBarController?.tabBar.items
                        
                        guard let res = response["data"] as? NSDictionary
                            else{
                                return
                        }
                        guard let balanceServices:String = res.value(forKey: "BalanceServices") as? String
                            else{
                                return
                        }
                        if balanceServices == "0"
                        {
                            if let tabArray = tabBarControllerItems {
                                let tabBarItem1: UITabBarItem = tabArray[1]
                                let  tabBarItem2: UITabBarItem = tabArray[2]
                                tabBarItem1.isEnabled = false
                                tabBarItem2.isEnabled = false
                            }
                            AlertControl.alert(appmassage: "Voucher Code Services Expire.", view: self)
                        }
                        else{
                            if let tabArray = tabBarControllerItems {
                                let tabBarItem1: UITabBarItem = tabArray[1]
                                let  tabBarItem2: UITabBarItem = tabArray[2]
                                
                                tabBarItem1.isEnabled = true
                                tabBarItem2.isEnabled = true
                            }
                            
                            UserDefaults.standard.set(balanceServices, forKey: "balanceServices")
                            UserDefaults.standard.synchronize()
                        }
                    }
                    else{
                        AlertControl.alert(appmassage: (response["Message"] as? String)!, view: self)
                    }
                    
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
                
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    func getRechargeVoucher()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            
            let Url = ServiceUrl.freelanceUrl + "VoucherDetails"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        guard let res = response["data"] as? NSDictionary
                            else{
                                return
                        }
                        guard let voucherCode:String = res.value(forKey: "VoucherCode") as? String
                            else{
                                return
                        }
                        self.txtEnterCode.text = voucherCode
                    }
                    else{
                        AlertControl.alert(appmassage: (response["Message"] as? String)!, view: self)
                    }
                    
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
                
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
}

