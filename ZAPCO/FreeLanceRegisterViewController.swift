//
//  FreeLanceRegisterViewController.swift
//  ZAPCO
//
//  Created by vinove on 12/21/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation


class FreeLanceRegisterViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UITextFieldDelegate, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    //MARK: - Variables
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtLicenseNo: UITextField!
    @IBOutlet weak var txtSkills: UITextField!
    @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var imageVProfile: UIImageView!
    @IBOutlet weak var txtExpertise: UITextField!
    @IBOutlet weak var btnUploadProfile: UIButton!
    @IBOutlet weak var btn10th: UIButton!
    @IBOutlet weak var btn12th: UIButton!
    @IBOutlet weak var btnGrad: UIButton!
    @IBOutlet weak var btnDL: UIButton!
    @IBOutlet weak var btnVotID: UIButton!
    @IBOutlet weak var btnAdhar: UIButton!
    @IBOutlet weak var btnOthers: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var heightCollV: NSLayoutConstraint!
    
    
    var myPicker = UIPickerView()
    var txtfieldPicker = UITextField()
    var arrPicker = NSMutableArray()
    
    var imagePicker = UIImagePickerController()
    var arrDocuments : Array<UIImage> = []
    var arrDocumentNames : Array<String> = []
    var locationValue:[String:Any] = ["Latitude":"","Longitude":""]
    var arrSkills = NSMutableArray()
    var arrCategoryID = NSMutableArray()
    var arrExpertise = NSMutableArray()
    var arrData = NSArray()
    var catID = NSInteger()
    var arrSubData = NSArray()
    var subCatID = NSInteger()
    var isTypeService = Bool()
    var isStudent : Bool = false
    var dictDocuments = NSMutableDictionary()
    var arrSubCat = NSMutableArray()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtName.delegate = self
        txtConfirmPassword.delegate = self
        txtSkills.delegate = self
        txtExpertise.delegate = self
        txtPhoneNo.delegate = self
        txtLicenseNo.delegate = self
        
        setUpView()
        heightCollV.constant = 0
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        getCategory()
        getSubCategory()
      
        self.locationValue.updateValue(String(LocationManager.manager.currentLocation.coordinate.latitude), forKey: "Latitude")
        self.locationValue.updateValue(String(LocationManager.manager.currentLocation.coordinate.longitude), forKey: "Longitude")
        
        self.imageVProfile.contentMode = .scaleAspectFill
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func  setUpView()
    {
        Shadow.textShadow(view: view1)
        Shadow.textShadow(view: view2)
        
        txtSkills = TextfieldImage.textfieldImageRight(txt: txtSkills)
        txtExpertise = TextfieldImage.textfieldImageRight(txt: txtExpertise)
    
        imageVProfile.layer.cornerRadius = imageVProfile.frame.size.height/2.0
        imageVProfile.layer.masksToBounds = true
    }
    
    
    
    //MARK: - IBActions
    @IBAction func btnSignUp(_ sender: Any)
    {
        if txtName.text == ""{
            txtName.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter your Name", view: self)
            
        }
        else if txtEmail.text == ""{
            txtEmail.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Email ID", view: self)
            
        }
        else if txtPassword.text == ""{
            txtPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Password", view: self)
            
        }
        else if txtConfirmPassword.text == ""{
             txtConfirmPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Confirm Password", view: self)
           
        }
        else if txtConfirmPassword.text != txtPassword.text{
            txtPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Password Not Macthed", view: self)
            
        }
        else if txtPhoneNo.text == ""{
             addDoneButtonOnKeyboard()
            AlertControl.alert(appmassage: "Please Enter Phone Number", view: self)
           
        }
        else if txtLicenseNo.text == ""{
            txtLicenseNo.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter License Number", view: self)
            
        }
            
        else if txtSkills.text == ""{
            txtSkills.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Select Your Trade", view: self)
            
        }
            
        else if (txtPassword.text?.count)! < 6 || (txtConfirmPassword.text?.count)! < 6
        {
             txtPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Password Must Contain atlest 6 Characters", view: self)
           
        }
        
        else if isValidEmail(testStr: txtEmail.text!) == false
        {
            txtEmail.becomeFirstResponder()
            AlertControl.alert(appmassage: "Invalid Email", view: self)
            
        }
        
        else if (txtPhoneNo.text?.count)! < 10 || (txtPhoneNo.text?.count)! > 12
        {
            txtPhoneNo.becomeFirstResponder()
            AlertControl.alert(appmassage: "Invalid Phone Number", view: self)
            
        }
            
        else if txtPhoneNo.text?.count == 0 || txtConfirmPassword.text?.count == 0 || txtPassword.text?.count == 0 || txtName.text?.count == 0 || txtSkills.text?.count == 0 || txtLicenseNo.text?.count == 0 || txtEmail.text?.count == 0
        {
            AlertControl.alert(appmassage: "All fields Are mandatory", view: self)
        }
            
        else
        {
        getSignUp()
        }
    }
    
    
    @IBAction func btnLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUploadProfileImage(_ sender: UIButton)
    {
        btnUploadProfile.isSelected = true
        upload()
    }
    
    @IBAction func btnUploadDocuments(_ sender: UIButton)
    {
        if arrDocuments.count < 2
        {
            btnUploadProfile.isSelected = false
            if (sender.isSelected == false)
            {
                sender.isSelected = true
                sender.setImage(UIImage(named:"sel"), for: UIControlState.normal)
            }
         upload()
        }
        else
        {
            AlertControl.alert(appmassage: "You can upload maximum two documents", view: self)
        }
        
    }
    
    
    @IBAction func deleteDocuments(_ sender: UIButton)
    {
        self.arrDocuments.remove(at: sender.tag )
        self.arrDocumentNames.remove(at: sender.tag)
        self.collectionV.reloadData()
        if arrDocuments.count > 0
        {
            heightCollV.constant = 70
        }
        else
        {
            heightCollV.constant = 0
        }
    }
    
    @IBAction func btnStudent(_ sender: UIButton)
    {
        if (sender.isSelected == false)
        {
            sender.isSelected = true
            isStudent = true
            sender.setImage(UIImage(named:"sel"), for: UIControlState.normal)
        }
        else
        {
            isStudent = false
            sender.isSelected = false
            sender.setImage(UIImage(named:"unsel"), for: UIControlState.normal)
        }
    }
    
    
    //MARK: - UITextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == txtName)
        {
            txtEmail.becomeFirstResponder()
        }
        else if (textField == txtEmail)
        {
            txtPassword.becomeFirstResponder()
        }
        else if(textField == txtPassword)
        {
            txtConfirmPassword.becomeFirstResponder()
        }
        else if(textField == txtConfirmPassword)
        {
            txtPhoneNo.becomeFirstResponder()
        }
        else if(textField == txtPhoneNo)
        {
            txtLicenseNo.becomeFirstResponder()
        }
        else if(textField == txtLicenseNo)
        {
            txtSkills.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtPhoneNo
        {
            isTypeService = false
            addDoneButtonOnKeyboard()
        }
        
        if (textField == txtExpertise)
        {
            isTypeService = false
            arrExpertise.removeAllObjects()
            if txtSkills.text == ""
            {
                AlertControl.alert(appmassage: "Please select the skills", view: self)
            }
            else
            {
                for dict in self.arrSubData
                {
                    let catDict = dict as! NSDictionary
                    if let subCat = catDict.value(forKey: "CategoryId") as? NSInteger{
                        print(subCat)
                        if self.catID == catDict.value(forKey: "CategoryId") as! NSInteger
                        {
                            self.arrExpertise.add(catDict.value(forKey: "SubCategory")!)
                            self.arrSubCat.add(catDict.value(forKey: "SubCategoryId")!)
                        }
                    }
                    if self.arrSubData.count == 0
                    {
                        self.arrExpertise.add("")
                    }
                }
                
                txtfieldPicker = txtExpertise
                arrPicker = arrExpertise
                self.pickUp(txtfieldPicker)
            }
        }
        
        if (textField == txtSkills)
        {
            isTypeService = true
            txtfieldPicker = txtSkills
            arrPicker = arrSkills
            self.pickUp(txtfieldPicker)
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        textField.resignFirstResponder()
        myPicker.isHidden = true
    }
    
    
    //MARK: - KeyboardCustomization
    func addDoneButtonOnKeyboard()
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width: 320, height:50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        self.txtPhoneNo.inputAccessoryView = doneToolbar
    }
    func doneButtonAction()
    {
        self.txtLicenseNo.becomeFirstResponder()
    }
    
    
    
    //MARK: - UIPickerViewMethods
    func pickUp(_ textField : UITextField)
    {
        // UIPickerView
        myPicker = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        myPicker.delegate = self
        myPicker.dataSource = self
        myPicker.backgroundColor = UIColor.white
        textField.inputView = myPicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick()
    {
        self.txtfieldPicker.resignFirstResponder()
    }
    @objc func cancelClick()
    {
        self.txtfieldPicker.resignFirstResponder()
    }
    
    //MARK: - EmailValidations
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    //MARK: - UIPickerViewDatasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        if arrPicker.count == 0
        {
            return 0
        }
        else
        {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if arrPicker.count == 0
        {
            return 0
        }
        else
        {
            return arrPicker.count
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if !(arrPicker.count == 0)
        {
            return arrPicker[row] as? String
        }
        else
        {
            return ""
        }
    }
    
    
    //MARK: - UIPickerviewDelegates
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent  component: Int)
    {
        txtfieldPicker.text = arrPicker[row] as? String
        // let dictCat = arrData[row] as! NSDictionary
        if isTypeService{
            self.catID = arrCategoryID[row] as! NSInteger
        }
        else if arrSubCat.count != 0
        {
            if let scatID = arrSubCat[row] as? NSInteger {
                self.subCatID = scatID
            }
        }
    }
    
    
    
    
    //MARK: - UploadImageMethods
    func upload()
    {
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - ImagePickerDelegates
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:  [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        if (btnUploadProfile.isSelected == true)
        {
            imageVProfile.image = chosenImage
            btnUploadProfile.isSelected = false
        }
        else
        {
            arrDocuments.append(chosenImage)
            if arrDocuments.count == 0
            {
                arrDocumentNames.append("1")
            }
            else
            {
                arrDocumentNames.append("2")
            }
            collectionV.reloadData()
            if arrDocuments.count > 0
            {
                heightCollV.constant = 70
            }
            else
            {
                heightCollV.constant = 0
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    
   
    //MARK: - CollectionViewDataSourceMethods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrDocuments.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellDocuments", for: indexPath as IndexPath)
        let imageV: UIImageView = cell.viewWithTag(100) as! UIImageView
        imageV.image = self.arrDocuments[indexPath.item]
        
        let btnDelete:UIButton = UIButton(frame: CGRect(x: cell.frame.size.width-22, y: 10, width: 20, height: 20))
        btnDelete.setImage(UIImage(named:"delete"), for: UIControlState.normal)
        btnDelete.tag = indexPath.item
        btnDelete.addTarget(self, action: #selector(deleteDocuments(_:)), for: UIControlEvents.touchUpInside)
    
        cell.addSubview(imageV)
        cell.addSubview(btnDelete)
        return cell
    }
    
    
    // MARK: - WebServiceMethods
    func getSignUp()
    {
        self.view.endEditing(true)
        self.locationValue.updateValue(String(LocationManager.manager.currentLocation.coordinate.latitude), forKey: "Latitude")
        self.locationValue.updateValue(String(LocationManager.manager.currentLocation.coordinate.longitude), forKey: "Longitude")
        
        let strAddress = UserDefaults.standard.value(forKey: "address") as! String
        let strCity = UserDefaults.standard.value(forKey: "city") as! String
        let strProfileImageName = "F_" + txtName.text!
        
        var strIsStudent = String()
        if isStudent == false
        {
            strIsStudent = "false"
        }else
        {
            strIsStudent = "true"
        }
        
        var arrDocImageData = Array<Any>()
        for i in 0..<arrDocuments.count
        {
            arrDocImageData.append(self.imageConvertMethod(img: arrDocuments[i]))
        }
        
        var arrayFinalDocuments = Array<Any>()
        
        for i in 0..<arrDocumentNames.count
        {
            let dictDoc = ["DocumentType":arrDocumentNames[i], "DocumentImageString":arrDocImageData[i]] as [String : Any]
            arrayFinalDocuments.append(dictDoc)
        }
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let systemVersion = UIDevice.current.systemVersion
            let deviceid = UIDevice.current.identifierForVendor?.uuidString
            let token = UserDefaults.standard.value(forKey: "ApplicationUniqueIdentifier") as! String
            
            let param = NSMutableDictionary()
            param.setValue("5", forKey: "RoleId")
            param.setValue("2", forKey: "userType")
            param.setValue(self.txtName.text, forKey: "Name")
            param.setValue(self.txtEmail.text, forKey: "Email")
            param.setValue(self.txtPassword.text, forKey: "Password")
            param.setValue(txtPhoneNo.text, forKey: "PhoneNumber")
            param.setValue(systemVersion, forKey: "DeviceOsVersion")
            param.setValue("iOS", forKey: "DeviceType")
            param.setValue(token, forKey: "DeviceId")
            param.setValue(strAddress, forKey: "Address")
            param.setValue(strCity, forKey: "city")
            param.setValue(strIsStudent, forKey: "isStudent")
            param.setValue(self.catID, forKey: "skills")
            param.setValue(txtExpertise.text, forKey: "ExpertiseDescription")
            param.setValue(self.subCatID, forKey: "subcategoryId")
            param.setValue(arrayFinalDocuments, forKey: "documents")
            param.setValue(self.locationValue, forKey: "location")
            param.setValue(strProfileImageName, forKey: "ProfileImageName")
            param.setValue((self.imageConvertMethod(img:self.imageVProfile.image!)), forKey: "ProfileImageString")
            param.setValue(txtLicenseNo.text, forKey: "LicenseNumber")
           
            let tokenUrl = ServiceUrl.signUpURL + "signUp"
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : ""){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                       if let data = response["Data"] as? NSDictionary
                       {
                        let uData = Userdata()
                        uData.userData = data
                        Shared.sharedInstance.userDetails = uData
                        let userDta: Data = NSKeyedArchiver.archivedData(withRootObject: uData.userData)
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(userDta, forKey: stringConstants.userData)
                        }
                        self.getToken()
                    }
                    else
                    {
                        AlertControl.alert(appmassage: response["Message"] as! String, view: self)
                    }
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    func getToken()
    {
        self.view.endEditing(true)
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        let param = NSMutableDictionary()
        param.setValue("password", forKey: "grant_Type")
        param.setValue(self.txtEmail.text!, forKey: "username")
        param.setValue(self.txtPassword.text!, forKey: "password")
        
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let tokenUrl = ServiceUrl.baseUrl + "/token"
        VTAlmofireHelper.sharedInstance.post(with: URL(string:tokenUrl)!, method: "POST", params: param as? Dictionary<String, Any>, complition: { (response, error) -> (Void) in
            print(response ?? 00)
            
            if let error:String = response!["error_description"] as? String
            {
                if error == "The user name or password is incorrect."
                {
                    AlertControl.alert(appmassage:error, view: self)
                    DispatchQueue.main.sync {
                        ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                        self.txtPassword.text = ""
                        self.txtEmail.text = ""
                    }
                    return
                }
                
            }
            
            let dict = response! as NSDictionary
            let tokenDATA = TokenData()
            tokenDATA.tokenType = dict.value(forKey: "token_type") as! NSString
            tokenDATA.accesstoken = dict.value(forKey: "access_token") as! NSString
            tokenDATA.userID = dict.value(forKey: "userID") as! NSString
            Shared.sharedInstance.token_Data = tokenDATA
            Shared.sharedInstance.tokenDict = dict as! [String : Any]
            let tokenDta: Data = NSKeyedArchiver.archivedData(withRootObject: Shared.sharedInstance.tokenDict)
            let userDefaults = UserDefaults.standard
            userDefaults.setValue(tokenDta, forKey: stringConstants.tokenData)
            
            DispatchQueue.main.async
                {
                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "freeTabbar")
                appDelegate.window?.rootViewController = initialViewController
                appDelegate.window?.makeKeyAndVisible()
                AlertControl.alert(appmassage: "Registration is Done Successfully!", view: self)
            }
            
            
        })
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
       
    }
    
    func imageConvertMethod(img:UIImage)->String{
        let imageData: NSData = UIImageJPEGRepresentation(img, 0.4)! as NSData
        let imageStr = imageData.base64EncodedString(options:.lineLength64Characters)
        
        return imageStr
    }
    
    
    func getCategory()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true{
        
        let tokenUrl = ServiceUrl.CommonUrl + "category"
        // let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : ""){
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)

            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                     self.arrCategoryID.removeAllObjects()
                    let data = response["Data"] as! NSArray
                    self.arrData = data
                    for dict in data
                    {
                        let catDict = dict as! NSDictionary
                        if  let category = catDict.value(forKey: "Category") as? String{
                            self.arrSkills.add(category)
                            self.arrCategoryID.add(catDict.value(forKey: "CategoryId") as! NSInteger)
                        }
                        if self.arrData.count == 0
                        {
                            self.arrSkills.add("")
                        }
                        self.myPicker.reloadAllComponents()
                    }
                    
                }
            }
            }}
        else
        {
            return
        }
        
    }
    
    func getSubCategory()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true{
        
        let tokenUrl = ServiceUrl.CommonUrl + "subcategory"
        // let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : ""){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                    let data = response["Data"] as! NSArray
                    self.arrSubData = data
                }
            }
            }}
        else
        {
           return
        }
    }
    
}


