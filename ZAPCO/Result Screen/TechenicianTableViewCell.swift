//
//  TechenicianTableViewCell.swift
//  ZAPCO
//
//  Created by vinove on 10/13/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class TechenicianTableViewCell: UITableViewCell {
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var btnView: customButton!
    @IBOutlet weak var btnBook: customButton!
    @IBOutlet weak var rating: UIRateView!
    
    
    override func layoutSubviews() {
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
         Shadow.textShadow(view: viewCell)
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
