//
//  NotificationTableViewCell.swift
//  ZAPCO
//
//  Created by vinove on 10/24/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var imgNotifi: UIImageView!
    @IBOutlet weak var lblNotifi: UILabel!
    @IBOutlet weak var viewCell: UIView!
    
    
    override func layoutSubviews() {
        Shadow.textShadow(view: self.viewCell)
        imgNotifi.layer.cornerRadius = imgNotifi.frame.size.height/2
        imgNotifi.layer.masksToBounds = true
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
