//
//  NotificationViewController.swift
//  ZAPCO
//
//  Created by vinove on 10/24/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class NotificationViewController: UIViewController
{
    
    //MARK: - Variables
    @IBOutlet weak var tableView: UITableView!
    var arrData = NSMutableArray()
    var style = ToastStyle()
    
    @IBOutlet weak var lblBadgeCount: UILabel!
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.lblBadgeCount.layer.cornerRadius = self.lblBadgeCount.frame.width/2

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        getNotificationList()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    
    
    //MARK: - IBActions
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK: - WebServiceMethods
    func getNotificationList()
    {
        if self.arrData.count != 0{
        self.arrData.removeAllObjects()
        }
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
      
        let tokenUrl = ServiceUrl.customerUrl + "NotificationDetails"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : auth){
            (response:Dictionary,success:Bool) in
            print(response)
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success)
            {
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    if  let arr = response["Data"] as? NSArray
                    {
                        for dict in arr{
                            let dictData = dict as! NSDictionary
                            let not = NotificationData()
                            not.notifyDict = dictData
                            if let title = dictData.value(forKey: "Notification") as? String{
                                not.notifyTitle = title
                            }else{
                                not.notifyTitle = ""
                            }
                            if let img = dictData.value(forKey: "ImagePath") as? String{
                                not.image = img
                            }else{
                                not.image = ""
                            }
                            if let id = dictData.value(forKey: "NotificationId") as? NSNumber{
                                not.notifyId = id
                            }else{
                               not.notifyId = 0
                            }
                            not.reqId = "\(dictData.value(forKey: "RequestId")!)"
                             
                            if let dt = dictData.value(forKey: "Date") as? String{
                                not.dateTime = dt
                            }else{
                                not.dateTime = ""
                            }
                            
                            if let viewStatus = dictData.value(forKey: "ViewStatus") as? NSNumber{
                            not.status = viewStatus
                            }else{
                                not.status = 0
                            }
                            self.arrData.add(not)
                        }
                    }
                    if self.arrData.count == 0
                    {
                        //self.view.makeToast(response["Message"] as? String, duration: 2.0, position: .center, style: self.style)
                        self.view.makeToast("No Notification to Show", duration: 0.5, position: .center, style: self.style)
                        self.tableView.reloadData()
                    }
                    else
                    {
                        for i in 0..<self.arrData.count{
                           let objects = self.arrData[i] as! NotificationData
                            self.notificationStatus(id: objects.notifyId)
                        }
                        self.tableView.reloadData()
                    }
                }
                else
                {
                     self.view.makeToast("No Notification to Show", duration: 0.5, position: .center, style: self.style)
                }
            }
            else{
                
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
        }
        }
        
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    
    func notificationStatus(id: NSNumber)
    {
//        ["Read": 0, "Code": 200, "Unread": 1, "Message": Data does not Exit, "Data": <__NSSingleObjectArrayI 0x103be5570>(
//            {
//            Date = "2018-09-28T12:21:03.093";
//            ImagePath = "http://180.151.232.92:124/ImageStorage/Customer/9edbf70a-4935-421f-9207-e89472cd253a.jpg";
//            Notification = " Request Accepted successfully for Electrical";
//            NotificationId = 195;
//            NotificationuniqueId = "6dbb0a77-4fbf-4fc6-a115-9ed9775a0ef0";
//            RequestId = 1509;
//            ViewStatus = 0;
//            }
       
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            let notifyId = id as? Int
            param.setValue(notifyId, forKey: "NotificationuniqueId")
            
            let tokenUrl = ServiceUrl.customerUrl + "NotificationStatusUpdate"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: [:] ,  url: tokenUrl, authentication : auth){
                (response:Dictionary,success:Bool) in
                print(response)
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success)
                {
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
            
                    }
                    else
                    {
                       
                    }
                }
                else{
                    
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
        }
            
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
}

extension NotificationViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrData.count != 0
        {
            return arrData.count
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"NotificationTableViewCell") as! NotificationTableViewCell
        let objects = self.arrData[indexPath.row] as! NotificationData
        cell.lblNotifi.text = objects.notifyTitle
        
        if objects.image != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.image as String)
            if profileUrl != nil
            {
                cell.imgNotifi.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.imgNotifi.image = placeholderImage
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let objects = self.arrData[indexPath.row] as! NotificationData
        self.notificationStatus(id: objects.notifyId)
        if let userData:Data  = UserDefaults.standard.value(forKey: stringConstants.userData) as? Data {
            let dictionary: NSDictionary = (NSKeyedUnarchiver.unarchiveObject(with: userData) as? NSDictionary)!
            let uData = Userdata()
            uData.userData = dictionary
            Shared.sharedInstance.userDetails = uData
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            switch dictionary["RoleId"] as! Int{
            case 2: //customer
                let controller = storyboard.instantiateViewController(withIdentifier: "ServiceHistoryDetailsViewController") as! ServiceHistoryDetailsViewController
                controller.notificationReqId = objects.reqId
                UserDefaults.standard.set(false, forKey: "showRateView")
                UserDefaults.standard.synchronize()
                self.navigationController?.pushViewController(controller, animated: true)
                break
            case 4: //company technician
                let controller = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
                controller.notificationReqId = objects.reqId
                self.navigationController?.pushViewController(controller, animated: true)
                break
            case 5: //freelanceTechnician
                let controller = storyboard.instantiateViewController(withIdentifier: "ReqDetailsViewController") as! ReqDetailsViewController
                controller.notificationReqId = objects.reqId
                self.navigationController?.pushViewController(controller, animated: true)
                break
            default:
                break
            }
        }
        
    }
}

