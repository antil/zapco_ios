//
//  ResonToCancelViewController.swift
//  ZAPCO
//
//  Created by vinove on 3/23/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class ResonToCancelViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{
    @IBOutlet weak var txtReason: UITextField!
    
    var myPicker = UIPickerView()
    var txtfieldPicker = UITextField()
    var arrPicker = NSMutableArray()
    var reqID = String()
    var catID = String()
    
    //MARK: - ViewLifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        txtReason.delegate = self
        txtfieldPicker.delegate = self
        
        let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        let image = UIImage(named: "dropdown")
        imgV.image = image
        txtReason.rightViewMode = UITextFieldViewMode.always
        txtReason.rightView = imgV
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        cancelReason()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    
    
    
    @IBAction func btnSubmit(_ sender: Any)
    {
        if txtReason.text == ""
        {
            AlertControl.alert(appmassage: "Please Select Any Reason to Cancel", view: self)
        }
        else
        {
            cancelService()
        }
    }
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.view.transform = self.view.transform.scaledBy(x: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
            self.view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        }, completion: {finished in
            self.willMove(toParentViewController: nil)
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
            
        })
    }
    
    @IBAction func btnDropDown(_ sender: Any)
    {
       self.pickUp(txtfieldPicker)
    }
    
    
    //MARK: - UIPickerViewMethods
    func pickUp(_ textField : UITextField)
    {
        // UIPickerView
        myPicker = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        myPicker.delegate = self
        myPicker.dataSource = self
        myPicker.backgroundColor = UIColor.white
        textField.inputView = myPicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick()
    {
        if arrPicker.count != 0{
            if txtReason.text == ""{
                 txtReason.text = arrPicker[0] as? String
            }
        }
        self.txtfieldPicker.resignFirstResponder()
    }
    @objc func cancelClick()
    {
        self.txtfieldPicker.resignFirstResponder()
    }
    
    
    //MARK: - UIPickerViewDatasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        if arrPicker.count == 0
        {
            return 0
        }
        else
        {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if arrPicker.count == 0
        {
            return 0
        }
        else
        {
            return arrPicker.count
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if !(arrPicker.count == 0)
        {
            return arrPicker[row] as? String
        }
        else
        {
            return "Data not found"
        }
    }
    
    
    //MARK: - UIPickerviewDelegates
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent  component: Int)
    {
        txtfieldPicker.text = arrPicker[row] as? String
    }
    
    
    //MARK: - UITextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        txtfieldPicker = txtReason
        self.pickUp(txtfieldPicker)
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        textField.resignFirstResponder()
        myPicker.isHidden = true
    }
    
    
    
    
    //MARK: - WebServiceMethods
    func cancelReason()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        let param = NSMutableDictionary()
        param.setValue("1", forKey: "CategoryId")
        
        let tokenUrl = ServiceUrl.customerUrl + "cancelReasons"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth)
        {
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success)
            {
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    let arrData = response["Data"] as! NSArray
                    for str in arrData
                    {
                        let reason =  str as! NSString
                        self.arrPicker.add(reason)
                    }
               }
                
            }
            else{
                AlertControl.alert(appmassage: "Try Again Later!", view: self)
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
        
        func cancelService()
        {
            if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
            {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            param.setValue(reqID, forKey: "requestID")
            param.setValue(txtReason.text, forKey: "reason")
            
            let tokenUrl = ServiceUrl.customerUrl + "cancelService"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth)
            {
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                
                if(success)
                {
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        AlertControl.alert(appmassage: response["Message"] as! String, view: self)
                        self.view.removeFromSuperview()
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try Again Later!", view: self)
                    self.view.removeFromSuperview()
                }
            }
            }
            else
            {
                AlertControl.alert(appmassage: "No Internet Connection!", view: self)
                self.view.removeFromSuperview()
            }
        }

    
}
