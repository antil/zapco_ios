//
//  ServiceHistoryDetailsViewController.swift
//  ZAPCO
//
//  Created by vinove on 3/22/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import CoreLocation

class ServiceHistoryDetailsViewController: UIViewController, UITextViewDelegate
{
    
    @IBOutlet weak var lblReqNumbr: UILabel!
    @IBOutlet weak var lblReqDT: UILabel!
    @IBOutlet weak var lblReqType: UILabel!
    
    @IBOutlet weak var lblTechnician: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAmt: UILabel!
    @IBOutlet weak var lblEta: UILabel!
    @IBOutlet weak var ratingView: UIRateView!
    @IBOutlet weak var txtComments: UITextView!
    var historyDetails = ServiceHistory()
    var strCategory = String()
    var strAddress = String()
    var notificationReqId:String?
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewReq: UIView!
    @IBOutlet weak var viewRating: UIView!
    var location = ["Latitude":"28.980955", "Longitude":"77.180955"]
   
    
    @IBOutlet weak var heightRateV: NSLayoutConstraint!
    @IBOutlet weak var heightViewRate: NSLayoutConstraint!
   
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        txtComments.delegate = self
    }
    
    func initialSetup()
    {
        //Shadow.textShadow(view: self.viewTop)
        Shadow.textShadow(view: self.viewReq)
        Shadow.textShadow(view: self.viewRating)
        
        self.lblReqNumbr.text = String(historyDetails.reqID)
       self.lblReqDT.text = ShowTime.dateConvert(date: historyDetails.dateTime as String) + " at " + ShowTime.timeConvert(date: historyDetails.dateTime as String)
        
        self.lblReqType.text = historyDetails.category as String
        self.lblTechnician.text = String(historyDetails.id)
        
        self.lblAmt.text = historyDetails.amount as String
        self.lblEta.text = historyDetails.eta as String
        
        if historyDetails.amount == ""
        {
            self.lblAmt.text = "Not Applicable"
        }
        if historyDetails.eta == ""
        {
            self.lblEta.text = "Not Applicable"
        }
        if historyDetails.comments == ""
        {
           self.txtComments.text = ""
        }
        else{
            self.txtComments.text = historyDetails.comments as String}
        self.lblLocation.text = strAddress
        self.ratingView.rating = Double(historyDetails.rating)
        self.strCategory = historyDetails.category as String
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if self.notificationReqId != nil {
            getData()
        } else{
        
            if (historyDetails.latitude as NSNumber) == 0 || (historyDetails.longitude as NSNumber) == 0
            {
                print("location not found")
            }
            else
            {
                getAddressFromLatLon(pdblLatitude: "\(historyDetails.latitude)", withLongitude: "\(historyDetails.longitude)")
            }
            initialSetup()
        }
       
       if UserDefaults.standard.value(forKey: "showRateView") as? Bool == true
       {
        heightViewRate.constant = 512
        viewRating.isHidden = false
        heightRateV.constant = 162
        }
        else
        {
        heightViewRate.constant = 340
        viewRating.isHidden = true
        heightRateV.constant = 0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if notificationReqId != nil
        {
            notificationReqId = nil
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String)
    {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.subThoroughfare != nil {
                        addressString = addressString + pm.subThoroughfare! + " "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }

                    print(addressString)
                    self.lblLocation.text = addressString
                }
        })
      
    }
    
    //MARK: - UITextViewDelegates
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        txtComments.resignFirstResponder()
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
  
    
    
    //MARK: - IBActions
    @IBAction func btnSendInvoice(_ sender: Any)
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(self.lblReqNumbr.text, forKey: "requestId")
       
        let Url = ServiceUrl.customerUrl + "requestInvoice"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                }
            }
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    @IBAction func btnSubmit(_ sender: Any)
    {
        //{ "technicianId":"2", "rating":"4", "comments":"Test comment", "requestId":"172"}
        location.updateValue(String(LocationManager.manager.currentLocation.coordinate.latitude), forKey: "Latitude")
        location.updateValue(String(LocationManager.manager.currentLocation.coordinate.longitude), forKey: "Longitude")
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            
            let ratings = ratingView.rating
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(self.lblTechnician.text, forKey: "technicianId")
        param.setValue(self.lblReqNumbr.text, forKey: "requestId")
        param.setValue(ratings, forKey: "rating")
        param.setValue(txtComments.text, forKey: "comments")
       
        let Url = ServiceUrl.customerUrl + "technicianFeedback"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                }
        
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }}
        }
            else
            {
                AlertControl.alert(appmassage: "No Internet Connection!", view: self)
            }
    }
    
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTrackTech(_ sender: Any)
    {
        //{ "technicianId":"1", "requestId":"5", "location":{"latitude":"0.0", "longitude":"0.0"}}
        
        location.updateValue(String(LocationManager.manager.currentLocation.coordinate.latitude), forKey: "Latitude")
        location.updateValue(String(LocationManager.manager.currentLocation.coordinate.longitude), forKey: "Longitude")
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(self.lblTechnician.text, forKey: "technicianId")
        param.setValue(self.lblReqNumbr.text, forKey: "requestId")
        param.setValue(location, forKey: "location")

        let Url = ServiceUrl.customerUrl + "trackTechnician"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        if let dict:NSDictionary = response["Data"] as! NSDictionary?
                        {
                           if let latitude = dict["Latitude"] as? NSNumber
                           {
                            let longitude = dict["Longitude"]
                            let controller = self.storyboard?.instantiateViewController(withIdentifier: "TrackTechViewController") as! TrackTechViewController
                            controller.lat = latitude
                            controller.long = longitude as! NSNumber
                            self.navigationController?.pushViewController(controller, animated: true)
                           }
                            
                        }
                    
                    }
                }
            else{
                AlertControl.alert(appmassage: "Try Again Later!", view: self)
                }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }

   
    @IBAction func btnNotification(_ sender: Any)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnCancelReq(_ sender: Any)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ResonToCancelViewController") as! ResonToCancelViewController
        controller.reqID = lblReqNumbr.text!
        controller.catID = strCategory
        controller.willMove(toParentViewController: self)
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        controller.didMove(toParentViewController: self)
        
        controller.view.transform = controller.view.transform.scaledBy(x: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
            controller.view.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
        }, completion: nil)
    }
    
    //MARK: - WebServiceMethod
    func getData()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            param.setValue(notificationReqId, forKey: "RequestId")
           
            let tokenUrl = ServiceUrl.customerUrl + "NotificationrequestDetail"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                        let dictData = response["Data"] as! NSDictionary
                        
                        let serviceData = ServiceHistory()
                        if let dictRequester:NSDictionary = dictData.value(forKey: "Technician") as? NSDictionary
                        {
                            if let lat:NSNumber = dictRequester.value(forKey: "Latitude") as? NSNumber{
                                serviceData.latitude = lat
                            }else { serviceData.latitude = 0 }
                            if let lon:NSNumber = dictRequester.value(forKey: "Longitude") as? NSNumber{
                                serviceData.longitude = lon
                            }else { serviceData.longitude = 0 }
                            
                            if let nim:NSString = dictRequester.value(forKey: "TechnicianName") as? NSString
                            {
                                serviceData.name = nim
                            }
                            
                            if let im = dictRequester.value(forKey: "TechnicianImage") as? String{
                                
                                serviceData.image = im as NSString
                            }
                            if let id = dictRequester.value(forKey: "TechnicianId") as? NSInteger{
                                serviceData.id = id
                            }else{
                                serviceData.id = 0
                            }
                            
                        }
                        if let amount = dictData.value(forKey: "Amount") as? String{
                            
                            serviceData.amount = amount as NSString
                        }
                        if let cat:NSString = dictData.value(forKey: "Category") as? NSString
                        {
                            serviceData.category = cat
                        }
                        if let com:NSString = dictData.value(forKey: "Comments") as? NSString
                        {
                            serviceData.comments = com
                        }
                        if let dt:NSString = dictData.value(forKey: "Datetime") as? NSString
                        {
                            serviceData.dateTime = dt
                        }
                        //                            if let dtm:NSString = dictData.value(forKey: "Datetime") as? NSString
                        //                            {
                        //                                serviceData.dateTime = dtm
                        //                            }
                        
                        if let eta = dictData.value(forKey: "Eta") as? NSString{
                            serviceData.eta = eta
                        }
                        
                        if let rating = dictData.value(forKey: "Rating") as? NSInteger{
                            serviceData.rating = rating
                        }else { serviceData.rating = 0 }
                        if let reqId = dictData.value(forKey: "RequestID") as? NSInteger{
                            serviceData.reqID = reqId
                        }
                        if let sts = dictData.value(forKey: "Status") as? NSInteger{
                            serviceData.status = sts
                        }
                        if let tsk = dictData.value(forKey: "Task") as? NSString{
                            serviceData.task = tsk
                        }
                        self.historyDetails = serviceData
                        self.initialSetup()
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
}
