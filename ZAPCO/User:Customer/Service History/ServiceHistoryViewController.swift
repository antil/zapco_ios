//
//  ServiceHistoryViewController.swift
//  ZAPCO
//
//  Created by vinove on 11/7/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Toast_Swift

class ServiceHistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate {
    
    @IBOutlet weak var btnPending: UIButton!
    @IBOutlet weak var btnComplted: UIButton!
    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var tableHostory: UITableView!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewButtons: CustomView!
    
    var datePicker = UIDatePicker()
    var textfieldDate = UITextField()
    var style = ToastStyle()
    var historyType = String()
    var arrShowData = NSMutableArray()
    var notificationReqId = NSNumber()

    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.initialSetup()
        self.arrShowData.removeAllObjects()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.getHistoryData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    func initialSetup()
    {
        self.historyType = "0"
        self.tableHostory.delegate = self
        self.tableHostory.dataSource = self
//        Shadow.textShadow(view: self.viewTop)
//        Shadow.textShadow(view: self.viewButtons)
        
        btnPending.isSelected = true
        btnPending.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        btnPending.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnComplted.isSelected = false
        btnSchedule.isSelected = false
        btnSchedule.backgroundColor = UIColor.white
        btnSchedule.layer.borderColor = UIColor.lightGray.cgColor
        btnSchedule.layer.borderWidth = 1.0

        self.txtFrom.delegate = self
        self.txtTo.delegate = self
    }
    
    
    //MARK: - WebServiceMethod
    func getHistoryData()
    {
        //{"endDate":"","HistoryServiceType":"1","limit":10,"offset":0,"startDate":""}
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            self.arrShowData.removeAllObjects()
            
            let param = NSMutableDictionary()
            param.setValue(self.txtFrom.text, forKey: "startDate")
            param.setValue(self.txtTo.text, forKey: "endDate")
            param.setValue(self.historyType, forKey: "HistoryServiceType")
            param.setValue(self.arrShowData.count, forKey: "offset")
            param.setValue(self.arrShowData.count + 15, forKey: "limit")
            
            let jsonData = try! JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions.prettyPrinted)
            var jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            jsonString = jsonString.trimmingCharacters(in: CharacterSet.whitespaces)
            print(jsonString)
            
            
            let tokenUrl = ServiceUrl.customerUrl + "serviceHistory"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                        let arrData = response["Data"] as! NSArray
                        for dict in arrData{
                            
                            let dictData = dict as! NSDictionary
                            
                            let serviceData = ServiceHistory()
                            
                            if let amount = dictData.value(forKey: "Amount") as? String{
                                
                                serviceData.amount = amount as NSString
                            }
                            if let im = dictData.value(forKey: "Image") as? String{
                                
                                serviceData.image = im as NSString
                            }
                            if let cat:NSString = dictData.value(forKey: "Category") as? NSString
                            {
                                serviceData.category = cat
                            }
                            if let com:NSString = dictData.value(forKey: "Comments") as? NSString
                            {
                                serviceData.comments = com
                            }
                            if let dtm:NSString = dictData.value(forKey: "Datetime") as? NSString
                            {
                                serviceData.dateTime = dtm
                            }
                            
                            if let eta = dictData.value(forKey: "Eta") as? NSString{
                                serviceData.eta = eta
                            }
                            if let id = dictData.value(forKey: "Id") as? NSInteger{
                                serviceData.id = id
                            }
                            if let lat:NSNumber = dictData.value(forKey: "Latitude") as? NSNumber{
                                serviceData.latitude = lat
                                //"\(dictData.value(forKey: "Latitude")!)"
                            }else { serviceData.latitude = 0 }
                            if let lon:NSNumber = dictData.value(forKey: "Longitude") as? NSNumber{
                                serviceData.longitude = lon
                                //"\(dictData.value(forKey: "Longitude")!)"
                            }else { serviceData.longitude = 0 }
                            if let nim:NSString = dictData.value(forKey: "Name") as? NSString
                            {
                                serviceData.name = nim
                            }
                            if let rating = dictData.value(forKey: "Rating") as? NSInteger{
                                serviceData.rating = rating
                            }else { serviceData.rating = 0 }
                            if let reqId = dictData.value(forKey: "RequestID") as? NSInteger{
                                serviceData.reqID = reqId
                            }
                            if let sts = dictData.value(forKey: "Status") as? NSInteger{
                                serviceData.status = sts
                            }
                            if let tsk = dictData.value(forKey: "Task") as? NSString{
                                serviceData.task = tsk
                            }
                            
                            if let time = dictData.value(forKey: "Time") as? NSString{
                                serviceData.time = time
                            }
                            
                            self.arrShowData.add(serviceData)
                        }
                        
                        if self.arrShowData.count > 0
                        {
                            self.tableHostory.reloadData()
                        }
                        else{
                            self.view.makeToast(response["Message"] as? String, duration: 0.5, position: .center, style: self.style)
                            self.tableHostory.reloadData()
                        }
                        
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    //MARK: - TableviewDelegates and Datasource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrShowData.count > 0
        {
            return arrShowData.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceHistoryTableViewCell") as! ServiceHistoryTableViewCell
        if(arrShowData.count > 0 && arrShowData.count > indexPath.row){
        let objects = self.arrShowData[indexPath.row] as! ServiceHistory
        
        cell.lblUsername.text = objects.name as String
        cell.lblUserType.text = objects.category as String
 
        //Datetime = "2018-10-03T00:00:00"
        let date = ShowTime.dateConvert(date:objects.dateTime as String)
        cell.lblDate.text = date as String
       // cell.lblTime.text = objects.time as String
        cell.lblTime.text = ShowTime.showTime(time: objects.time as String)
        cell.btnView.addTarget(self, action: #selector(btnClickView(_:)), for: .touchUpInside)
        cell.btnView.tag = indexPath.row
        if objects.image != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.image as String)
            if profileUrl != nil
            {
                cell.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.imgUser.image = placeholderImage
            }
            
        }
        
//        DispatchQueue.main.async
//            {
//        let imgUrl = URL.init(string: objects.image as String)
//        let imgData = try? Data.init(contentsOf: imgUrl!)
//        if imgData != nil
//        {
//            cell.imgUser.image = UIImage.init(data: imgData!)
//        }
//        else{
//        cell.imgUser.image = UIImage.init(named: "user-1")
//        }
            //        }
        }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let objects = self.arrShowData.object(at:indexPath.row) as! ServiceHistory
//
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ServiceHistoryDetailsViewController") as! ServiceHistoryDetailsViewController
//        controller.historyDetails = objects
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
//
    
    
    //MARK: - IBActions
    
    @IBAction func btnClickView(_ sender: UIButton)
    {
        let objects = self.arrShowData.object(at:sender.tag) as! ServiceHistory
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ServiceHistoryDetailsViewController") as! ServiceHistoryDetailsViewController
        controller.historyDetails = objects
        if self.historyType == "2"{
            UserDefaults.standard.set(true, forKey: "showRateView")
            UserDefaults.standard.synchronize()
        }else{
            UserDefaults.standard.set(false, forKey: "showRateView")
            UserDefaults.standard.synchronize()
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    
    }
    
    @IBAction func btnClickPending(_ sender: UIButton)
    {
        sender.isSelected = true
        sender.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        sender.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnComplted.isSelected = false
        btnComplted.backgroundColor = UIColor.white
        btnComplted.layer.borderColor = UIColor.lightGray.cgColor
        btnComplted.layer.borderWidth = 1.0
        btnSchedule.isSelected = false
        btnSchedule.backgroundColor = UIColor.white
        btnSchedule.layer.borderColor = UIColor.lightGray.cgColor
        btnSchedule.layer.borderWidth = 1.0
        historyType = "0"
        self.getHistoryData()
        self.tableHostory.reloadData()
    }
    
    
    @IBAction func btnClickSchedule(_ sender: UIButton)
    {
        sender.isSelected = true
        sender.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        sender.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnComplted.isSelected = false
        btnComplted.backgroundColor = UIColor.white
        btnComplted.layer.borderColor = UIColor.lightGray.cgColor
        btnComplted.layer.borderWidth = 1.0
        btnPending.isSelected = false
        btnPending.backgroundColor = UIColor.white
        btnPending.layer.borderColor = UIColor.lightGray.cgColor
        btnPending.layer.borderWidth = 1.0
        self.historyType = "1"
        self.getHistoryData()
        self.tableHostory.reloadData()
        
    }
    @IBAction func btnCompleted(_ sender: UIButton)
    {
        sender.isSelected = true
        sender.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        sender.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnSchedule.isSelected = false
        btnSchedule.backgroundColor = UIColor.white
        btnSchedule.layer.borderColor = UIColor.lightGray.cgColor
        btnSchedule.layer.borderWidth = 1.0
        btnPending.isSelected = false
        btnPending.backgroundColor = UIColor.white
        btnPending.layer.borderColor = UIColor.lightGray.cgColor
        btnPending.layer.borderWidth = 1.0
        self.historyType = "2"
        self.getHistoryData()
        self.tableHostory.reloadData()
    }
    @IBAction func btnSearch(_ sender: Any) {
        if txtFrom.text == ""{
            AlertControl.alert(appmassage: "Please Enter start date", view: self)
        }
        else if txtTo.text == ""{
            AlertControl.alert(appmassage: "Please Enter end date", view: self)
            
        }else{
            self.getHistoryData()
        }
    }
    
    @IBAction func btnNotifi(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    

   
    //MARK: - UITextfieldDelegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        if textField == self.txtFrom{
           
            self.textfieldDate = self.txtFrom
             self.pickUpDate(self.textfieldDate)
            
        }
        else if textField == self.txtTo{
            self.textfieldDate = self.txtTo
             self.pickUpDate(self.textfieldDate)

        }
        
    }
    
    
    //MARK: - DatePickerMethods
    func pickUpDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        self.textfieldDate.text = dateFormatter1.string(from: datePicker.date)
        self.textfieldDate.resignFirstResponder()
    }
    func cancelClick() {
    self.textfieldDate.resignFirstResponder()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
