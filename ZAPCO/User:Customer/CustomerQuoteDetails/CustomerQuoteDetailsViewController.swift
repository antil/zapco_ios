//  CustomerQuoteDetailsViewController.swift
//  ZAPCO
//
//  Created by vinove on 11/6/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import PayU_coreSDK_Swift

class CustomerQuoteDetailsViewController: UIViewController {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblRequest: UILabel!
    @IBOutlet weak var lblDescr: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblTotalCost: UILabel!
    var quotesDetails = NSDictionary()
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    var paymentParams = PayUModelPaymentParams()
    
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print(quotesDetails)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.setUpViw()
    }
    
    func setUpViw()
    {
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
        self.imgUser.layer.borderWidth = 1.0
        self.imgUser.layer.borderColor = UIColor.lightGray.cgColor
        Shadow.textShadow(view: viewTop)
        Shadow.textShadow(view: view1)
        Shadow.textShadow(view: view2)
        
        self.lblUserName.text = self.quotesDetails.value(forKey: "TechnicianName") as? String
        if let ps = self.quotesDetails.value(forKey: "Category") as? String
        {
            self.lblService.text = ps
        }
        
        if let amt = self.quotesDetails.value(forKey: "Amount")
        {
            ammountForPayment = "\(amt)"
            self.lblCost.text = "$ " + "\(amt)"
            self.lblTotalCost.text = "$ " + "\(amt)"
        }
        else{
            self.lblCost.text = "Not Applicable"
            self.lblTotalCost.text = "Not Applicable"
        }
        self.lblRequest.text = "\(self.quotesDetails.value(forKey: "RequestID")!)"
        requestIdForPayment = "\(self.quotesDetails.value(forKey: "RequestID")!)"
        
        if let dat = self.quotesDetails.value(forKey: "Datetime") as? String
        {
            self.lblDate.text = ShowTime.dateConvert(date: dat) as String + " at " + ShowTime.timeConvert(date: dat) as String
        }
        
        if let decr = self.quotesDetails.value(forKey: "Description") as? NSString{
            self.lblDescr.text = decr as String
        }
        else{
            self.lblDescr.text = "Not Applicable"
        }
        
        if let techImgStr = self.quotesDetails.value(forKey: "TechnicianImageString") as? String
        {
            
           // DispatchQueue.main.async {
                if let imgUrl = URL.init(string: techImgStr){
                    
                   // let imgData = try? Data.init(contentsOf: imgUrl)
                    
                   // if imgUrl != ""
                    //{
                    self.imgUser.af_setImage(withURL: imgUrl as URL, placeholderImage: UIImage(named:"user-1"))
                        //self.imgUser.image = UIImage.init(data: imgData!)
                   }
                    else
                    {
                        self.imgUser.image = UIImage(named:"user-1")
                    }
               
                
            //}
            
        }
        
    }
    
    
    //MARK: - IBActions
    @IBAction func btnPay(_ sender: Any)
    {
        setPaymentParam()
    
        if paymentParams.amount == "Nil" || paymentParams.amount == "<null>" {
            AlertControl.alert(appmassage: "Amount not found", view: self)
        }else{
        
        let control = self.storyboard?.instantiateViewController(withIdentifier: "PaymentsViewController")
            as! PaymentsViewController
        
        control.paymentParams = self.paymentParams
            
        
        self.navigationController?.pushViewController(control, animated: true)
        }}
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func generateTxnID() -> String {
        let currentDate = DateFormatter()
        currentDate.dateFormat = "yyyyMMddHHmmss"
//        let date = NSDate()
//        let dateString = currentDate.string(from : date as Date)
        let tranxTimeFormater = DateFormatter()
        tranxTimeFormater.dateFormat = "yyyy-MM-dd HH:mm"

        let date = NSDate()
        transactionId = currentDate.string(from : date as Date)
        transactionTime = tranxTimeFormater.string(from: date as Date)
        return transactionId
    }
    func setPaymentParam() {
        paymentParams.key = merchantKey
        paymentParams.txnId  = generateTxnID()
        paymentParams.amount = ammountForPayment
        paymentParams.productInfo = "iPhone"
        paymentParams.firstName = "Priyanka"
        paymentParams.email = "priyanka.antil@mail.vinove.com"
        paymentParams.userCredentials = "ra:ra"
        paymentParams.phoneNumber = "9990573330"
        paymentParams.environment = ENVIRONMENT_TEST
        //https://testtxncdn.payubiz.in/hdfc
        paymentParams.furl = "https://payu.herokuapp.com/failure"
        paymentParams.surl = "https://payu.herokuapp.com/success"
    }
    func methodOfReceivedNotification(notification: Notification){
        //Take Action on Notification
        
        print(notification)
        
        let alert = UIAlertController(title: "Response", message: "\(notification.object!)" , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        var merchantHash = String()
        let strConvertedRespone = "\(notification.object!)"
        
        // var jsonResult  = try JSONSerialization.jsonObject(with: notification.object!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
        
        
        let JSON : NSDictionary = try! JSONSerialization.jsonObject(with: strConvertedRespone.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
        
        if ((JSON.object(forKey: "status") as! String == "success"))
        {
            var cardToken = String()
            print("The transaction is successful")
            if (JSON.object(forKey: "cardToken")  != nil)
            {
                cardToken =  JSON.object(forKey: "cardToken") as! String
                
                if (JSON.object(forKey: "card_merchant_param") != nil)
                {
                    merchantHash = JSON.object(forKey: "card_merchant_param") as! String
                    
                }
            }
            
            
            let obj = PUSAGenerateHashes()
            
            obj.saveOneTapDataAtMerchantServer(Key: paymentParams.key!, withCardToken: cardToken, forUserCredentials: paymentParams.userCredentials!, withMerchantHash: merchantHash, withCompletionBlock: { (message, error) in
                
                
                print(message)
                
            })
        }
        self.present(alert, animated: true, completion: nil)
    }
}
