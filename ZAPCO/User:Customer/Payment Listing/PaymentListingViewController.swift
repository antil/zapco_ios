//
//  PaymentListingViewController.swift
//  ZAPCO
//
//  Created by vinove on 11/7/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Toast_Swift

class PaymentListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate
{
    
    @IBOutlet weak var viewTop: UIView!
    var arrPaymentsData = NSMutableArray()
    @IBOutlet weak var tableViewPayment: UITableView!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var tableV: UITableView!
    
    var myPicker = UIDatePicker()
    var txtFieldPicker = UITextField()
    var historyType = String()
    var arrHistoryData = NSMutableArray()
    var style = ToastStyle()
   
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.tableViewPayment.delegate = self
        self.tableViewPayment.dataSource = self
        self.txtTo.delegate = self
        self.txtFrom.delegate = self
        //Shadow.textShadow(view: self.viewTop)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.paymentList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK: - TextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtFrom
        {
            txtFieldPicker = txtFrom
        }
        else
        {
            txtFieldPicker = txtTo
        }
        pickUp(txtFieldPicker)
    }
    
    
    //MARK: - Datepicker
    func pickUp(_ textField : UITextField)
    {
        // UIPickerView
        myPicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        myPicker.datePickerMode = UIDatePickerMode.date
        myPicker.backgroundColor = UIColor.white
        myPicker.center = view.center
        textField.inputView = myPicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        txtFieldPicker.text = dateFormatter.string(from: myPicker.date)
        self.txtFieldPicker.resignFirstResponder()
    }
    @objc func cancelClick()
    {
        self.txtFieldPicker.resignFirstResponder()
    }
    
    
    
    
    
    //MARK: - IBActions
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func viewPayment(_ sender: UIButton)
    {
        let objects = self.arrPaymentsData.object(at: sender.tag) as! PaymentData
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentDetailsViewController") as! PaymentDetailsViewController
        controller.paymentDetails = objects.paymentDict
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnSearch(_ sender: Any)
    {
        if txtTo.text == "" || txtFrom.text == ""
        {
            AlertControl.alert(appmassage: "Please select any date", view: self)
        }
        else
        {
            paymentList()
        }
    }
    
    
    //MARK: - WebServiceMethods
    func paymentList()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            self.arrPaymentsData.removeAllObjects()
            let param = NSMutableDictionary()
            param.setValue(self.arrPaymentsData.count, forKey: "offset")
            param.setValue(self.arrPaymentsData.count + 15, forKey: "limit")
            
            let tokenUrl = ServiceUrl.customerUrl + "previousPaymemtList"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        /*
                         Amount = "<null>";
                         Category = Electrician;
                         Comment = "<null>";
                         Datetime = "2018-12-31T03:00:00";
                         Description = "<null>";
                         Location = "<null>";
                         Rating = "<null>";
                         RequestID = 152;
                         TechnicianId = 222;
                         TechnicianImageName = "F_Freelance.jpg";
                         TechnicianImageString = "http://180.151.232.92:124/ImageStorage/Freelancer/F_Freelance.jpg";
                         TechnicianName = "Freelance ";
                         
                         */
                        let arrData = response["Data"] as! NSArray
                        for dict in arrData
                        {
                            let dictData = dict as! NSDictionary
                            let paymentList = PaymentData()
                            paymentList.paymentDict = dictData
                            if let cat = dictData.value(forKey: "Category") as? String
                            {
                                paymentList.category = cat
                            }
                            if let nam:String = dictData.value(forKey: "TechnicianName") as? String
                            {
                                paymentList.name = nam
                            }
                            if let techId:NSNumber = dictData.value(forKey: "TechnicianId") as? NSNumber
                            {
                                paymentList.techId = techId
                            }else { paymentList.techId = 0 }
                            if let reqId:NSNumber = dictData.value(forKey: "RequestID") as? NSNumber
                            {
                                paymentList.reuestId = reqId
                            }else { paymentList.techId = 0 }
                            
                            if let dat = dictData.value(forKey: "Datetime") as? String
                            {
                                paymentList.date = dat
                            }else{
                                if let dat = dictData.value(forKey: "Datetime1") as? String
                                {
                                    paymentList.date = dat
                                }
                            }
                            if let time = dictData.value(forKey: "Time") as? String
                            {
                                paymentList.time = time
                            }
                            if let ratng = dictData.value(forKey: "Rating") as? NSNumber
                            {
                                paymentList.rating = ratng
                            }
                            else
                            {
                                paymentList.rating = 0
                            }
                            if let amt = dictData.value(forKey: "Amount") as? NSNumber
                            {
                                paymentList.amount = amt
                            }
                            else
                            {
                                paymentList.amount = 0
                            }
                            if let loc = dictData.value(forKey: "Location") as? String
                            {
                                paymentList.location = loc
                            }
                            if let comm = dictData.value(forKey: "Comment") as? String
                            {
                                paymentList.comments = comm
                            }
                            if let dateTime = dictData.value(forKey: "Datetime") as? String
                            {
                                paymentList.dateTime = dateTime
                            }
                            if let descriptn = dictData.value(forKey: "Description") as? String
                            {
                                paymentList.desc = descriptn
                            }
                            if let imageString = dictData.value(forKey: "TechnicianImageString") as? String
                            {
                                var str = imageString
                                
                                if let i = str.index(of: " ")
                                {
                                    str.remove(at: i)
                                }
                                
                                paymentList.image = str
                            }
                            
                            self.arrPaymentsData.add(paymentList)
                        }
                        if self.arrPaymentsData.count > 0
                        {
                            self.tableViewPayment.reloadData()
                        }
                        else
                        {
                            self.view.makeToast("Data not found", duration: 0.5, position: .center, style: self.style)
                            self.tableViewPayment.reloadData()
                        }
                    }
                    else
                    {
                        self.view.makeToast("Data not found", duration: 0.5, position: .center, style: self.style)
                        self.tableViewPayment.reloadData()
                    }
                    
                }
                else
                {
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    //MARK: - TableviewDelegates and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrPaymentsData.count > 0
        {
            return arrPaymentsData.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell") as! PaymentTableViewCell
        if(arrPaymentsData.count > 0 && arrPaymentsData.count > indexPath.row){
        let objects =  self.arrPaymentsData[indexPath.row] as! PaymentData
        //11-05-2018 11:10
        cell.lblName.text = objects.name as String
        cell.lblAmount.text =  String(describing: objects.amount)
        
        //let space = " "
       // let date = objects.date as String
        //        cell.lblDate.text = date.components(separatedBy: space).first
        //        cell.lblTime.text = date.components(separatedBy: space).last
        
        let date = ShowTime.dateConvert(date:objects.date as String)
        cell.lblDate.text = date as String
        cell.lblTime.text = ShowTime.showTime(time: objects.time as String)

        cell.lblCategory.text = objects.category
        
        if objects.image != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.image as String)
            if profileUrl != nil
            {
                cell.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.imgUser.image = placeholderImage
            }
            
        }
      
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(viewPayment(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    
    
   

}
