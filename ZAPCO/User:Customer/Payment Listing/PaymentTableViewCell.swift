//
//  PaymentTableViewCell.swift
//  ZAPCO
//
//  Created by vinove on 4/11/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnView: customButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
         Shadow.textShadow(view: self.viewCell)
        imgUser.layer.cornerRadius = self.imgUser.frame.size.width/2.0
        imgUser.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
