//
//  TechnicianDetailsViewController.swift
//  ZAPCO
//
//  Created by vinove on 11/6/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class TechnicianDetailsViewController: UIViewController {
   
    @IBOutlet weak var viewDetail: UIView!
    
    @IBOutlet weak var tableReview: UITableView!
    @IBOutlet weak var lblLicenseNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    var techDetails = NSDictionary()
    var arrReviewsData = NSMutableArray()
    var phoneNumber = NSString()
    @IBOutlet weak var viewTop: UIView!
   
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableReview.delegate = self
        self.tableReview.dataSource = self
         Shadow.textShadow(view: self.viewTop)
         Shadow.textShadow(view: self.viewDetail)
       
        self.imgUser.layer.cornerRadius = self.imgUser.frame.width/2.0
        self.imgUser.layer.masksToBounds = true
        
        print(techDetails)
        self.setUpData()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        
    }
    
   
    func setUpData(){
        
        self.lblName.text = (techDetails.value(forKey: "Name") as! String)
        if let license = techDetails.value(forKey: "LicenseNumber") as? NSString
        {
            self.lblLicenseNo.text = license as String
        }
      if  let arrReviews = techDetails.value(forKey: "CustomerReviewList") as? NSArray
      {
        for dict in arrReviews{
            
            let dictData = dict as! NSDictionary
            let reviews = TechReviews()
            
            if let comments =  dictData.value(forKey: "Comments") as? NSString{
                reviews.comment = comments
            }else{
                reviews.comment = ""
            }
            
            if let customrImage =  dictData.value(forKey: "CustomerImage") as? NSString{
                reviews.imageStr = customrImage
            }else{
                reviews.imageStr = ""
            }
            
            if let rating =  dictData.value(forKey: "Rating") as? NSInteger{
                reviews.rating = rating
            }else{
                reviews.rating = 0
            }
            
            // reviews.imageStr = dictData.value(forKey: "CustomerImage") as! NSString
            if let nam:NSString = dictData.value(forKey: "CustomerName") as? NSString
            {
                 reviews.name = nam
            }
           
            reviews.techID = dictData.value(forKey: "TechnicianId") as! NSInteger
             self.arrReviewsData.add(reviews)
        }
       
        }
           if let userImg = techDetails.value(forKey: "ProfileImagePath") as? String{
            
            let imgUrl = URL.init(string: userImg)
            let imgData = try? Data.init(contentsOf: imgUrl!)
            if imgData != nil
            {
                imgUser.image = UIImage.init(data: imgData!)
            }
            else
            {
               imgUser.image = UIImage.init(named: "user-1")
            }
            
        }
            if let phone = techDetails.value(forKey: "PhoneNumber") as? NSString
            {
               self.phoneNumber = phone
            }
        
        self.tableReview.reloadData()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    //MARK: - IBActions
    @IBAction func btnCall(_ sender: Any)
    {
        if let url = NSURL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url as URL)
        {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
   
    @IBAction func btnBook(_ sender: Any)
    {
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let viewPopUp = BookTechnician(frame: frame)
        viewPopUp.backgroundColor = UIColor.clear
        viewPopUp.technicianID = techDetails.value(forKey: "TechnicianId") as! NSInteger
        viewPopUp.categoryID = techDetails.value(forKey: "CategoryID") as! NSInteger
        viewPopUp.subCatID = techDetails.value(forKey: "SubCategoryID") as! NSInteger
        viewPopUp.technicianType = techDetails.value(forKey: "TechnicianType") as! NSString
        if let st:NSString = techDetails.value(forKey: "Category") as? NSString
        {
            viewPopUp.category = st
        }
        viewPopUp.strBookFrom = "TechDetail"
        
        viewPopUp.obj = self
        viewPopUp.userLoc.updateValue(LocationManager.manager.currentLocation.coordinate.latitude, forKey: "Latitude")
        viewPopUp.userLoc.updateValue(LocationManager.manager.currentLocation.coordinate.longitude, forKey: "Longitude") 
        self.view.addSubview(viewPopUp)
    }
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNotificationClick(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }

}

extension TechnicianDetailsViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrReviewsData.count > 0
        {
            return arrReviewsData.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "TechDetailsRviewTableViewCell") as! TechDetailsRviewTableViewCell
        let objects =  self.arrReviewsData[indexPath.row] as! TechReviews
        cell.lblName.text = objects.name as String
        cell.rating.rating = Double(objects.rating)
        if objects.comment != ""
        {
            cell.lblComment.text = objects.comment as String
        }
        
        if objects.imageStr != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.imageStr as String)
            if profileUrl != nil
            {
                cell.lblImage.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.lblImage.image = placeholderImage
            }
            
        }
        cell.rating.rating = Double(objects.rating)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
}
