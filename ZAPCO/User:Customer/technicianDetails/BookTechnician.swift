//
//  BookTechnician.swift
//  ZAPCO
//
//  Created by vinove on 1/4/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class BookTechnician: UIView,UITextFieldDelegate,UIPickerViewDelegate
{

    @IBOutlet weak var txtSlectDate: UITextField!
    @IBOutlet weak var txtSlectTime: UITextField!
    @IBOutlet weak var viewPopUp: UIView!
    var nibView: UIView!
    var datePicker = UIDatePicker()
    var showDate = Bool()
    var techData = NSDictionary()
    var userLoc = ["Latitude":345.980955, "Longitude":35.180955]
    var technicianID = NSInteger()
    var categoryID = NSInteger()
    var requestId = NSInteger()
    var reqNotificationId = NSInteger()
    
    var subCatID = NSInteger()
    var technicianType = NSString()
    var category = NSString()
    var name = NSString()
    var bookService = Bool()
    var amount = NSString()
    var obj :TechnicianDetailsViewController!
    var obj2 :HomeViewController!
    var strDate = String()
    var strTime = String()
    var strBookFrom = String()
    
    override func draw(_ rect: CGRect) {
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(viewTapped))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGestureRecognizer)
        self.datePicker.minimumDate = Date()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func instanceFromNib() -> UIView {
        return UINib(nibName: "BookTechnician", bundle: Bundle.main).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    func setup(){
//        txtSlectTime.delegate = self
//        txtSlectDate.delegate = self
     
        nibView = self.instanceFromNib()
        nibView.frame = bounds
        nibView.autoresizingMask = [UIViewAutoresizing.flexibleWidth ,UIViewAutoresizing.flexibleHeight]
        //nibView.backgroundColor = UIColor.clear
        addSubview(nibView)
    }
    
    func viewTapped(){
        self.removeFromSuperview()
    }
    
    @IBAction func btnBookClick(_ sender: Any)
    {
  // { "technicianType":"2", "CategoryID":"1","TechnicianId":"1", "SubCategoryID":"2", "Date":"","Time":"3:00:00","diatance":"", "Location":{"Latitude":"0.0", "Longitude":"0.0" }}
       
        if txtSlectDate.text == ""
        {
            let alert = UIAlertController(title: "ZAPCO", message: "Kindly Select Date", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok",style:UIAlertActionStyle.default,handler: nil))
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        else if txtSlectTime.text == ""
        {
            let alert = UIAlertController(title: "ZAPCO", message: "Kindly Select Time", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok",style:UIAlertActionStyle.default,handler: nil))
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        else
        {
        self.userLoc.updateValue(LocationManager.manager.currentLocation.coordinate.latitude, forKey: "Latitude")
        self.userLoc.updateValue(LocationManager.manager.currentLocation.coordinate.longitude, forKey: "Longitude")
        
        var techType = NSString()
        if self.technicianType == "Freelancer"
        {
            techType = "2"
        }
        else
        {
            techType = "1"
        }
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
         ProgressHud.progressBarDisplayer(indicator: true, view: self)
        let param = NSMutableDictionary()
       // param.setValue("", forKey: "diatance")
        param.setValue(technicianID, forKey: "TechnicianId")
        param.setValue(self.categoryID, forKey: "CategoryID")
        param.setValue(self.subCatID, forKey: "SubCategoryID")
        param.setValue(self.txtSlectDate.text, forKey: "Date")
        param.setValue(self.txtSlectTime.text, forKey: "Time")
        param.setValue(userLoc, forKey: "Location")
        param.setValue(techType, forKey: "TechnicianTypeId")
        
            //{"CategoryID":"2","Date":"07-24-2018","TechnicianTypeId":"4","Location":{"Latitude":"28.6811183","Longitude":"77.0853476"},"SubCategoryID":"3","TechnicianId":"2756","Time":"06:10:00"}
        
        let tokenUrl = ServiceUrl.customerUrl + "bookService"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param,  url: tokenUrl, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self)
            if(success)
            {
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    self.removeFromSuperview()
                    
                    let objects = ServiceHistory()
                    if let dictData:NSDictionary =  response["Data"] as? NSDictionary
                    {
                        if let reqId:NSInteger = dictData.value(forKey: "RequestId") as? NSInteger{
                          objects.reqID = reqId
                        self.reqNotificationId = reqId
                        } else {
                               objects.reqID = 0
                               self.reqNotificationId = 0  }

                        if let amt:NSString = dictData.value(forKey: "Amount") as? NSString
                        {
                             objects.amount = amt
                        }
                        objects.id = self.technicianID
                        let dt = self.strDate + "T" + self.txtSlectTime.text!
                        objects.dateTime = dt as NSString
                        objects.category = self.category
                        objects.longitude = self.userLoc["Longitude"]! as NSNumber
                        objects.latitude = self.userLoc["Latitude"]! as NSNumber
                    }
                    
                    self.bookNotification()
                
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let controller = storyBoard.instantiateViewController(withIdentifier: "ServiceHistoryDetailsViewController") as! ServiceHistoryDetailsViewController
                    controller.historyDetails = objects
                    UserDefaults.standard.set(false, forKey: "showRateView")
                    UserDefaults.standard.synchronize()
                    
                    if self.strBookFrom == "HomeView"
                    {
                        self.obj2.navigationController?.pushViewController(controller, animated: true)
                    }
                    else
                    {
                        self.obj.navigationController?.pushViewController(controller, animated: true)
                    }
                  }
                else
                {
                    
                    let alert = UIAlertController(title: "ZAPCO", message: response["Message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok",style:UIAlertActionStyle.default,handler: nil))
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                let alert = UIAlertController(title: "ZAPCO", message: "Try Again Later!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok",style:UIAlertActionStyle.default,handler: nil))
                
                self.removeFromSuperview()
            }
        }
        }
            else
            {
                let alert = UIAlertController(title: "ZAPCO", message: "No Internet Connection!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok",style:UIAlertActionStyle.default,handler: nil))
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                self.removeFromSuperview()
            }
    }
        
    }
    
    
     func bookNotification()
     {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true{
            var techType = NSString()
            if self.technicianType == "Freelancer"{
                techType = "2"
            }else{
                techType = "1"
            }
            ProgressHud.progressBarDisplayer(indicator: true, view: self)
            let param = NSMutableDictionary()
            param.setValue(userLoc, forKey: "Location")
            param.setValue(techType, forKey: "technicianType")
            param.setValue(reqNotificationId, forKey: "RequestId")
            param.setValue(technicianID, forKey: "TechnicianId")
            
            //{ "TechnicianId":"1392","RequestId":"1334",
           // "technicianType":"4", "Location":{"Latitude":"28.4515596", "Longitude":"77.0727665" }}
            
           
            let tokenUrl = ServiceUrl.customerUrl + "bookNotificationSend"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param,  url: tokenUrl, authentication : auth){
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        print("notification sent")
                    }else{
                        print("notification not sent")
                    }
                }
            }
            
        }else{
            let alert = UIAlertController(title: "ZAPCO", message: "No Internet Connection!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok",style:UIAlertActionStyle.default,handler: nil))
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            self.removeFromSuperview()
        }
    }
    
    
    
    @IBAction func btnCancelClick(_ sender: Any) {
         self.removeFromSuperview()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtSlectDate{
            self.showDate = true
            self.pickUpDate(self.txtSlectDate)
        }
        else if textField == txtSlectTime {
            self.showDate = false
            self.pickUpDate(self.txtSlectTime)
           // txtSlectTime.resignFirstResponder()
        }
        //  self.pickUpDate(self.txtSlectDate)
        //textField.resignFirstResponder()
    }
    
    @IBAction func selectDate(_ sender: UITextField) {
        
        //txtSlectDate.resignFirstResponder()
        
        
    }
    @IBAction func selectTime(_ sender: UITextField) {
        

       
    }
    func pickUpDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.minimumDate = Date()
        
        if showDate == true{
           self.datePicker.datePickerMode = UIDatePickerMode.date
        }else{
             self.datePicker.datePickerMode = UIDatePickerMode.time
        }
       
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    func doneClick()
    {
         //2018-05-10T09:32:00  05-31-201816:25:37
        let dateFormatter1 = DateFormatter()
        let dateformat = DateFormatter()
        
        if showDate == true
        {
            dateFormatter1.dateStyle = .medium
            dateFormatter1.dateFormat = "MM-dd-yyyy"
            txtSlectDate.text = dateFormatter1.string(from: datePicker.date)
            
            
            dateformat.dateFormat = "yyyy-MM-dd"
            strDate = dateformat.string(from: datePicker.date)
            //dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            txtSlectDate.resignFirstResponder()
        }else
        {
            dateFormatter1.timeStyle = .short
            dateFormatter1.dateFormat = "HH:mm:ss"
            txtSlectTime.text = dateFormatter1.string(from: datePicker.date)

            txtSlectTime.resignFirstResponder()
        }
       
    }
    func cancelClick() {
         if showDate == true{
             txtSlectDate.resignFirstResponder()
        }
         else{
           txtSlectTime.resignFirstResponder()
        }
       
    }
    
    
}
