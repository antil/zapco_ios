//
//  TechDetailsRviewTableViewCell.swift
//  ZAPCO
//
//  Created by vinove on 1/4/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class TechDetailsRviewTableViewCell: UITableViewCell {

    @IBOutlet weak var rating: UIRateView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblImage: UIImageView!
    
    
    @IBOutlet weak var viewCell: UIView!
    
    override func layoutSubviews() {
        self.lblImage.layer.cornerRadius = self.lblImage.frame.size.height/2.0
        self.lblImage.layer.masksToBounds = true
      Shadow.textShadow(view: self.viewCell)
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
