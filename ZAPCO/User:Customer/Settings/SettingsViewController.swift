//
//  SettingsViewController.swift
//  ZAPCO
//
//  Created by vinove on 3/27/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate
{

    //MARK: - Variables
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOldPasswd: UITextField!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var txtConfirmPasswd: UITextField!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var switchAvailability: UISwitch!
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    @IBOutlet weak var txtName: UITextField!
    var oldPasswd = String()
    var imagePicker = UIImagePickerController()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.imgUser.contentMode = .scaleAspectFill
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func setUpView()
    {
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
        self.imgUser.layer.borderWidth = 1.0
        self.imgUser.layer.borderColor = UIColor.lightGray.cgColor
       
        txtNewPassword.delegate = self
        txtOldPasswd.delegate = self
        txtConfirmPasswd.delegate = self
        txtName.delegate = self
        txtEmail.delegate = self
        txtPhoneNumber.delegate = self
        btnCornerChange.changeCorner2(button:self.btnLogout)
        btnCornerChange.changeCorner(button:self.btnProfile)
        Shadow.textShadow(view: self.viewTop)
        Shadow.textfieldShadow(textfield: txtOldPasswd)
        Shadow.textfieldShadow(textfield: txtConfirmPasswd)
        Shadow.textfieldShadow(textfield: txtNewPassword)
        
        let data = Shared.sharedInstance.userDetails
        let dictUserDetails = data?.userData
        print(dictUserDetails!)
        if dictUserDetails!["Name"] != nil
        {
            txtName.text = (dictUserDetails?["Name"] as? String)!
        }
        if dictUserDetails!["Email"] != nil
        {
            txtEmail.text = dictUserDetails?["Email"] as? String
        }
        if dictUserDetails!["PhoneNumber"] != nil
        {
            txtPhoneNumber.text = dictUserDetails?["PhoneNumber"] as? String
        }
       
        if let img = dictUserDetails?.value(forKey: "ProfileImageString") as? String
        {
                let placeholderImage = UIImage(named: "user-1")!
                let profileUrl = NSURL(string: img)
                if profileUrl != nil
                {
                    self.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
                }
                else
                {
                    self.imgUser.image = placeholderImage
                }
                
        }
        
        if let img = dictUserDetails?.value(forKey: "ImagePath") as? String
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: img)
            if profileUrl != nil
            {
                self.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                self.imgUser.image = placeholderImage
            }
            
        }

        if let passwd:String = Shared.sharedInstance.userDetails.userData["Password"] as? String
        {
            oldPasswd = passwd
        }
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
        viewBg.addGestureRecognizer(tap)
        
        let tapImage = UITapGestureRecognizer.init(target: self, action: #selector(handleImageTap))
        imgUser.addGestureRecognizer(tapImage)
    }
    
    
    func handleTap()
    {
       viewBg.isHidden = true
    }
    
    
    func handleImageTap()
    {
        upload()
    }
    
    
    //MARK: - UITextfieldDelgates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtOldPasswd
        {
            txtNewPassword.becomeFirstResponder()
        }
        else if textField == txtNewPassword
        {
            txtConfirmPasswd.becomeFirstResponder()
        }
        else
        {
        textField.resignFirstResponder()
        }
        return true
    }
    
    
    
    //MARK: - IBActions
    
    @IBAction func switchAvailable(_ sender: Any)
    {
        setNotification()
    }
    
    @IBAction func btnProfile(_ sender: Any)
    {
      
    }
    
    @IBAction func btnUpdateProfile(_ sender: Any)
    {
       updateProfile()
    }
    
    
    @IBAction func btnNotifiOn(_ sender: UIButton)
    {
        if sender.isSelected == true
        {
            sender.isSelected = false
            viewBg.isHidden = true
        }
        else
        {
            sender.isSelected = true
            viewBg.isHidden = false
        }
    }
    
    @IBAction func btnLogout(_ sender: Any)
    {
        let alert = UIAlertController(title: "Are you sure you want to", message: "Logout?", preferredStyle: UIAlertControllerStyle.alert)
        alert.setValue(NSAttributedString(string: alert.message!, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium), NSForegroundColorAttributeName : UIColor.red]), forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            self.logout()
        }))
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: nil))
        alert.view.tintColor = UIColor.init(red: 35.0/255.0, green: 15.0/255.0, blue: 120.0/255.0, alpha: 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnChangePassword(_ sender: Any)
    {
        if txtOldPasswd.text == ""
        {
            txtOldPasswd.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Old Password", view: self)
            
        }
        else if txtOldPasswd.text != oldPasswd
        {
            txtOldPasswd.becomeFirstResponder()
            AlertControl.alert(appmassage: "Old Password is wrong", view: self)
            
        }
        else if txtNewPassword.text == ""
        {
            txtNewPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter New Password", view: self)
            
        }
        else if txtConfirmPasswd.text == ""
        {
            txtConfirmPasswd.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Re-Enter Password", view: self)
            
        }
        else if txtConfirmPasswd.text != txtNewPassword.text
        {
            txtNewPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Password Not Matched", view: self)
        }
        
        else if ((txtNewPassword.text?.count)! < 6)
        {
            AlertControl.alert(appmassage: "Password should contain atleast 6 characters", view: self)
        }
            
        else
        {
            changePassword()
        }
    }
    
    
    @IBAction func btnNotification(_ sender: UIButton)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
        
    }
    
   
    //MARK: - UploadImageMethods
    func upload()
    {
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - ImagePickerDelegates
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:  [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        dismiss(animated: true, completion: nil)
        imgUser.image = chosenImage
    }
    
    
    
    
    
    //MARK: - WebServiceMethods
    func setNotification()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue("true", forKey: "sendNotification")
        
        let Url = ServiceUrl.customerUrl + "notificationSetting"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                }
            }
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
            
        
    }
    
    func changePassword()
    {
        self.view.endEditing(true)
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(txtOldPasswd.text, forKey: "oldPassword")
        param.setValue(txtNewPassword.text, forKey: "newPassword")
        
        let Url = ServiceUrl.CommonUrl + "changePassword"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                }
                else
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                }
            }
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    
    func updateProfile()
    {
        //{"email":"test3@gmail.com","name":"7896745655","phoneNumber":"7896745655","profileImage":""}

        txtName.resignFirstResponder()
        txtPhoneNumber.resignFirstResponder()
        txtEmail.resignFirstResponder()
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(txtEmail.text, forKey: "Email")
        param.setValue(txtName.text, forKey: "Name")
        param.setValue(txtPhoneNumber.text, forKey: "PhoneNumber")
        param.setValue(imageConvert(img: imgUser.image!), forKey: "ProfileImageString")
        
        let Url = ServiceUrl.customerUrl + "updateProfile"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){

            (response:Dictionary,success:Bool) in
            print(response)

            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    if let data = response["Data"] as? NSDictionary
                    {
                        let uData = Userdata()
                        uData.userData = data
                        Shared.sharedInstance.userDetails = uData
                        self.setUpView()
                    }
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                    
                }
            }
            else
            {
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
        }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    func imageConvert(img:UIImage)->String
    {
        let imageData: NSData = UIImageJPEGRepresentation(img, 0.4)! as NSData
        let imageStr = imageData.base64EncodedString(options:.lineLength64Characters)
        return imageStr
    }
    
    func logout()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let Url = ServiceUrl.CommonUrl + "logout"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                        ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                        
                        UserDefaults.standard.removeObject(forKey: "userData")
                        UserDefaults.standard.removeObject(forKey: stringConstants.tokenData)
                        UserDefaults.standard.removeObject(forKey: stringConstants.userData)
                        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "landingNavigation")
                        
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try Again Later!", view: self)
                }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
            }
}
