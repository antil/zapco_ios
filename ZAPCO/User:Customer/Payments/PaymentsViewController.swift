//
//  PaymentsViewController.swift
//  ZAPCO
//
//  Created by vinove on 11/7/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import PayU_coreSDK_Swift

class PaymentsViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{
    @IBOutlet weak var lblPaymentTitle: UILabel!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewDetails: UIView!
    @IBOutlet weak var txtPaymentMethod: UITextField!
    @IBOutlet weak var txtCardNo: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtCvv: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var viewCOD: UIView!
    @IBOutlet weak var txtZip: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtNameCOD: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    
    var arrShowData = NSMutableArray()
    var myPicker = UIPickerView()
    
    var txtfieldPicker = UITextField()
    var datePicker = MonthYearPickerView()
    var arrPicker = [String]()
    
    var paymentParams = PayUModelPaymentParams()
    let createRequest = PayUCreateRequest()
   
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Shadow.textShadow(view: self.viewTop)
        Shadow.textShadow(view: self.viewDetails)
        Shadow.textShadow(view: self.viewCOD)
        
        txtDate.delegate = self
        txtCvv.delegate = self
        txtName.delegate = self
        txtCardNo.delegate = self
        txtPaymentMethod.delegate = self
        txtZip.delegate = self
        txtCity.delegate = self
        txtAddress.delegate = self
        txtNameCOD.delegate = self
        
        
        let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        let image = UIImage(named: "dropdown")
        imgV.image = image
        txtPaymentMethod.rightViewMode = UITextFieldViewMode.always
        txtPaymentMethod.rightView = imgV
       
        arrPicker = ["Card",  "COD(Cash on delivery)"]
        
        let data = Shared.sharedInstance.userDetails
        let dictUserDetails = data?.userData
        print(dictUserDetails!)
        if dictUserDetails!["Address"] != nil
        {
            txtAddress.text = (dictUserDetails?["Address"] as? String)!
        }
        if dictUserDetails!["Name"] != nil
        {
            txtNameCOD.text = dictUserDetails?["Name"] as? String
        }
        
        txtCity.text = UserDefaults.standard.value(forKey: "city") as? String
        txtZip.text = UserDefaults.standard.value(forKey: "zipcode") as? String
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    
    //MARK: - TextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtCardNo
        {
            addDoneButtonOnKeyboard(txtCardNo)
        }
        if textField == txtCvv
        {
            addDoneButtonOnKeyboard(txtCvv)
        }
        if (textField == txtPaymentMethod)
        {
            self.pickUp(txtPaymentMethod)
        }
        
        if textField == txtDate
        {
            pickUpDate(txtDate)
        }
        if textField == txtZip
        {
            addDoneButtonOnKeyboard(txtZip)
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        textField.resignFirstResponder()
        myPicker.isHidden = true
    }
    
    //MARK: - KeyboardCustomization
    func addDoneButtonOnKeyboard(_ txt: UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width: 320, height:50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        txt.inputAccessoryView = doneToolbar
    }
    func doneButtonAction()
    {
        txtCvv.resignFirstResponder()
        txtCardNo.resignFirstResponder()
        txtZip.resignFirstResponder()
    }
    
    
    
    //MARK: - DatePickerMethods
    func pickUpDate(_ textField : UITextField)
    {
        // DatePicker
        self.datePicker = MonthYearPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        
        textField.inputView = self.datePicker
       // self.datePicker.datePickerMode = UIDatePickerMode.date
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func doneClick()
    {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM/yyyy"

        let string = String(format: "%02d/%d", datePicker.month, datePicker.year)
        self.txtDate.text = string
        txtDate.resignFirstResponder()
    }
    
    func cancelClick()
    {
        txtDate.resignFirstResponder()
    }
    
    
    
    //MARK: - UIPickerViewMethods
    func pickUp(_ textField : UITextField)
    {
        // UIPickerView
        myPicker = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        myPicker.delegate = self
        myPicker.dataSource = self
        myPicker.backgroundColor = UIColor.white
        textField.inputView = myPicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.doneClick1))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick1))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick1()
    {
            if txtPaymentMethod.text == "Select payment method"{
                txtPaymentMethod.text = arrPicker[0]
        }
        txtPaymentMethod.resignFirstResponder()
    }
    @objc func cancelClick1()
    {
        txtPaymentMethod.resignFirstResponder()
    }
    
    
    
    //MARK: - UIPickerViewDatasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arrPicker.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return arrPicker[row]
    }
    
    
    //MARK: - UIPickerviewDelegates
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent  component: Int)
    {
        txtPaymentMethod.resignFirstResponder()
        txtPaymentMethod.text = arrPicker[row]
        if row == 1
        {
            viewCOD.isHidden = false
            viewDetails.isHidden = true
            lblPaymentTitle.text = "Enter shipping Details"
        }
        else
        {
            viewCOD.isHidden = true
            viewDetails.isHidden = false
            lblPaymentTitle.text = "Enter Card Details"
        }
    }
    
    
    
    //MARK: - IBActions
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPay(_ sender: Any)
    {
        if viewCOD.isHidden == false
        {
            if txtPaymentMethod.text == "Select payment method "
            {
                AlertControl.alert(appmassage: "Please select Payment Method", view: self)
            }
            else if txtNameCOD.text?.count == 0 || txtAddress.text?.count == 0 || txtCity.text?.count == 0 || txtZip.text?.count == 0
            {
                AlertControl.alert(appmassage: "Field cannot be empty", view: self)
            }
            else
            {
           payCOD()
            }
        }
        
        //txtPaymentMethod.text = "abc"
        else{
        if txtPaymentMethod.text == "Select payment method "
        {
            AlertControl.alert(appmassage: "Please select Payment Method", view: self)
        }
        else if txtCardNo.text == nil || txtCardNo.text?.isValidCardNumber() == false
        {
            AlertControl.alert(appmassage: "Invalid Card Number", view: self)
        }
        else if txtCvv.text == nil || !txtCvv.text!.isNumeric || txtCvv.text!.count > 4
        {
            AlertControl.alert(appmassage: "Invalid cvv", view: self)
        }
        else if txtDate.text == nil || txtDate.text?.count == 0{
            AlertControl.alert(appmassage: "Invalid Date", view: self)
        }
        else
        {
            //  AlertControl.alert(appmassage: "In Progress", view: self)
            
            let dateString:NSString = txtDate.text! as NSString
            
            self.paymentParams.cardNumber =   self.txtCardNo.text ?? "4012001037141112"
            self.paymentParams.expiryMonth =   dateString.substring(to: 2)  //"01"
            self.paymentParams.expiryYear = dateString.substring(from: 3)  //2020
            self.paymentParams.CVV = txtCvv.text //"123"
            self.paymentParams.amount = "1"
            /*
             VISA CREDIT CARD
             Card No:4012 0010 3714 1112
             Exp:12/2020
             CVV:123
 */
            
            
            let shackyClassObject = shackyClass()
            shackyClassObject.generateHashesFromShackyClass(paymentParams: paymentParams, withSalt: merchantSalt)
            self.createRequest.createRequest(withPaymentParam: self.paymentParams, forPaymentType: PAYMENT_PG_CCDC) { (request, error) in
                
                if (error == "")
                {
                    DispatchQueue.main.async {
                        self.gotoWebView(req: request)
                    }
                }
                    
                else
                {
                    let alert = UIAlertController(title: "oops !", message: error as String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            }
        }
 
    }
    func gotoWebView(req:NSMutableURLRequest)  {
        let strBrd = UIStoryboard(name: "Main", bundle: nil)
        
        let webViewVC = strBrd.instantiateViewController(withIdentifier: "PUUIWebViewVC") as! PUUIWebViewVC
        
        webViewVC.request = req
        
        self.navigationController?.pushViewController(webViewVC, animated: true)
    }
    
    
    
    // MARK: - webServices
    func payCOD()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            param.setValue(requestIdForPayment, forKey: "RequestId")
            param.setValue(paymentParams.amount, forKey: "Amount")
            param.setValue(txtAddress.text, forKey: "Address")
            param.setValue(txtName.text, forKey: "name")
            param.setValue(txtCity.text, forKey: "city")
            param.setValue(txtZip.text, forKey: "zipCode")
            
            let jsonData = try! JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions.prettyPrinted)
            var jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            jsonString = jsonString.trimmingCharacters(in: CharacterSet.whitespaces)
            print(jsonString)
            
            let tokenUrl = ServiceUrl.customerUrl + "COD"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                      self.navigationController?.popToRootViewController(animated: true)
                    }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }

    

}


extension String
{
    var isNumeric: Bool
    {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    func isValidCardNumber() -> Bool {
        do {
            try CardValidator.performLuhnAlgorithm(with: self)
            return true
        }
        catch {
            return false
        }
    }
    
    func cardType() -> CardValidator.CardType?
    {
        let cardType = try? CardValidator.cardType(for: self)
        return cardType
    }
    
    func suggestedCardType() -> CardValidator.CardType? {
        let cardType = try? CardValidator.cardType(for: self, suggest: true)
        return cardType
    }
    
    func formattedCardNumber() -> String
    {
        let numbersOnlyEquivalent = replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression, range: nil)
        return numbersOnlyEquivalent.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    
    
    
}






