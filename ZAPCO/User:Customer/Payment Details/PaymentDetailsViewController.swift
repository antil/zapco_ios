//
//  PaymentDetailsViewController.swift
//  ZAPCO
//
//  Created by vinove on 11/7/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class PaymentDetailsViewController: UIViewController, UITextViewDelegate {

    var paymentDetails = NSDictionary()
    
    @IBOutlet weak var lblTechID: UILabel!
    @IBOutlet weak var lblReqNo: UILabel!
    @IBOutlet weak var lblReqDT: UILabel!
    @IBOutlet weak var lblReqType: UILabel!
    @IBOutlet weak var lblPaidAmt: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblETA: UILabel!
   
    @IBOutlet weak var txtVComments: UITextView!
    @IBOutlet weak var viewRatings: UIRateView!
    @IBOutlet weak var viewReq: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewRating: UIView!
    
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initialSetup()
    }
    
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func initialSetup(){
        
        Shadow.textShadow(view: self.viewTop)
        Shadow.textShadow(view: self.viewReq)
        Shadow.textShadow(view: self.viewRating)
        txtVComments.delegate = self
        
        self.lblReqNo.text = "\(paymentDetails["RequestID"]!)"
        self.lblReqType.text = self.paymentDetails["Category"] as? String
       
        if let amt = self.paymentDetails["Amount"]
        {
            self.lblPaidAmt.text = amt as? String
        }
        // self.lblETA.text = amt
        if let date = self.paymentDetails["Datetime"] as? String
        {
            let dateToShow = ShowTime.dateConvert(date:date as String)
            var time = String()
            if let tym:String = self.paymentDetails["Time"] as? String
            {
                time = ShowTime.showTime(time: tym)
            }
            self.lblReqDT.text = dateToShow +  " at " + time
            
           // self.lblReqDT.text = ShowTime.dateConvert(date: d) + " at " + ShowTime.timeConvert(date: d)
        }
        self.lblTechID.text = "\(paymentDetails["TechnicianId"]!)"
        if let com = self.paymentDetails["Comment"] as? String
        {
            self.txtVComments.text = com
        }
        if let loc = self.paymentDetails["Location"] as? String
        {
            self.txtVComments.text = loc
        }
        
        if let ratng = self.paymentDetails["Rating"] as? NSNumber
        {
            viewRatings.rating = Double(Int(ratng))
        }
        
    }
    
    //MARK: - UITextViewDelegates
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        txtVComments.resignFirstResponder()
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    //MARK: - IBActions
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit(_ sender: Any)
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            param.setValue(self.lblTechID.text, forKey: "technicianId")
            param.setValue(self.lblReqNo.text, forKey: "requestId")
            param.setValue(viewRatings.rating, forKey: "rating")
            param.setValue(txtVComments.text, forKey: "comments")
            
            let Url = ServiceUrl.customerUrl + "technicianFeedback"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success)
                {
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
   
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func btnSendInvoice(_ sender: Any)
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            param.setValue(self.lblReqNo.text, forKey: "requestId")
            
            let Url = ServiceUrl.customerUrl + "requestInvoice"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
}
