//
//  QuotesListCustmrTableViewCell.swift
//  ZAPCO
//
//  Created by vinove on 11/24/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class QuotesListCustmrTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnView: customButton!

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func layoutSubviews() {
        Shadow.textShadow(view: self.viewCell)
       imgUser.layer.cornerRadius = self.imgUser.frame.size.width/2.0
       imgUser.layer.masksToBounds = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
