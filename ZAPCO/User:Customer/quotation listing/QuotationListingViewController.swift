//
//  QuotationListingViewController.swift
//  ZAPCO
//
//  Created by vinove on 11/6/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Toast_Swift
import AlamofireImage
import PayU_coreSDK_Swift

class QuotationListingViewController: UIViewController
{
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var tableQuotes: UITableView!
    var arrShowData = NSMutableArray()
    
    var paymentParams = PayUModelPaymentParams()
    let webService = PayUWebService()
    
    var style = ToastStyle()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableQuotes.delegate = self
        self.tableQuotes.dataSource = self
        Shadow.textShadow(view: self.viewTop)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.arrShowData.removeAllObjects()
        self.showQuotes()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    func generateTxnID() -> String {
        
        let currentDate = DateFormatter()
        currentDate.dateFormat = "yyyyMMddHHmmss"
        let date = NSDate()
        let dateString = currentDate.string(from : date as Date)
        return dateString
        
    }
    //MARK: - WebServiceMethods
    func showQuotes()
    {
        arrShowData.removeAllObjects()
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(self.arrShowData.count, forKey: "offset")
        param.setValue(self.arrShowData.count + 15, forKey: "limit")
       
        let tokenUrl = ServiceUrl.customerUrl + "quotations"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    
                    let arrData = response["Data"] as! NSArray
                    for dict in arrData{
                        let dictData = dict as! NSDictionary
                        let techReq = CustomerQuotesList()
                        techReq.quotesDict = dictData
                        if let cat = dictData.value(forKey: "Category") as? String
                        {
                            techReq.category = cat
                        }
                        
                        if let dt = dictData.value(forKey: "Datetime") as? String
                        {
                            techReq.dateTime = dt
                        }
                        if let tim = dictData.value(forKey: "TimeLog") as? String
                        {
                            techReq.time = tim
                        }
                        if let com = dictData.value(forKey: "Comment") as? String{
                            techReq.comment = com
                        }
                        if let m = dictData.value(forKey: "Amount") as? NSNumber{
                            techReq.amount = m
                        }
                        else
                        {
                            techReq.amount = 0
                        }
                        if let m = dictData.value(forKey: "TechnicianId") as? NSNumber
                        {
                            techReq.techID = m
                        }
                        if let loc = dictData.value(forKey: "Location") as? String
                        {
                            techReq.location = loc
                        }
                        if let des = dictData.value(forKey: "Description") as? String
                        {
                            techReq.descrip = des
                        }
                        if let img = dictData.value(forKey: "TechnicianImageString") as? String{
                            techReq.reqImage = img
                        }
                        techReq.name = dictData.value(forKey: "TechnicianName") as! String
                        if let m = dictData.value(forKey: "RequestID") as? NSNumber
                        {
                            techReq.requestId = m
                        }
                       
                        if let rat = dictData.value(forKey: "Rating") as? NSNumber{
                            techReq.rating = rat
                        }
                        else
                        {
                            techReq.rating = 0
                        }
                        
                        self.arrShowData.add(techReq)
                    }
                    
                    if self.arrShowData.count == 0{
                        self.tableQuotes.reloadData()
                        self.view.makeToast("There is No quotes to show!")
                        
                    }else{
                        
                        self.tableQuotes.reloadData()
                    }
                    
                }
                else if (response["Code"] as AnyObject).integerValue == 202
                {
                    self.view.makeToast("No Quotations exist between specified time period", duration: 0.5, position: .center, style: self.style)
                    
                }
            }
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }

    
    
    @IBAction func btnViewDetail(_ sender: UIButton)
    {
        let objects = self.arrShowData.object(at: sender.tag) as! CustomerQuotesList
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CustomerQuoteDetailsViewController") as! CustomerQuoteDetailsViewController
        controller.quotesDetails = objects.quotesDict
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
//    @IBAction func btnAddClick(_ sender: UIButton)
//    {
//        let objects = self.arrShowData.object(at: sender.tag) as! QuotesList
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddQuotesViewController") as! AddQuotesViewController
//        controller.strReqN0 = String(describing: objects.requestId)
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
//
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CustomerQuoteDetailsViewController") as! CustomerQuoteDetailsViewController
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
//
    
    //MARK: - IBActions
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
}

extension QuotationListingViewController: UITableViewDelegate,UITableViewDataSource
{
   
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrShowData.count > 0
        {
            return arrShowData.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuotesListCustmrTableViewCell") as! QuotesListCustmrTableViewCell
        if(arrShowData.count > 0 && arrShowData.count > indexPath.row){
        let objects = self.arrShowData[indexPath.row] as! CustomerQuotesList
        
        cell.lblName.text = objects.name as String
        cell.lblCategory.text = objects.category as String
        let date = ShowTime.dateConvert(date:objects.dateTime as String)
        cell.lblDate.text = date as String
        if objects.time != ""{
        cell.lblTime.text = ShowTime.showTime(time: objects.time)
        }
        if objects.reqImage != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.reqImage as String)
            if profileUrl != nil
            {
                cell.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.imgUser.image = placeholderImage
            }
            
        }
        
        
        cell.btnView.tag = indexPath.item
        cell.btnView.addTarget(self, action: #selector(btnViewDetail(_:)), for: .touchUpInside)
        }
            return cell
    }
}
