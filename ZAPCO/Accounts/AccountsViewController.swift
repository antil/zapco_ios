//
//  AccountsViewController.swift
//  ZAPCO
//
//  Created by vinove on 10/24/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class AccountsViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate
{
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var switchSetAvialbility: UISwitch!
    @IBOutlet weak var imgIDCard: UIImageView!
    @IBOutlet weak var imgZapcoLicense: UIImageView!
    @IBOutlet weak var btnEdit: customButton!
    @IBOutlet weak var btnLogout: customButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtDscr: UITextField!
    @IBOutlet weak var txtPermit: UITextField!
    
    @IBOutlet weak var viewTop: UIView!
    
    var status = Bool()
    var imagePicker = UIImagePickerController()
    var location = ["Latitude":"28.980955", "Longitude":"77.180955" ]
    var strAddress = String()
    var strDocString = String()
    var strPermitString = String()
    var strImgString = String()
    var selectProfile = Bool()
    var selectDoc1 = Bool()
    var selectDoc2 = Bool()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUpViw()
        self.txtName.delegate = self
        self.txtEmail.delegate = self
        self.txtPhone.delegate = self
        self.txtCity.delegate = self
        self.txtDscr.delegate = self
        self.txtPermit.delegate = self
        
        Shadow.textShadow(view: self.viewTop)
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleImgProfileTap))
        imgUser.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(handleImgDoc1Tap))
        imgIDCard.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(handleImgDoc2Tap))
        imgZapcoLicense.addGestureRecognizer(tap3)
        
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            getData()
        }
        else
        {
            let data = Shared.sharedInstance.userDetails
            let dictUserDetails = data?.userData
            print(dictUserDetails!)
            if dictUserDetails!["Name"] != nil
            {
                txtName.text = dictUserDetails?["Name"] as? String
            }
            if dictUserDetails!["Email"] != nil
            {
                txtEmail.text = dictUserDetails?["Email"] as? String
            }
            if dictUserDetails!["PhoneNumber"] != nil
            {
                txtPhone.text = dictUserDetails?["PhoneNumber"] as? String
            }
            if dictUserDetails!["PermitNumber"] != nil
            {
                txtPermit.text = dictUserDetails?["PermitNumber"] as? String
            }
            if dictUserDetails!["Description"] != nil
            {
                txtDscr.text = dictUserDetails?["Description"] as? String
            }
            
            if dictUserDetails!["City"] != nil
            {
                txtCity.text = dictUserDetails?["City"] as? String
            }
            
            if let img = dictUserDetails?.value(forKey: "ImageString") as? String
            {

                let placeholderImage = UIImage(named: "user-1")!
                let profileUrl = NSURL(string: img as String)
                if profileUrl != nil
                {
                    self.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
                }
                else
                {
                    self.imgUser.image = placeholderImage
                }
            }
            else{
                self.imgUser.image = UIImage(named:"user-1")
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        btnEdit.isSelected = false
        self.imgUser.contentMode = .scaleAspectFill
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUpViw()
    {
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
        self.imgUser.layer.borderWidth = 1.0
        self.imgUser.layer.borderColor = UIColor.lightGray.cgColor
        
        Shadow.buttonShadow(view: self.btnEdit)
        Shadow.buttonShadow(view: self.btnLogout)
        Shadow.textShadow(view: self.viewBase)
    }
    
    func handleImgProfileTap()
    {
        selectProfile = true
        selectDoc1 = false
        selectDoc2 = false
        upload()
    }
    
    func handleImgDoc1Tap()
    {
        selectProfile = false
        selectDoc1 = true
        selectDoc2 = false
        upload()
    }
    
    func handleImgDoc2Tap()
    {
        selectProfile = false
        selectDoc1 = false
        selectDoc2 = true
        upload()
    }
    
    //MARK: - UITextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtPhone{
            self.addDoneButtonOnKeyboard()
        }
    }
    
    //MARK: - KeyboardCustomization
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width: 320, height:50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        txtPhone.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.txtPhone.resignFirstResponder()
        
    }
    

    //MARK: - IBActions
    @IBAction func btnEdit(_ sender: customButton)
    {
       
        if sender.isSelected == false{
            self.txtName.becomeFirstResponder()
            self.txtName.isEnabled = true
            self.txtEmail.isEnabled = true
            self.txtPhone.isEnabled = true
            self.txtCity.isEnabled = true
            self.txtDscr.isEnabled = true
            self.txtPermit.isEnabled = true
            self.imgUser.isUserInteractionEnabled = true
            self.imgZapcoLicense.isUserInteractionEnabled = true
            self.imgIDCard.isUserInteractionEnabled = true
            self.btnEdit.setTitle("Save", for: .normal)
            sender.isSelected = true
            
        }else{
            self.txtName.isEnabled = false
            self.txtEmail.isEnabled = false
            self.txtPhone.isEnabled = false
            self.txtCity.isEnabled = false
            self.txtDscr.isEnabled = false
            self.txtPermit.isEnabled = false
            self.imgUser.isUserInteractionEnabled = false
            self.imgZapcoLicense.isUserInteractionEnabled = false
            self.imgIDCard.isUserInteractionEnabled = false
            self.btnEdit.setTitle("Edit", for: .normal)
            sender.isSelected = false
            self.saveAccount()
        }
  
    }
    
    @IBAction func switchStatus(_ sender: Any)
    {
      updateWorkStatus()
    }
   
    
    @IBAction func btnLogout(_ sender: Any)
    {
        let alert = UIAlertController(title: "Are you sure you want to", message: "Logout?", preferredStyle: UIAlertControllerStyle.alert)
        alert.setValue(NSAttributedString(string: alert.message!, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium), NSForegroundColorAttributeName : UIColor.red]), forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            self.logout()
        }))
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: nil))
        alert.view.tintColor = UIColor.init(red: 35.0/255.0, green: 15.0/255.0, blue: 120.0/255.0, alpha: 1.0)
        self.present(alert, animated: true, completion: nil)

    }
    
    @IBAction func btnReq(_ sender: Any) {
        
        let searchResult = self.storyboard?.instantiateViewController(withIdentifier: "RequestListViewController") as! RequestListViewController
        self.navigationController?.pushViewController(searchResult, animated: true)
    }
    @IBAction func btnQuotes(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "QuotesViewController") as! QuotesViewController
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
  
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
   
    
    //MARK: - UploadImageMethods
    func upload()
    {
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - ImagePickerDelegates
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:  [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        dismiss(animated: true, completion: nil)
        if (selectProfile == true)
        {
            imgUser.image = chosenImage
            imgUser.contentMode = .scaleAspectFill
        }
        else if (selectDoc1 == true)
        {
            imgIDCard.image = chosenImage
        }
        else
        {
            imgZapcoLicense.image = chosenImage
        }
        
    }
    
    
    //MARK: - WebServiceMethods
    func getData(){
      
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let Url = ServiceUrl.companyTechnicianUrl + "account"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                    let dictData = response["Data"] as! NSDictionary
                    
                    if let img = dictData.value(forKey: "ImageString") as? String
                    {
                        self.strImgString = img

                        let placeholderImage = UIImage(named: "user-1")!
                        let profileUrl = NSURL(string: img)
                        if profileUrl != nil
                        {
                            self.imgUser.af_setImage(withURL: profileUrl as! URL, placeholderImage: placeholderImage)
//                            DispatchQueue.main.async(execute: { () -> Void in
//                                let data = try? Data(contentsOf: profileUrl! as URL) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//                                self.imgUser.image = UIImage(data: data!)
//                            })
                        }
                        else
                        {
                            self.imgUser.image = placeholderImage
                        }
                    }
                    else{
                        self.imgUser.image = UIImage(named:"user-1")
                    }
                    
                    if let nam:String = dictData.value(forKey: "Name") as? String{
                        self.txtName.text = nam
                    }else { self.txtName.text = "" }
                    
                    if let em:String = dictData.value(forKey: "Email") as? String{
                        self.txtEmail.text = em
                    }else { self.txtEmail.text = "" }
                    
                    if let cit:String = dictData.value(forKey: "City") as? String{
                        self.txtCity.text = cit
                    }else { self.txtCity.text = "" }
                    
                    if let desc:String = dictData.value(forKey: "Description") as? String{
                        self.txtDscr.text = desc
                    }else { self.txtDscr.text = "" }
                    
                    if let per:String = dictData.value(forKey: "PermitNumber") as? String{
                        self.txtPermit.text = per
                    }else { self.txtPermit.text = "" }
                    
                    if let add:String = dictData.value(forKey: "Address") as? String{
                        self.strAddress = add
                    }else { self.strAddress = "" }
                    
                    if let _:String = dictData.value(forKey: "PhoneNumber") as? String{
                        self.txtPhone.text = String(describing: dictData.value(forKey: "PhoneNumber") as! NSString)
                    }else { self.txtPhone.text = "" }
                    
                    
                    self.location.updateValue(String(describing: dictData.value(forKey: "Latitude")), forKey: "Latitude")
                    self.location.updateValue(String(describing: dictData.value(forKey: "Longitude")), forKey: "Longitude")
                    
            
                            if let img = dictData.value(forKey: "DocumentImageString") as? String
                            {
                                self.strDocString = img
                
                                let placeholderImage = UIImage(named: "zapco_logo")!
                                let profileUrl = NSURL(string: img)
                                if profileUrl != nil
                                {
                                    self.imgIDCard.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
//                                    DispatchQueue.main.async(execute: { () -> Void in
//                                        let data = try? Data(contentsOf: profileUrl! as URL) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//                                        self.imgIDCard.image = UIImage(data: data!)
//                                    })
                                }
                                else
                                {
                                    self.imgIDCard.image = placeholderImage
                                }
                            }
                            else
                            {
                                self.imgIDCard.image = UIImage(named:"zapco_logo")
                            }
                            
                            if let img = dictData.value(forKey: "PermitImageString") as? String
                            {
                                self.strPermitString = img
                                
                                let placeholderImage = UIImage(named: "zapco_logo")!
                                let profileUrl = NSURL(string: img)
                                if profileUrl != nil
                                {
                                    self.imgZapcoLicense.af_setImage(withURL: profileUrl as! URL, placeholderImage: placeholderImage)
//                                    DispatchQueue.main.async(execute: { () -> Void in
//                                        let data = try? Data(contentsOf: profileUrl! as URL) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//                                        self.imgZapcoLicense.image = UIImage(data: data!)
//                                    })
                                }
                                else
                                {
                                    self.imgZapcoLicense.image = placeholderImage
                                }
                                
                            }
                            else{
                                self.imgZapcoLicense.image = UIImage(named:"zapco_logo")
                            }
                            
           
                }
                
            }
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }
        }
        else
        {
            //AlertControl.alert(appmassage: "No Internet Connection!", view: self)
            return
        }
    }
    
    
    
    func updateWorkStatus()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(status, forKey: "workStatus")
        
        let Url = ServiceUrl.companyTechnicianUrl + "updateWorkStatus"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                }
            }
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    
    func saveAccount(){
        ///updateAccount        
//        { "name":"TechnicainTestin ", "email":"TechnicainTesting5207apt@gmail.com","location": {
//            "Longitude": 77.085361,
//            "Latitude": 28.6809848
//            }, "ImageString":"", "DocumentImageString":"","description":"13:00:00", "permitNumber":"A1276897675", "PermitImageString":"","Address":"jcijkldgfjgldfkhg;kkkk"}

        // "name":"Kumar vishwas", "email":"kumar@gmail.com", "profileImage":"", "phoneNumber":"980156883", "city":"Gurugram", "description":"13:00:00", "permitNumber":"A1276897675", "permitImage":"", "documentImage":""}
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            self.location.updateValue(String(LocationManager.manager.currentLocation.coordinate.latitude), forKey: "Latitude")
            self.location.updateValue(String(LocationManager.manager.currentLocation.coordinate.longitude), forKey: "Longitude")
        
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(self.txtName.text, forKey: "name")
        param.setValue(self.txtEmail.text, forKey: "email")
        param.setValue(self.txtPhone.text, forKey: "phoneNumber")
       // param.setValue(self.txtCity.text, forKey: "city")
        param.setValue(self.txtDscr.text, forKey: "description")
        param.setValue(self.txtPermit.text, forKey: "permitNumber")
        param.setValue(self.location, forKey: "location")
        param.setValue(self.imageConvert(img: self.imgUser), forKey: "ImageString")
        param.setValue(self.imageConvert(img: self.imgZapcoLicense), forKey: "PermitImageString")
        param.setValue(self.imageConvert(img: self.imgIDCard), forKey: "DocumentImageString")
        param.setValue(self.strAddress, forKey: "Address")
        print(param)
        
        let Url = ServiceUrl.companyTechnicianUrl + "updateAccount"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                    self.getData()
                }
            }
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    func imageConvert(img:UIImageView)->String{
        let imageData: NSData = UIImageJPEGRepresentation(img.image!, 0.4)! as NSData
        let imageStr = imageData.base64EncodedString(options:.lineLength64Characters)
        
        return imageStr
    }
    
    
    func logout()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let Url = ServiceUrl.CommonUrl + "logout"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                    ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                    
                    UserDefaults.standard.removeObject(forKey: "userData")
                    UserDefaults.standard.removeObject(forKey: stringConstants.tokenData)
                    UserDefaults.standard.removeObject(forKey: stringConstants.userData)
                    let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                    rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "landingNavigation")
                    
                    
                }
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    
}
