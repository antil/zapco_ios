//
//  ReqdetailsCompletedViewController.swift
//  ZAPCO
//
//  Created by vinove on 4/9/18.
//  Copyright © 2018 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class ReqdetailsCompletedViewController: UIViewController {
    
    var reqdetails = TechReqList()

    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblTaskStatus: UILabel!
    @IBOutlet weak var lblReqFrom: UILabel!
    @IBOutlet weak var reqFor: UILabel!
    @IBOutlet weak var reqNo: UILabel!
    @IBOutlet weak var lblPaymntStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDescr: UILabel!
    @IBOutlet weak var lblCostEstimate: UILabel!
    @IBOutlet weak var lblTotalCost: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        print(reqdetails)
        self.setUpViw()
    }
    
    
    func setUpViw()
    {
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
        self.imgUser.layer.borderWidth = 1.0
        self.imgUser.layer.borderColor = UIColor.lightGray.cgColor
        Shadow.textShadow(view: self.viewTop)
        Shadow.textShadow(view: self.viewBase)
        if reqdetails.image != ""
        {
            let imgUrl = URL.init(string: reqdetails.image as String)
            let imgData = try? Data.init(contentsOf: imgUrl!)
            if imgData != nil
            {
                self.imgUser.image  = UIImage.init(data: imgData!)
            }
            else
            {
                self.imgUser.image = UIImage.init(named: "user-1")
            }
        }
        print(Shared.sharedInstance.userDetails.userData)
        self.lblReqFrom.text = (self.reqdetails.name as String)
        self.reqFor.text = (self.reqdetails.category as String)
        self.reqNo.text = "\(reqdetails.requestId)"
        
        let date = ShowTime.dateConvert(date:self.reqdetails.dateTime as String)
        self.lblDate.text = date as String
        self.lblTime.text = ShowTime.timeConvert(date:self.reqdetails.dateTime as String)
  
            
            if reqdetails.descrip != ""
            {
                self.lblDescr.text = reqdetails.descrip as String
            }
            else{
                self.lblDescr.text = ""
            }
        
        if reqdetails.status != 0
        {
            if reqdetails.status == 2
            {
             self.lblPaymntStatus.text = "PAID"
            }
         //self.lblPaymntStatus.text = "\(reqdetails.status)"
        }
        if reqdetails.amount != 0
        {
            self.lblCostEstimate.text = "\(reqdetails.amount)"
            self.lblTotalCost.text = "\(reqdetails.amount)"
        }else{
           
            self.lblCostEstimate.text = "Not Applicable"
            self.lblTotalCost.text = "Not Applicable"
        }
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: - IBActions
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func btnReqClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnQuotesClick(_ sender: Any)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "QuotesViewController") as! QuotesViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnAccounts(_ sender: Any)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    

}
