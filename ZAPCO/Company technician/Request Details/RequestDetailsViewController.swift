//
//  RequestDetailsViewController.swift
//  ZAPCO
//
//  Created by vinove on 10/17/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class RequestDetailsViewController: UIViewController {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var requestedFrom: UILabel!
    @IBOutlet weak var requestedFor: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDescr: UILabel!
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTaskAssigned: UILabel!
    var reqdetails = TechReqList()
    var notificationReqId: String!
    var reqType: String!
    var reqId = NSNumber()

    @IBOutlet weak var btnComplete: customButton!
    
    @IBOutlet weak var btnAdd: UIButton!
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print(reqdetails)
        
    }

    override func viewWillAppear(_ animated: Bool)
    {
        
        
        if self.notificationReqId !=  nil {
            getData()
        } else{
            self.setUpViw()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if notificationReqId != nil
        {
            notificationReqId = nil
        }
    }
    
    func setUpViw()
    {
        if self.reqType == "scheduled"{
            self.btnAdd.isHidden = true
            self.btnComplete.isHidden = false
        }else
        {
            self.btnAdd.isHidden = false
            self.btnComplete.isHidden = true
        }
        
      self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
      self.imgUser.layer.masksToBounds = true
      self.imgUser.layer.borderWidth = 1.0
      self.imgUser.layer.borderColor = UIColor.lightGray.cgColor
        Shadow.textShadow(view: self.viewTop)
        Shadow.textShadow(view: self.viewBase)
        
        if let img = reqdetails.value(forKey: "image") as? NSString
        {
            let imgUrl = URL.init(string: img as String)
            let imgData = try? Data.init(contentsOf: imgUrl!)
            if imgData != nil
            {
                self.imgUser.image  = UIImage.init(data: imgData!)
            }
            else
            {
                self.imgUser.image = UIImage.init(named: "user-1")
            }
        }
        
        
        if let nam = self.reqdetails.value(forKey: "name") as? String
        {
            self.requestedFrom.text = nam
        }
        if let cat = self.reqdetails.value(forKey: "category") as? String
        {
            self.requestedFor.text = cat
        }
        if let tsk = self.reqdetails.value(forKey: "task") as? String
        {
            self.lblTaskAssigned.text = tsk
        }
        if let dt = self.reqdetails.value(forKey: "dateTime") as? String
        {
            let date = ShowTime.dateConvert(date:dt)
            self.lblDate.text = date as String
           
        }
        if let time = self.reqdetails.value(forKey: "time") as? String
        {
            self.lblTime.text = ShowTime.showTime(time:time)
        }
        if let requestID = self.reqdetails.value(forKey: "requestId")
        {
            self.reqId = requestID as! NSNumber
        }
        
        if let reqAdd = self.reqdetails.value(forKey: "address") as? String
        {
            self.lblAddress.text = reqAdd
        }
        else{
            self.lblAddress.text = ""
        }
        
        if let decr = self.reqdetails.value(forKey: "descrip") as? NSString{
            self.lblDescr.text = decr as String as String
        }
        else{
             self.lblDescr.text = ""
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
     //MARK: - IBActions
    @IBAction func btnRequst(_ sender: Any)
    {
       self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func btnQuotesClick(_ sender: Any)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "QuotesViewController") as! QuotesViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
   
    
    @IBAction func btnAccClick(_ sender: Any)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddQuotes(_ sender: Any)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddQuotesViewController") as! AddQuotesViewController
        controller.strReqN0 = "\(reqdetails.value(forKey: "requestId")!)"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnCompleteClick(_ sender: Any) {
       self.completeRequest()
    }
    //MARK: - WebServiceMethod
    func getData()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            
            let param = NSMutableDictionary()
            param.setValue(notificationReqId, forKey: "RequestId")
            
            let tokenUrl = ServiceUrl.customerUrl + "NotificationrequestDetail"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                        let dictData = response["Data"] as! NSDictionary
                        
                        let serviceData = TechReqList()
                        if let dictRequester:NSDictionary = dictData.value(forKey: "requester") as? NSDictionary
                        {
                            if let lat:NSNumber = dictRequester.value(forKey: "Latitude") as? NSNumber{
                                serviceData.latitude = lat
                            }else { serviceData.latitude = 0 }
                            if let lon:NSNumber = dictRequester.value(forKey: "Longitude") as? NSNumber{
                                serviceData.longitude = lon
                            }else { serviceData.longitude = 0 }
                            
                            if let nim:NSString = dictRequester.value(forKey: "RequesterName") as? NSString
                            {
                                serviceData.name = nim
                            }
                            
                            if let im = dictRequester.value(forKey: "RequesterName") as? String{
                                
                                serviceData.image = im as NSString
                            }
                            if let id = dictRequester.value(forKey: "requesterId") as? NSInteger{
                                serviceData.id = id
                            }else{
                                serviceData.id = 0
                            }
                            
                        }
                        if let amount = dictData.value(forKey: "Amount") as? NSNumber{
                            
                            serviceData.amount = amount
                        }else{
                            serviceData.amount = 0
                        }
                        if let cat:NSString = dictData.value(forKey: "Category") as? NSString
                        {
                            serviceData.category = cat
                        }
                        if let dtm:NSString = dictData.value(forKey: "Datetime") as? NSString
                        {
                            serviceData.dateTime = dtm
                        }
                        if let reqId = dictData.value(forKey: "RequestID") as? NSNumber{
                            serviceData.requestId = reqId
                        }else{
                            serviceData.requestId = 0
                        }
                        if let tsk = dictData.value(forKey: "Task") as? NSString{
                            serviceData.task = tsk
                        }
                        self.reqdetails = serviceData
                        self.setUpViw()
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    func completeRequest()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            
            let tokenUrl = ServiceUrl.companyTechnicianUrl + "RequestComplete?RequestId=\(self.reqId)"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            //api/companyTechnician/RequestComplete?RequestId=1487
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
}
