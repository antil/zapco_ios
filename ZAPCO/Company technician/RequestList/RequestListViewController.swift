//
//  RequestListViewController.swift
//  ZAPCO
//
//  Created by vinove on 10/31/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Toast_Swift
import MapKit

class RequestListViewController: UIViewController, CLLocationManagerDelegate
{
    
    
    @IBOutlet weak var btnPending: UIButton!
    @IBOutlet weak var btnCompleted: UIButton!
    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var viewButtons: UIView!
    @IBOutlet weak var tableView: UITableView!
    var reqType = ""
    var arrShowData = NSMutableArray()
   var style = ToastStyle()
    
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        Shadow.textShadow(view: view)
        Shadow.textShadow(view: topView)
        Shadow.textShadow(view: viewButtons)
        
        self.reqType = "pending"
        self.btnPending.isSelected = true
        self.btnPending.backgroundColor = UIColor.init(red: 39.0/255.0, green: 26.0/255.0, blue: 117.0/255.0, alpha: 1.0)
        self.btnPending.setTitleColor(UIColor.white, for: .selected)
        self.btnCompleted.backgroundColor = UIColor.white
        self.btnCompleted.setTitleColor(UIColor.init(red: 63.0/255.0, green: 64.0/255.0, blue: 100.0/255.0, alpha: 1.0), for: .normal)
        self.btnCompleted.backgroundColor = UIColor.white
        self.btnCompleted.setTitleColor(UIColor.init(red: 63.0/255.0, green: 64.0/255.0, blue: 100.0/255.0, alpha: 1.0), for: .normal)
        self.navigationController?.navigationBar.isHidden = false
       
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
       
        if CLLocationManager.locationServicesEnabled()
        {
            switch CLLocationManager.authorizationStatus()
            {
            case .notDetermined, .restricted, .denied:
                showLocationPopUp()
                print("Denied")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        }
        else
        {
            print("Location services are not enabled")
        }
        self.navigationController?.navigationBar.isHidden = false
          self.callService()
    }
    
    
    func showLocationPopUp()
    {
        let alert = UIAlertController(title: "Device Location Off!", message: "Enable Location?", preferredStyle: UIAlertControllerStyle.alert)
        alert.setValue(NSAttributedString(string: alert.message!, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 16, weight: UIFontWeightMedium), NSForegroundColorAttributeName : UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)]), forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            self.enableLocation()
        }))
        alert.addAction(UIAlertAction(title: "NO!", style: UIAlertActionStyle.default, handler: nil))
        alert.view.tintColor = UIColor.black
        self.present(alert, animated: true, completion: nil)
    }
    
    func enableLocation()
    {
        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.sid.ZAPCO")
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    
    
    
    //MARK: - Web Services
    
    func callService(){
        //{ "requestType":"scheduled/completed", offset=0,limit=15}
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            self.arrShowData.removeAllObjects()
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            param.setValue(self.reqType, forKey: "requestType")
            param.setValue(0, forKey: "offset")
            param.setValue(20, forKey: "limit")
            
            let Url = ServiceUrl.companyTechnicianUrl + "requestList"
            
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success)
                {
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                        let arrData = response["Data"] as! NSArray
                        
                        for dict in arrData{
                            let dictData = dict as! NSDictionary
                            let techReq = TechReqList()
                            
                            techReq.reqDict = dictData
                            
                            if let reqAdd = dictData.value(forKey: "Address") as? NSString{
                                techReq.address = reqAdd
                            }
                            else{
                                techReq.address = ""
                            }
                            if dictData.value(forKey: "Amount") as? NSNumber != nil
                            {
                                techReq.amount = dictData.value(forKey: "Amount") as! NSNumber
                            }
                            else{
                                techReq.amount = 0
                            }
                            if let cat = dictData.value(forKey: "Category") as? NSString{
                                techReq.category = cat
                            }
                            if let dt = dictData.value(forKey: "DateTime") as? NSString{
                                techReq.dateTime = dt
                            }
                            if let time = dictData.value(forKey: "Time") as? NSString{
                                techReq.time = time
                            }
                            if let decr = dictData.value(forKey: "Description") as? NSString{
                                techReq.descrip = decr
                            }
                            else{
                                techReq.descrip = ""
                            }
                            if let img = dictData.value(forKey: "Image") as? NSString{
                                techReq.image = img
                            }
                            else{
                                techReq.image = ""
                            }
                            if let lmt:NSNumber = dictData.value(forKey: "Limit") as? NSNumber
                            {
                                techReq.limit = lmt
                            } else { techReq.limit = 0 }
                            if let nam:NSString = dictData.value(forKey: "Name") as? NSString
                            {
                                techReq.name = nam
                            }
                            if let task:NSString = dictData.value(forKey: "Task") as? NSString
                            {
                                techReq.task = task
                            }
                            if let oft:NSNumber = dictData.value(forKey: "Offset") as? NSNumber
                            {
                                techReq.offset = oft
                            } else { techReq.offset = 0 }
                            if let rid:NSNumber = dictData.value(forKey: "RequestId") as? NSNumber
                            {
                                techReq.requestId = rid
                            } else { techReq.requestId = 0 }
                            techReq.reqType = dictData.value(forKey: "RequestType") as! NSString
                            if let st:NSNumber = dictData.value(forKey: "Status") as? NSNumber
                            {
                                techReq.status = st
                            } else { techReq.status = 0 }
                            
                            self.arrShowData.add(techReq)
                        }
                        
                        if self.arrShowData.count == 0
                        {
                             self.tableView.reloadData()
                            if self.reqType == "completed"
                            {
                                self.view.makeToast("Data not found", duration: 0.5, position: .center, style: self.style)
                                
                            }else
                            {
                                self.view.makeToast("Data not found", duration: 0.5, position: .center, style: self.style)
                            }
                            
                        }
                        else
                        {
                            self.tableView.reloadData()
                        }
                    }
                    else
                    {
                        self.view.makeToast(response["Message"] as? String, duration: 0.5, position: .center, style: self.style)
                    }
                    
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBActions
    
    @IBAction func btnClickView(_ sender: UIButton)
    {
        let objects = self.arrShowData.object(at: sender.tag) as! TechReqList
        
        if  self.reqType == "completed" {
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ReqdetailsCompletedViewController") as! ReqdetailsCompletedViewController
            controller.reqdetails = objects
            self.navigationController?.pushViewController(controller, animated: true)
            
        }else if self.reqType == "scheduled" {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
            controller.reqdetails = objects
            controller.reqType = "scheduled"
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
            controller.reqdetails = objects
            controller.reqType = "pending"
            self.navigationController?.pushViewController(controller, animated: true)
            }
    }
    
   
    @IBAction func btnSchedule(_ sender: UIButton)
    {
        self.reqType = "scheduled"
        sender.isSelected = true
        sender.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        sender.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnCompleted.isSelected = false
        btnCompleted.setTitleColor(UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0), for: UIControlState.selected)
        btnCompleted.backgroundColor = UIColor.white
        btnCompleted.layer.borderColor = UIColor.lightGray.cgColor
        btnCompleted.layer.borderWidth = 1.0
        btnPending.isSelected = false
        btnPending.backgroundColor = UIColor.white
        btnPending.layer.borderColor = UIColor.lightGray.cgColor
        btnPending.setTitleColor(UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0), for: UIControlState.selected)
        btnPending.layer.borderWidth = 1.0
        
        self.callService()
        self.tableView.reloadData()
    }
    
    @IBAction func btnCompleted(_ sender: UIButton)
    {
        self.reqType = "completed"
        sender.isSelected = true
        sender.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        sender.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnPending.isSelected = false
        btnPending.backgroundColor = UIColor.white
        btnPending.setTitleColor(UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0), for: UIControlState.selected)
        btnPending.layer.borderColor = UIColor.lightGray.cgColor
        btnPending.layer.borderWidth = 1.0
        btnSchedule.isSelected = false
        btnSchedule.backgroundColor = UIColor.white
        btnSchedule.setTitleColor(UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0), for: UIControlState.selected)
        btnSchedule.layer.borderColor = UIColor.lightGray.cgColor
        btnSchedule.layer.borderWidth = 1.0
        
        self.callService()
        self.tableView.reloadData()
    }
    
    
    @IBAction func btnPendng(_ sender: UIButton)
    {
        self.reqType = "pending"
        sender.isSelected = true
        sender.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        sender.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnCompleted.isSelected = false
        btnCompleted.backgroundColor = UIColor.white
        btnCompleted.setTitleColor(UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0), for: UIControlState.selected)
        btnCompleted.layer.borderColor = UIColor.lightGray.cgColor
        btnCompleted.layer.borderWidth = 1.0
        btnSchedule.isSelected = false
        btnSchedule.backgroundColor = UIColor.white
        btnSchedule.setTitleColor(UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0), for: UIControlState.selected)
        btnSchedule.layer.borderColor = UIColor.lightGray.cgColor
        btnSchedule.layer.borderWidth = 1.0
        
        self.callService()
        self.tableView.reloadData()
    }
    
    @IBAction func btnQuotes(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "QuotesViewController") as! QuotesViewController
       
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnAccClick(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnRequestClick(_ sender: Any)
    {
        
    }
    
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
}


extension RequestListViewController: UITableViewDataSource, UITableViewDelegate
  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrShowData.count > 0
        {
            return self.arrShowData.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestListTableViewCell") as! RequestListTableViewCell
        if(arrShowData.count > 0 && arrShowData.count > indexPath.row){

        let objects = self.arrShowData[indexPath.row] as! TechReqList
        
        if  self.reqType == "completed" {
            cell.viewCompleted.isHidden = false
            cell.btnView.isHidden = true
            cell.lblAmount.text = "$ " + String(describing: objects.amount)
            
        }else{
            cell.btnView.isHidden = false
            cell.viewCompleted.isHidden = true
        }
        
        cell.lblName.text = objects.name as String
        cell.lblType.text = objects.category as String
        
        let date = ShowTime.dateConvert(date:objects.dateTime as String)
        cell.lblDate.text = date as String
        
        
        if objects.time != ""{
           cell.lblTime.text = ShowTime.showTime(time:objects.time as String)
        }
        
        cell.btnView.addTarget(self, action: #selector(btnClickView(_:)), for: .touchUpInside)
        cell.btnView.tag = indexPath.row
        
        if objects.image != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.image as String)
            if profileUrl != nil
            {
                cell.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.imgUser.image = placeholderImage
            }
        }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let objects = self.arrShowData[indexPath.row] as! TechReqList
        if self.reqType == "completed"
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ReqdetailsCompletedViewController") as! ReqdetailsCompletedViewController
            controller.reqdetails = objects
            self.navigationController?.pushViewController(controller, animated: true)
        }
        

    }
    

   }

