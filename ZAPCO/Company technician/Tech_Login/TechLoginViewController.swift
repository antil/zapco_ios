//
//  LoginViewController.swift
//  ZAPCO
//
//  Created by vinove on 10/16/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Alamofire

class TechLoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRemeber: UIButton!
    
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.btnRemeber.layer.borderColor = UIColor.lightGray.cgColor
        self.btnRemeber.layer.borderWidth = 1.0
        self.btnRemeber.layer.cornerRadius = 3.0
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        
        self.btnRemeber.layer.borderWidth = 1
        self.btnRemeber.layer.borderColor  = UIColor.lightGray.cgColor
        self.btnRemeber.layer.masksToBounds = true
        
        if UserDefaults.standard.value(forKey: "emailID") as? String == nil
        {
            self.btnRemeber.setImage(UIImage(named:""), for: .normal)
            self.btnRemeber.isSelected = false
            self.txtEmail.text = ""
            self.txtPassword.text = ""
        }
        else{
            self.btnRemeber.setImage(UIImage(named:"blue_tick"), for: .normal)
            self.btnRemeber.isSelected = true
            self.txtEmail.text = UserDefaults.standard.value(forKey: "emailID") as? String
            self.txtPassword.text = UserDefaults.standard.value(forKey: "password") as? String
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    
    //MARK: - UITextfieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtEmail
        {
            txtPassword.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return true;
    }
    
    //MARK: - IBActions
    @IBAction func btnRemember(_ sender: UIButton)
    {
        if sender.isSelected == false
        {
            self.btnRemeber.setImage(UIImage(named:"blue_tick"), for: .normal)
            sender.isSelected = true
            UserDefaults.standard.set(self.txtEmail.text, forKey: "emailID")
            UserDefaults.standard.set(self.txtPassword.text, forKey: "password")
        }else
        {
            self.btnRemeber.setImage(UIImage(named:""), for: .normal)
            self.btnRemeber.layer.borderColor = UIColor.lightGray.cgColor
            self.btnRemeber.layer.borderWidth = 1.0
            self.btnRemeber.layer.cornerRadius = 3.0
            sender.isSelected = false
            UserDefaults.standard.removeObject(forKey: "emailID")
            UserDefaults.standard.removeObject(forKey: "password")
        }
        
    }
    
    @IBAction func btnForgot(_ sender: Any) {
        
        let forgot = self.storyboard?.instantiateViewController(withIdentifier: "forgotViewController") as! forgotViewController
        forgot.userType = "4"
        
        self.navigationController?.pushViewController(forgot, animated: true)
        
    }
    
    @IBAction func btnLogin(_ sender: Any)
    {
        if txtEmail.text == ""
        {
            AlertControl.alert(appmassage: "Please Enter Email ID", view: self)
        }
        else if txtPassword.text == ""{
            AlertControl.alert(appmassage: "Please Enter Password", view: self)
        }else{
            
            if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
            {
            
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            //{"grant_Type":"password", "username":"", "password":""}
            let param = NSMutableDictionary()
            param.setValue("password", forKey: "grant_Type")
            param.setValue(self.txtEmail.text, forKey: "username")
            param.setValue(self.txtPassword.text, forKey: "password")
            
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let tokenUrl = ServiceUrl.baseUrl + "/token"
            VTAlmofireHelper.sharedInstance.post(with: URL(string:tokenUrl)!, method: "POST", params: param as? Dictionary<String, Any>, complition: { (response, error) -> (Void) in
                print(response ?? 00)
                
                if let error:String = response?["error_description"] as? String
                {
                    if error == "The user name or password is incorrect."
                    {
                        AlertControl.alert(appmassage:error, view: self)
                        DispatchQueue.main.sync {
                            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                            self.txtPassword.text = ""
                            self.txtEmail.text = ""
                        }
                        return
                    }
                }
                else if let dict:NSDictionary = response as NSDictionary?
                {
                    let tokenDATA = TokenData()
                    tokenDATA.tokenType = dict.value(forKey: "token_type") as! NSString
                    tokenDATA.accesstoken = dict.value(forKey: "access_token") as! NSString
                    tokenDATA.userID = dict.value(forKey: "userID") as! NSString
                    Shared.sharedInstance.token_Data = tokenDATA
                    Shared.sharedInstance.tokenDict = dict as! [String : Any]
                    self.getLogin()
                }
                    
                else
                {
                    DispatchQueue.main.sync
                        {
                            AlertControl.alert(appmassage: "Try again later!", view: self)
                            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                    }
                }
                
            })}
            else
            {
                AlertControl.alert(appmassage: "No Internet Connection!", view: self)
            }
            
        }}
    
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnShowPswd(_ sender: UIButton)
    {
        if sender.isSelected == true
        {
            sender.isSelected = false
            txtPassword.isSecureTextEntry = true
            sender.setImage(UIImage.init(named: "eyeClosed"), for: UIControlState.normal)
        }
        else
        {
            sender.isSelected = true
            txtPassword.isSecureTextEntry = false
            sender.setImage(UIImage.init(named: "eyeOpen"), for: UIControlState.normal)
        }
    }
    
    
    func getLogin()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        let token = UserDefaults.standard.value(forKey: "ApplicationUniqueIdentifier") as! String
        let systemVersion = UIDevice.current.systemVersion
            
        let param = NSMutableDictionary()
        param.setValue("4", forKey: "userType")
        param.setValue("1", forKey: "techinicanType")
        param.setValue(self.txtEmail.text, forKey: "email")
        param.setValue(self.txtPassword.text, forKey: "password")
        param.setValue("iOS", forKey: "deviceType")
        param.setValue(token, forKey: "deviceToken")
        param.setValue(systemVersion, forKey: "deviceOsVersion")
        
        let tokenUrl = ServiceUrl.CommonUrl + "login"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                let responseDict = response["Response"] as! NSDictionary
                if (responseDict["StatusCode"] as AnyObject).integerValue == 200{

                    let data = responseDict["Data"] as! NSDictionary
                    let uData = Userdata()
                    uData.userData = data
                    Shared.sharedInstance.userDetails = uData
                    let userDta: Data = NSKeyedArchiver.archivedData(withRootObject: uData.userData)
                    let tokenDta: Data = NSKeyedArchiver.archivedData(withRootObject: Shared.sharedInstance.tokenDict)
                    
                    let userDefaults = UserDefaults.standard
                    
                    userDefaults.set(userDta, forKey: stringConstants.userData)
                    userDefaults.setValue(tokenDta, forKey: stringConstants.tokenData)
        
                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                    let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "NavTechnicianController")

                    appDelegate.window?.rootViewController = initialViewController
                    appDelegate.window?.makeKeyAndVisible()
                    
                }
                else if (responseDict["StatusCode"] as AnyObject).integerValue == 301
                {
                    ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                    AlertControl.alert(appmassage: responseDict["Message"] as! String, view: self)
                }
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
  
    }
    
   
}
