//
//  LoginViewController.swift
//  ZAPCO
//
//  Created by vinove on 9/14/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import MapKit


class LoginViewController:UIViewController,GIDSignInUIDelegate,GIDSignInDelegate,UITextFieldDelegate, CLLocationManagerDelegate, locationDelegates
{
    
    //MARK: - Variables
    @IBOutlet weak var btnRemember: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet var scrolV: TPKeyboardAvoidingScrollView!
    var userEmail = String()
    var socialID = String()
    var userName = String()
    var userPhone = NSNumber()
    var loginType = String()
    var socialLoginType = String()
    var profilePic = NSString()
    var userType = NSString()
    var techType = NSString()
    var location = ["latitude":28.980955, "longitude":77.180955]
    var locationM = CLLocationManager()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.btnRemember.layer.borderColor = UIColor.lightGray.cgColor
        self.btnRemember.layer.borderWidth = 1.0
        self.btnRemember.layer.cornerRadius = 3.0
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        locationM.delegate = self
        if self.loginType == "Freelance"
        {
            self.userType = "5"
            self.techType = "2"
        }
        else
        {
            self.userType = "2"
            self.techType = ""
        }
        if UserDefaults.standard.value(forKey: "useremailID") as? String == nil{
            
            self.btnRemember.setImage(UIImage(named:""), for: .normal)
            self.btnRemember.isSelected = false
            self.txtEmail.text = ""
            self.txtPassword.text = ""
        }
        else{
            self.btnRemember.setImage(UIImage(named:"blue_tick"), for: .normal)
            self.btnRemember.isSelected = true
            self.txtEmail.text = UserDefaults.standard.value(forKey: "useremailID") as? String
            self.txtPassword.text = UserDefaults.standard.value(forKey: "userpassword") as? String
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
        LocationManager.manager.delegates = self
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    
    //Current Location
    func updatedAddress(currentLocation: CLLocation)
    {
        self.location.updateValue(currentLocation.coordinate.latitude, forKey: "latitude")
        self.location.updateValue(currentLocation.coordinate.longitude, forKey: "longitude")
    }
    
    
    //MARK: - UITextfieldDelgates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtEmail
        {
            txtPassword.becomeFirstResponder()
        }
        else
        {
        textField.resignFirstResponder()
        }
        return true;
    }
    
    
    //MARK: - IBActions
    @IBAction func btnSignUp(_ sender: Any)
    {
        if self.loginType == "Freelance"
        {
            let control =  self.storyboard?.instantiateViewController(withIdentifier: "FreeLanceRegisterViewController") as! FreeLanceRegisterViewController
            self.navigationController?.pushViewController(control, animated: true)
            
        }
        else
        {
            let control =  self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController")
            self.navigationController?.pushViewController(control!, animated: true)
            
        }
        
    }
    
    
    @IBAction func btnForgot(_ sender: Any)
    {
        let control =  self.storyboard?.instantiateViewController(withIdentifier: "forgotViewController") as! forgotViewController
        control.userType = self.userType as String
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    @IBAction func btnLoginClick(_ sender: Any)
    {
        if txtEmail.text == ""
        {
            AlertControl.alert(appmassage: "Please Enter Email ID", view: self)
        }
        else if txtPassword.text == ""
        {
            AlertControl.alert(appmassage: "Please Enter Password", view: self)
        }
        else
        {
            self.loginService()
        }
        
    }
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnRememberClick(_ sender: UIButton) {
        
        if sender.isSelected == false
        {
            self.btnRemember.setImage(UIImage(named:"blue_tick"), for: .normal)
            sender.isSelected = true
            UserDefaults.standard.set(self.txtEmail.text, forKey: "useremailID")
            UserDefaults.standard.set(self.txtPassword.text, forKey: "userpassword")
            
        }else{
            self.btnRemember.setImage(UIImage(named:""), for: .normal)
            
            self.btnRemember.layer.borderColor = UIColor.lightGray.cgColor
            self.btnRemember.layer.borderWidth = 1.0
            self.btnRemember.layer.cornerRadius = 3.0
            sender.isSelected = false
            UserDefaults.standard.removeObject(forKey: "useremailID")
            UserDefaults.standard.removeObject(forKey: "userpassword")
        }
        
    }
    
    @IBAction func btnShowPswd(_ sender: UIButton)
    {
        if sender.isSelected == true
        {
            sender.isSelected = false
            txtPassword.isSecureTextEntry = true
            sender.setImage(UIImage.init(named: "eyeClosed"), for: UIControlState.normal)
        }
        else
        {
            sender.isSelected = true
            txtPassword.isSecureTextEntry = false
            sender.setImage(UIImage.init(named: "eyeOpen"), for: UIControlState.normal)
        }
    }
    
    
    //MARK: - WebServices
    func loginService()
    {
        self.view.endEditing(true)
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            let param = NSMutableDictionary()
            if self.socialID != ""
            {
                self.txtEmail.text = ""
                self.txtPassword.text = ""
                param.setValue("password", forKey: "grant_Type")
                param.setValue(self.userEmail, forKey: "username")
                param.setValue("", forKey: "password")
            }else
            {
                param.setValue("password", forKey: "grant_Type")
                param.setValue(self.txtEmail.text!, forKey: "username")
                param.setValue(self.txtPassword.text!, forKey: "password")
            }
            
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let tokenUrl = ServiceUrl.baseUrl + "/token"
            VTAlmofireHelper.sharedInstance.post(with: URL(string:tokenUrl)!, method: "POST", params: param as? Dictionary<String, Any>, complition: { (response, error) -> (Void) in
                print(response ?? 00)
                
                if let error:String = response?["error_description"] as? String
                {
                    if error == "The user name or password is incorrect."
                    {
                        AlertControl.alert(appmassage:error, view: self)
                        DispatchQueue.main.sync {
                            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                            self.txtPassword.text = ""
                            self.txtEmail.text = ""
                        }
                        return
                    }
                }
                else if let dict:NSDictionary = response as NSDictionary?
                {
                    let tokenDATA = TokenData()
                    tokenDATA.tokenType = dict.value(forKey: "token_type") as! NSString
                    tokenDATA.accesstoken = dict.value(forKey: "access_token") as! NSString
                    tokenDATA.userID = dict.value(forKey: "userID") as! NSString
                    Shared.sharedInstance.token_Data = tokenDATA
                    Shared.sharedInstance.tokenDict = dict as! [String : Any]
                    self.getLogin()
                }
                    
                else
                {
                    DispatchQueue.main.sync
                        {
                            AlertControl.alert(appmassage: "Try Again Later!", view: self)
                            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                    }
                }
                
            })
            
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
       
    }
    
    func getLogin()
    {
        //{"userType":"", "techinicanType":"", "email":"", "password":"",  deviceType:"web/iOS/Android",deviceToken:"",deviceOsVersion:"" ,"address":"","city":"" ,location:{"latitude":"","longitude":""}}
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            let strAddress = UserDefaults.standard.value(forKey: "address") as! String
            let strCity = UserDefaults.standard.value(forKey: "city") as! String
            let token = UserDefaults.standard.value(forKey: "ApplicationUniqueIdentifier") as! String
            let systemVersion = UIDevice.current.systemVersion
            
            let param = NSMutableDictionary()
            param.setValue(self.userType, forKey: "userType")
            param.setValue("2", forKey: "techinicanType")
            param.setValue(self.txtEmail.text, forKey: "email")
            param.setValue(self.txtPassword.text, forKey: "password")
            param.setValue("iOS", forKey: "deviceType")
            param.setValue(token, forKey: "deviceToken")
            param.setValue(systemVersion, forKey: "deviceOsVersion")
            param.setValue(strAddress, forKey: "address")
            param.setValue(strCity, forKey: "city")
            param.setValue(self.location, forKey: "location")
            
            print(param)
            let tokenUrl = ServiceUrl.CommonUrl + "login"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    let responseDict = response["Response"] as! NSDictionary
                    if (responseDict["StatusCode"] as AnyObject).integerValue == 200{
                        
                        let data = responseDict["Data"] as! NSDictionary
                        let uData = Userdata()
                        uData.userData = data

                        Shared.sharedInstance.userDetails = uData
                        let userDta: Data = NSKeyedArchiver.archivedData(withRootObject: uData.userData)
                        let tokenDta: Data = NSKeyedArchiver.archivedData(withRootObject: Shared.sharedInstance.tokenDict)
                        
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(userDta, forKey: stringConstants.userData)
                        userDefaults.setValue(tokenDta, forKey: stringConstants.tokenData)
                        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                        var initialViewController = UIViewController()
                        
                        if self.loginType == "Freelance"
                        {
                            initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "freeTabbar")
                        }
                        else if self.loginType == "Customer"
                        {
                            initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "tabController")
                        }
                        else
                        {
                            initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "NavTechnicianController")
                        }
                        
                        appDelegate.window?.rootViewController = initialViewController
                        appDelegate.window?.makeKeyAndVisible()
                    }
                    else if (responseDict["StatusCode"] as AnyObject).integerValue == 301
                    {
                        ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                        AlertControl.alert(appmassage: responseDict["Message"] as! String, view: self)
                    }
                }
            }
            
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
        
    }
    
    
    
    //MARK: - SocialLogin Methods
    @IBAction func btnFBClick(_ sender: Any)
    {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.systemAccount
        //"email","public_profile","user_friends"
        fbLoginManager.logIn(withReadPermissions: ["email","public_profile"], from: self, handler: {(result, error) -> Void in
            if error != nil {
                print("Logged in through facebook" )
                
                print("Facebook Login Error----\n")
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                
                
                
            }else if (result?.isCancelled)!{
                print("Facebook Login Canceled----\n")
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                
                
            }
            else {
                self.returnUserData()
                
            }
        }
        )
        
        
    }
    
    
    func returnUserData()
    {
        let userRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"id,interested_in,gender,birthday,email,age_range,name,picture.width(480).height(480)"])
        userRequest.start(completionHandler: { (connection, result , error) -> Void in
            
            if ((error) != nil)
            {
                
            }else
            {
                print(result ?? 0.0)
                var dictResult = NSDictionary()
                dictResult = result as! NSDictionary
                
                if let email: String = dictResult.value(forKey: "email") as? String
                {
                    self.userEmail = email
                }
                else
                {
                    AlertControl.alert(appmassage: "It seems your social id doesn't have an email id.Please try signing up with other social id or register new", view: self)
                    return
                }
                if let phone: NSNumber = dictResult.value(forKey: "phone") as? NSNumber
                {
                    self.userPhone = phone
                }
                
                self.userName = dictResult.value(forKey: "name") as! String
                self.socialID = dictResult.value(forKey: "id") as! String
                self.socialLoginType = "2"
                
                if let picture:NSDictionary = dictResult.value(forKey: "picture") as? NSDictionary
                {
                    let dataPic = picture.value(forKey: "data") as! NSDictionary
                    self.profilePic = dataPic.value(forKey: "url") as! NSString
                }
            
                self.socialLogin()
            }
        })
        ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
    }
    
    @IBAction func btnGoogleClick(_ sender: Any)
    {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //completed sign In
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            self.socialID = userId!
            self.userName = fullName!
            self.userEmail = email!
            self.socialLoginType = "1"
            
            
            print(userId ?? 00)
            print(idToken ?? 00)
            print(fullName ?? 00)
            print(givenName ?? 00)
            print(familyName ?? 00)
            print(email ?? 00)
            
            self.socialLogin()
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    func socialLogin()
    {

//        {
//            "LoginType" : "1",
//            "ProfileImageString" : "",
//            "DeviceToken" : "eQCIJx4o-6Y:APA91bHIOm9aL5z-5DWaU18UmRdHRYWPzatYMNYprwMIes6aAru0url-tEFA9AoOidvHc3XlwUDrzVm4iUGRdNqF8Js2tamsQ9QeR22IzOoL8pVWmQjwDiYHFfCPUABdsKDlF2Wis5AC",
//            "DeviceOsVersion" : "11.3.1",
//            "DeviceType" : "iOS",
//            "Email" : "priyanka16antil@gmail.com",
//            "SocialId" : "102174421669080987598",
//            "UserType" : "2",
//            "Name" : "priyanka antil",
//            "PhoneNumber" : "123567788"
//        }

        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {

            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            
            let deviceid = UIDevice.current.identifierForVendor?.uuidString
            print(deviceid!)
            let token = UserDefaults.standard.value(forKey: "ApplicationUniqueIdentifier") as! String
            let systemVersion = UIDevice.current.systemVersion
            let strAddress = UserDefaults.standard.value(forKey: "address") as! String
            let strCity = UserDefaults.standard.value(forKey: "city") as! String
            
            let param = NSMutableDictionary()
            param.setValue(self.userType, forKey: "UserType")
            param.setValue(self.userName, forKey: "Name")
            param.setValue(self.userEmail, forKey: "Email")
            param.setValue(self.profilePic, forKey: "ProfileImageString")
            param.setValue(self.socialLoginType, forKey: "LoginType")
            param.setValue("iOS", forKey: "DeviceType")
            param.setValue(token, forKey: "DeviceToken")
            param.setValue("\(systemVersion)", forKey: "DeviceOsVersion")
            param.setValue(self.socialID, forKey: "SocialId")
            param.setValue("", forKey: "PhoneNumber")
            param.setValue(strAddress, forKey: "address")
            param.setValue(strCity, forKey: "city")
            param.setValue(self.location, forKey: "location")
            
            let tokenUrl = ServiceUrl.CommonUrl + "socialLogin"
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : ""){
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success)
                {
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        let data = response["Data"] as! NSDictionary
                        let uData = Userdata()
                        uData.userData = data
                        Shared.sharedInstance.userDetails = uData
                        let userDta: Data = NSKeyedArchiver.archivedData(withRootObject: uData.userData)
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(userDta, forKey: stringConstants.userData)
                    
                        self.getToken()
                    }
                    else
                    {
                        AlertControl.alert(appmassage: "Try again later!", view: self)
                    }
                }
                else
                {
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
            
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    
    func getToken()
    {
        self.view.endEditing(true)
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            let data = Shared.sharedInstance.userDetails
            let dictUserDetails = data?.userData
            print(dictUserDetails!)
            var email : String!
            var password : String!
            if dictUserDetails!["Email"] != nil
            {
               email  = (dictUserDetails?["Email"] as? String)!
            }
            if dictUserDetails!["Password"] != nil
            {
                password = dictUserDetails?["Password"] as? String
            }
            
            let param = NSMutableDictionary()
            param.setValue("password", forKey: "grant_Type")
            param.setValue(email, forKey: "username")
            param.setValue(password, forKey: "password")
            
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let tokenUrl = ServiceUrl.baseUrl + "/token"
            VTAlmofireHelper.sharedInstance.post(with: URL(string:tokenUrl)!, method: "POST", params: param as? Dictionary<String, Any>, complition: { (response, error) -> (Void) in
                print(response ?? 00)
                
                if let error:String = response!["error_description"] as? String
                {
                    if error == "The user name or password is incorrect."
                    {
                        AlertControl.alert(appmassage:error, view: self)
                        DispatchQueue.main.sync {
                            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                            self.txtPassword.text = ""
                            self.txtEmail.text = ""
                        }
                        return
                    }
                    
                }
                let dict = response! as NSDictionary
                let tokenDATA = TokenData()
                tokenDATA.tokenType = dict.value(forKey: "token_type") as! NSString
                tokenDATA.accesstoken = dict.value(forKey: "access_token") as! NSString
                tokenDATA.userID = dict.value(forKey: "userID") as! NSString
                Shared.sharedInstance.token_Data = tokenDATA
                Shared.sharedInstance.tokenDict = dict as! [String : Any]
                let tokenDta: Data = NSKeyedArchiver.archivedData(withRootObject: Shared.sharedInstance.tokenDict)
                let userDefaults = UserDefaults.standard
                userDefaults.setValue(tokenDta, forKey: stringConstants.tokenData)
                
                DispatchQueue.main.sync
                    {
                        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                        var initialViewController = UIViewController()
                        
                        if self.loginType == "Freelance"
                        {
                            initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "freeTabbar")
                        }
                        else
                        {
                            initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "tabController")
                        }
                        
                        appDelegate.window?.rootViewController = initialViewController
                        appDelegate.window?.makeKeyAndVisible()
                }
                
            })
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    
}












