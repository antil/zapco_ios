 //
//  FreeHistoryTableViewCell.swift
//  ZAPCO
//
//  Created by vinove on 12/26/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class FreeHistoryTableViewCell: UITableViewCell
{

    //MARK: - Variables
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var btnView: customButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCatType: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    
    //MARK: - ViewLifeCycle
    override func layoutSubviews()
    {
        Shadow.textShadow(view: self.viewCell)
        imgUser.layer.cornerRadius = self.imgUser.frame.size.width/2.0
        imgUser.layer.masksToBounds = true
    }
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
