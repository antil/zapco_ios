//
//  FreeHistoryViewController.swift
//  ZAPCO
//
//  Created by vinove on 12/26/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Toast_Swift

class FreeHistoryViewController: UIViewController, UITextFieldDelegate
{
    //Variables
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var tableViewHistory: UITableView!
    @IBOutlet weak var btnCompleted: UIButton!
    @IBOutlet weak var viewButtons: UIView!
    
    var myPicker = UIDatePicker()
    var txtFieldPicker = UITextField()
    var historyType = String()
    var arrHistoryData = NSMutableArray()
    var style = ToastStyle()
    var dictData = FreeHistoryData()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableViewHistory.delegate = self
        self.tableViewHistory.dataSource = self
        self.txtFrom.delegate = self
        self.txtTo.delegate = self
        setUpView()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.getHistory()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func setUpView()
    {
        self.historyType = "1"
        viewButtons.layer.masksToBounds = false
        viewButtons.layer.borderWidth = 1.0
        viewButtons.layer.borderColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0).cgColor
        Shadow.textShadow(view: viewButtons)
        Shadow.textShadow(view: self.viewTop)
        
        let imageV = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let img = UIImage(named: "cal_selected")
        imageV.image = img
        txtFrom.leftViewMode = UITextFieldViewMode.always
        txtFrom.leftView = imageV
        
        let imageVi = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageVi.image = img
        txtTo.leftViewMode = UITextFieldViewMode.always
        txtTo.leftView = imageVi
        
        
        btnSchedule.isSelected = true
        btnSchedule.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        btnSchedule.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnCompleted.isSelected = false
        btnCompleted.backgroundColor = UIColor.white
        btnCompleted.layer.borderColor = UIColor.lightGray.cgColor
        btnCompleted.layer.borderWidth = 1.0

        
    }
    
    
    
    //MARK: - TextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtFrom
        {
            txtFieldPicker = txtFrom
        }
        else
        {
            txtFieldPicker = txtTo
        }
        pickUp(txtFieldPicker)
    }
    
 
    
    
    //MARK: - Datepicker
    func pickUp(_ textField : UITextField)
    {
        // UIPickerView
        myPicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        myPicker.datePickerMode = UIDatePickerMode.date
        myPicker.backgroundColor = UIColor.white
        myPicker.center = view.center
        textField.inputView = myPicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        txtFieldPicker.text = dateFormatter.string(from: myPicker.date)
        self.txtFieldPicker.resignFirstResponder()
    }
    @objc func cancelClick()
    {
        self.txtFieldPicker.resignFirstResponder()
    }
    
    
    
    //MARK: - IBActions
    @IBAction func BtnComplete(_ sender: UIButton)
    {
        sender.isSelected = true
        sender.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        sender.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnSchedule.isSelected = false
        btnSchedule.backgroundColor = UIColor.white
        btnSchedule.layer.borderColor = UIColor.lightGray.cgColor
        btnSchedule.layer.borderWidth = 1.0
        historyType = "2"
        getHistory()
        self.tableViewHistory.reloadData()
    }
    
    
    @IBAction func btnSchedule(_ sender: UIButton)
    {
        sender.isSelected = true
        sender.backgroundColor = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        sender.setTitleColor(UIColor.white, for: UIControlState.selected)
        btnCompleted.isSelected = false
        btnCompleted.backgroundColor = UIColor.white
        btnCompleted.layer.borderColor = UIColor.lightGray.cgColor
        btnCompleted.layer.borderWidth = 1.0
        historyType = "1"
        getHistory()
        self.tableViewHistory.reloadData()
    }
    
    
    @IBAction func btnSearch(_ sender: Any)
    {
        if txtTo.text == "" || txtFrom.text == ""
        {
            AlertControl.alert(appmassage: "Please select any date", view: self)
        }
        else
        {
            getHistory()
        }
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
        as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func btnClickView(_ sender: UIButton)
    {
        let objects = self.arrHistoryData.object(at:sender.tag) as! FreeHistoryData
        if self.historyType == "1"
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "FreeHistoryScheduledViewController") as! FreeHistoryScheduledViewController
            controller.historyDetails = objects
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "FreeCompeltHistoryViewController") as! FreeCompeltHistoryViewController
            controller.historyDetails = objects
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    
    
    
    
    //MARK: - WebServiceMethods
    func getHistory()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            arrHistoryData.removeAllObjects()
            // { "StartDate":"1-1-2018", "EndDate":"1-19-2018", "RequestHistoryType":"1","Limit":"15" ,"Offset":"0"}
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            param.setValue(txtFrom.text, forKey: "StartDate")
            param.setValue(txtTo.text, forKey: "EndDate")
            param.setValue(self.historyType, forKey: "RequestHistoryType")
            param.setValue(arrHistoryData.count, forKey: "Offset")
            param.setValue(arrHistoryData.count + 15, forKey: "Limit")
           
            let Url = ServiceUrl.freelanceUrl + "getHistory"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
                //{"code":200, "message":"", "data":"{"requestHistoryList":[{"name":"", "id:":"", "image":"", "category":"", "dateTime":"", "requestid":"", "task":"", "amount":"", "status":"", "paymentStatus":""}]}
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        if  let arrData = response["Data"] as! NSArray?
                        {
                            for dict in arrData
                            {
                                
                                let dictData = dict as! NSDictionary
                                let historyData = FreeHistoryData()
                                
                                if let amount = dictData.value(forKey: "amount") as? NSNumber
                                {
                                    historyData.amount = amount as NSNumber
                                }
                                if let cat:NSString = dictData.value(forKey: "Category") as? NSString
                                {
                                    historyData.category = cat
                                }
                                if let dt:NSString = dictData.value(forKey: "Datetime") as? NSString
                                {
                                    historyData.dateTime = dt
                                }
                                if let time:NSString = dictData.value(forKey: "Time") as? NSString
                                {
                                    historyData.time = time
                                }
                                if let id:NSNumber = dictData.value(forKey: "Id") as? NSNumber
                                {
                                    historyData.Id = id
                                }
                                else
                                {
                                    historyData.Id = 0
                                }
                                if let nam:NSString = dictData.value(forKey: "Name") as? NSString
                                {
                                    historyData.name = nam
                                }
                                if let lat:NSNumber = dictData.value(forKey: "Latitude") as? NSNumber
                                {
                                    historyData.latitude = lat
                                }
                                else
                                {
                                    historyData.latitude = 0
                                }
                                if let lon:NSNumber = dictData.value(forKey: "Longitude") as? NSNumber
                                {
                                    historyData.longitude = lon
                                }
                                else
                                {
                                    historyData.longitude = 0
                                }
                                if let img:NSString = dictData.value(forKey: "Image") as? NSString
                                {
                                    historyData.image = img
                                }
                                if let amt:NSNumber = dictData.value(forKey: "Amount") as? NSNumber
                                {
                                    historyData.amount = amt
                                }
                                else
                                {
                                    historyData.amount = 0
                                }
                                if let ri:NSNumber = dictData.value(forKey: "RequestId") as? NSNumber
                                {
                                    historyData.reuestId = ri
                                }else { historyData.reuestId = 0 }
                                if let task:NSString = dictData.value(forKey: "Task") as? NSString
                                {
                                    historyData.task = task
                                }
                                if let st:NSNumber = dictData.value(forKey: "Status") as? NSNumber
                                {
                                    historyData.status = st
                                }else { historyData.status = 0 }
                                self.arrHistoryData.add(historyData)
                            }
                            
                            if self.arrHistoryData.count > 0
                            {
                                self.tableViewHistory.reloadData()
                            }
                            else
                            {
                                self.view.makeToast("Data not found", duration: 0.5, position: .center, style: self.style)
                                self.tableViewHistory.reloadData()
                            }
                            
                        }
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
                
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
}

extension FreeHistoryViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrHistoryData.count > 0
        {
            return arrHistoryData.count
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FreeHistoryTableViewCell") as! FreeHistoryTableViewCell
        if (self.arrHistoryData.count > 0 && self.arrHistoryData.count > indexPath.row)
        {
        let objects = arrHistoryData[indexPath.row] as! FreeHistoryData
        
        cell.lblName.text = objects.name as String
        cell.lblDate.text = ShowTime.dateConvert(date:objects.dateTime as String)
    // cell.lblTime.text = ShowTime.timeConvert(date:objects.dateTime as String)
        if objects.time != ""{
            cell.lblTime.text = ShowTime.showTime(time:objects.time as String)
        }
        if objects.image != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.image as String)
            if profileUrl != nil
            {
                cell.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.imgUser.image = placeholderImage
            }
            
        }
        cell.lblCatType.text = objects.category as String
        cell.btnView.addTarget(self, action: #selector(btnClickView(_:)), for: .touchUpInside)
        cell.btnView.tag = indexPath.row
        }
        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        
//        let objects = self.arrHistoryData.object(at:indexPath.row) as! FreeHistoryData
//        if self.historyType == "1"
//        {
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "FreeHistoryScheduledViewController") as! FreeHistoryScheduledViewController
//            controller.historyDetails = objects
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
//        else
//        {
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "FreeCompeltHistoryViewController") as! FreeCompeltHistoryViewController
//            controller.historyDetails = objects
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
//        
//    }
    
    
}



    




    


    


