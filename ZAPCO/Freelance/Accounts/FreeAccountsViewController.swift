//
//  FreeAccountsViewController.swift
//  ZAPCO
//
//  Created by vinove on 12/27/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Alamofire

class FreeAccountsViewController: UIViewController ,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate
{
    
    //MARK: - Variables
    @IBOutlet weak var imgVProfile: UIImageView!
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var switchSetAvialbility: UISwitch!
    @IBOutlet weak var btnEdit: customButton!
    @IBOutlet weak var btnLogout: customButton!
    @IBOutlet weak var btnChangePasswd: customButton!
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtExpertise: UITextField!
    @IBOutlet weak var txtLicenseNo: UITextField!
    @IBOutlet weak var txtServiceType: UITextField!
   
    @IBOutlet weak var imageVDoc1: UIImageView!
    @IBOutlet weak var imageVDoc2: UIImageView!
    
    //change password variables
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var btnUpdate: customButton!
    @IBOutlet weak var btnBack: customButton!
    @IBOutlet weak var txtOldPasswd: UITextField!
    @IBOutlet weak var txtConfirmPasswd: UITextField!
    @IBOutlet weak var txtNewPasswd: UITextField!
    @IBOutlet weak var viewDetailsPasswd: UIView!
    
    @IBOutlet weak var switchStatus: UISwitch!
    var status = Bool()
    var address = String()
    var location:[String:Any] = ["Latitude":"","Longitude":""]
    //var dicDocument:[String:Any] = ["DocumentImageString":"","DocumentImageString":""]
    var dicDocument = NSMutableArray()
    var myPicker = UIPickerView()
    var txtfieldPicker = UITextField()
    var arrPicker = NSMutableArray()
    var isTypeService = Bool()
    var arrSkills = NSMutableArray()
    var arrCategoryID = NSMutableArray()
    var arrExpertise = NSMutableArray()
    var arrData = NSArray()
    var catID = NSInteger()
    var arrSubData = NSArray()
    var subCatID = NSInteger()
    var arrSubCat = NSMutableArray()
    var imagePicker = UIImagePickerController()
    var strOldServiceType = String()
    var selectProfile = Bool()
    var selectDoc1 = Bool()
    var selectDoc2 = Bool()
     var oldPasswd = String()
    var userid = NSNumber()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUpViw()
        
        Shadow.textShadow(view: self.viewTop)
        Shadow.textShadow(view:self.viewBg)
        txtName.delegate = self
        txtEmail.delegate = self
        txtPhone.delegate = self
        txtCity.delegate = self
        txtLicenseNo.delegate = self
        txtServiceType.delegate = self
        txtExpertise.delegate = self
        txtOldPasswd.delegate = self
        txtNewPasswd.delegate = self
        txtConfirmPasswd.delegate = self
    
        txtName.isEnabled = false
        txtEmail.isEnabled = false
        txtPhone.isEnabled = false
        txtCity.isEnabled = false
        txtExpertise.isEnabled = false
        txtServiceType.isEnabled = false
        txtLicenseNo.isEnabled = false
        imgVProfile.isUserInteractionEnabled = false
        imageVDoc1.isUserInteractionEnabled = false
        imageVDoc2.isUserInteractionEnabled = false
        switchStatus.isEnabled = false
        btnEdit.setTitle("Edit", for: .normal)
        btnEdit.isSelected = false
        selectProfile = false
        selectDoc1 = false
        selectDoc2 = false
        
        switchStatus.tintColor = UIColor.lightGray
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleImgProfileTap))
        imgVProfile.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(handleImgDoc1Tap))
        imageVDoc1.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(handleImgDoc2Tap))
        imageVDoc2.addGestureRecognizer(tap3)
        
        self.getData()
    
//        let data = Shared.sharedInstance.userDetails
 //       let dictUserDetails = data?.userData
 //       print(dictUserDetails!)
//        if dictUserDetails!["UserId"] != nil
//        {
//            self.userid = ((dictUserDetails?["UserId"]) as! NSNumber?)!
//        }
        
//        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
//        {
       // }
//        else
//        {
//            if dictUserDetails!["Name"] != nil
//            {
//                txtName.text = dictUserDetails?["Name"] as? String
//            }
//            if dictUserDetails!["Email"] != nil
//            {
//                txtEmail.text = dictUserDetails?["Email"] as? String
//            }
//            if dictUserDetails!["PhoneNumber"] != nil
//            {
//                txtPhone.text = dictUserDetails?["PhoneNumber"] as? String
//            }
//            if dictUserDetails!["ServiceType"] != nil
//            {
//                txtServiceType.text = dictUserDetails?["ServiceType"] as? String
//            }
//            if dictUserDetails!["Expertise"] != nil
//            {
//                txtExpertise.text = dictUserDetails?["Expertise"] as? String
//            }
//            if dictUserDetails!["LicenseNumber"] != nil
//            {
//                txtLicenseNo.text = dictUserDetails?["LicenseNumber"] as? String
//            }
//            if dictUserDetails!["City"] != nil
//            {
//                txtCity.text = dictUserDetails?["City"] as? String
//            }
//
//            if let img = dictUserDetails?.value(forKey: "ImageString") as? String
//            {
//                let placeholderImage = UIImage(named: "user-1")!
//                let imgUrl = URL.init(string: img)
//                if imgUrl != nil
//                {
//                    self.imgVProfile.af_setImage(withURL: imgUrl! as URL, placeholderImage: placeholderImage)
//                }
//                else
//                {
//                    self.imgVProfile.image = placeholderImage
//                }
//            }
//            else{
//                self.imgVProfile.image = UIImage(named:"user-1")
//            }
//            if let passwd:String = Shared.sharedInstance.userDetails.userData["Password"] as? String
//            {
//                oldPasswd = passwd
//            }
//        }
//
        
    }

    override func viewWillAppear(_ animated: Bool)
    {
        getCategory()
        getSubCategory()
        
        if status == true
        {
            switchStatus.isOn = true
        }
        else
        {
            switchStatus.isOn = false
        }
        
        self.imgVProfile.contentMode = .scaleAspectFill
    
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
  
    
    func setUpViw()
    {
        self.imgVProfile.layer.cornerRadius = self.imgVProfile.frame.size.height/2.0
        self.imgVProfile.layer.masksToBounds = true
        self.imgVProfile.layer.borderWidth = 1.0
        self.imgVProfile.layer.borderColor = UIColor.lightGray.cgColor
        
        Shadow.buttonShadow(view: btnEdit)
        Shadow.buttonShadow(view: btnLogout)
        Shadow.buttonShadow(view: btnChangePasswd)
        
        Shadow.textfieldShadow(textfield: txtNewPasswd)
        Shadow.textfieldShadow(textfield: txtConfirmPasswd)
        Shadow.textfieldShadow(textfield: txtOldPasswd)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    func handleImgProfileTap()
    {
        selectProfile = true
        selectDoc1 = false
        selectDoc2 = false
        upload()
    }
    
    func handleImgDoc1Tap()
    {
        selectProfile = false
        selectDoc1 = true
        selectDoc2 = false
        upload()
    }
    
    func handleImgDoc2Tap()
    {
        selectProfile = false
        selectDoc1 = false
        selectDoc2 = true
        upload()
    }
    
    
    //MARK: - IBActions
    @IBAction func btnEdit(_ sender: customButton)
    {
        if sender.isSelected == false
        {
            txtName.becomeFirstResponder()
            txtName.isEnabled = true
            txtEmail.isEnabled = true
            txtPhone.isEnabled = true
            txtCity.isEnabled = true
            txtExpertise.isEnabled = true
            txtServiceType.isEnabled = true
            txtLicenseNo.isEnabled = true
            imgVProfile.isUserInteractionEnabled = true
            imageVDoc1.isUserInteractionEnabled = true
            imageVDoc2.isUserInteractionEnabled = true
            switchStatus.isEnabled = true
            btnEdit.setTitle("Save", for: .normal)
            sender.isSelected = true
            
        }else
        {
            txtName.isEnabled = false
            txtEmail.isEnabled = false
            txtPhone.isEnabled = false
            txtCity.isEnabled = false
            txtExpertise.isEnabled = false
            txtServiceType.isEnabled = false
            txtLicenseNo.isEnabled = false
            imgVProfile.isUserInteractionEnabled = false
            imageVDoc1.isUserInteractionEnabled = false
            imageVDoc2.isUserInteractionEnabled = false
            switchStatus.isEnabled = false
            btnEdit.setTitle("Edit", for: .normal)
            sender.isSelected = false
            self.saveAccount()
        }
    }

    
    @IBAction func btnChangePassword(_ sender: Any)
    {
        viewBg.isHidden = false
    }
    
    
    @IBAction func btnLogOut(_ sender: Any)
    {
        let alert = UIAlertController(title: "Are you sure you want to", message: "Logout?", preferredStyle: UIAlertControllerStyle.alert)
        alert.setValue(NSAttributedString(string: alert.message!, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium), NSForegroundColorAttributeName : UIColor.red]), forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            self.logout()
        }))
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: nil))
        alert.view.tintColor = UIColor.init(red: 35.0/255.0, green: 15.0/255.0, blue: 120.0/255.0, alpha: 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func btnUpdatePasswd(_ sender: Any)
    {
        if txtOldPasswd.text == ""
        {
            txtOldPasswd.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Old Password", view: self)
            
        }
        else if txtOldPasswd.text != oldPasswd
        {
            txtOldPasswd.becomeFirstResponder()
            AlertControl.alert(appmassage: "Old Password is wrong", view: self)
            
        }
        else if txtNewPasswd.text == ""
        {
            txtNewPasswd.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter New Password", view: self)
            
        }
        else if txtConfirmPasswd.text == ""
        {
            txtConfirmPasswd.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Re-Enter Password", view: self)
            
        }
        else if txtConfirmPasswd.text != txtNewPasswd.text
        {
            txtNewPasswd.becomeFirstResponder()
            AlertControl.alert(appmassage: "Password Not Matched", view: self)
        }
        else if ((txtNewPasswd.text?.count)! < 6)
        {
            AlertControl.alert(appmassage: "Password should contain atleast 6 characters", view: self)
        }
            
        else
        {
        changePassword()
        }
    }
    
    
    
    
    @IBAction func actionSwitch(_ sender: UISwitch)
    {
        sender.isSelected = !sender.isSelected
       
        updateWorkStatus()
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        viewBg.isHidden = true
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func deleteDocuments(_ sender: UIButton)
    {
       
    }
    
    
    //MARK: - TextfieldDelegates
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtPhone
        {
            isTypeService = false
            self.addDoneButtonOnKeyboard()
        }
        
        if (textField == txtExpertise)
        {
            isTypeService = false
            arrExpertise.removeAllObjects()
            if txtServiceType.text == ""
            {
                AlertControl.alert(appmassage: "Please select the skills", view: self)
            }
            else
            {
                strOldServiceType = txtServiceType.text!
                self.arrExpertise.removeAllObjects()
                self.arrSubCat.removeAllObjects()
                for dict in self.arrSubData
                {
                    let catDict = dict as! NSDictionary
                    if let subCat = catDict.value(forKey: "CategoryId") as? NSInteger{
                        print(subCat)
                        if self.catID == catDict.value(forKey: "CategoryId") as! NSInteger
                        {
                            self.arrExpertise.add(catDict.value(forKey: "SubCategory")!)
                            self.arrSubCat.add(catDict.value(forKey: "SubCategoryId")!)
                        }
                    }
                    if self.arrSubData.count == 0
                    {
                        self.arrExpertise.add("")
                    }
                }
                txtfieldPicker = txtExpertise
                arrPicker = arrExpertise
                self.pickUp(txtfieldPicker)
            }
        }
        
        if (textField == txtServiceType)
        {
            isTypeService = true
            txtfieldPicker = txtServiceType
            arrPicker = arrSkills
            self.pickUp(txtfieldPicker)
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if (textField == txtServiceType)
        {
            if (txtServiceType.text == strOldServiceType)
            {
                
            }
           else
            {
            txtExpertise.text = ""
            }
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtOldPasswd
        {
            txtNewPasswd.becomeFirstResponder()
        }
        else if textField == txtNewPasswd
        {
            txtConfirmPasswd.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        myPicker.isHidden = true
        return true
    }
    
    
    //MARK: - KeyboardCustomization
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width: 320, height:50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        txtPhone.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.txtPhone.resignFirstResponder()
        
    }

    
    
    //MARK: - UIPickerViewMethods
    func pickUp(_ textField : UITextField)
    {
        // UIPickerView
        myPicker = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        myPicker.delegate = self
        myPicker.dataSource = self
        myPicker.backgroundColor = UIColor.white
        textField.inputView = myPicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick()
    {
        self.txtfieldPicker.resignFirstResponder()
    }
    @objc func cancelClick()
    {
        self.txtfieldPicker.resignFirstResponder()
    }
    
    
    //MARK: - UIPickerViewDatasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        if arrPicker.count == 0
        {
            return 0
        }
        else
        {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if arrPicker.count == 0
        {
            return 0
        }
        else
        {
            return arrPicker.count
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if !(arrPicker.count == 0)
        {
            return arrPicker[row] as? String
        }
        else
        {
            return ""
        }
    }
    
    
    //MARK: - UIPickerviewDelegates
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent  component: Int)
    {
        txtfieldPicker.text = arrPicker[row] as? String
       // let dictCat = arrData[row] as! NSDictionary
        if isTypeService{
            self.catID = arrCategoryID[row] as! NSInteger
        }
       else if arrSubCat.count != 0
        {
            if let scatID = arrSubCat[row] as? NSInteger {
                self.subCatID = scatID
            }
        }
    }
    
    
    
    
    //MARK: - UploadImageMethods
    func upload()
    {
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - ImagePickerDelegates
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:  [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        dismiss(animated: true, completion: nil)
        if (selectProfile == true)
        {
           imgVProfile.image = chosenImage
           imgVProfile.contentMode = .scaleAspectFit
        }
        else if (selectDoc1 == true)
        {
            imageVDoc1.image = chosenImage
            imageVDoc1.contentMode = .scaleAspectFit
            self.dicDocument.replaceObject(at: 0, with: self.imageConvert(img: self.imageVDoc1.image!))
            //self.dicDocument.updateValue(self.imageConvert(img: self.imageVDoc1.image!), forKey: "DocumentImageString")
        }
        else
        {
            imageVDoc2.image = chosenImage
            imageVDoc2.contentMode = .scaleAspectFit
            self.dicDocument.replaceObject(at: 1, with: self.imageConvert(img: self.imageVDoc1.image!))
            //self.dicDocument.updateValue(self.imageConvert(img: self.imageVDoc1.image!), forKey: "PermitImagestring")
        }
        
        
        
    }
    
    
    

    //MARK: - WebServiceMethod
    func getData()
    {
        self.dicDocument.removeAllObjects()
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let Url = ServiceUrl.freelanceUrl + "getProfile"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        if let arrData = response["Data"] as? NSArray
                        {
                            for dict in arrData
                            {
                                let dictData = dict as! NSDictionary
                                
                                if let img = dictData.value(forKey: "ImageString") as? String
                                {
                                    let placeholderImage = UIImage(named: "user-1")!
                                    let profileUrl = NSURL(string: img)
                                    if profileUrl != nil
                                    {
//                                        DispatchQueue.main.async(execute: { () -> Void in
//                                            let data = try? Data(contentsOf: profileUrl! as URL)
//                                            self.imgVProfile.image = UIImage(data: data!)
//                                        })
                                        self.imgVProfile.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
                                    }
                                    else
                                    {
                                        self.imgVProfile.image = placeholderImage
                                    }
                                }
                                else{
                                    self.imgVProfile.image = UIImage(named:"user-1")
                                }
                                self.imgVProfile.contentMode = .scaleAspectFill
                                
                                if let nam:String = dictData.value(forKey: "Name") as? String
                                {
                                    self.txtName.text = nam
                                }
                                if let email:String = dictData.value(forKey: "Email") as? String
                                {
                                    self.txtEmail.text = email
                                }
                                
                                
                                if let city:String = dictData.value(forKey: "City") as? String
                                {
                                    self.txtCity.text = city
                                }
                                if let phn:String = dictData.value(forKey: "PhoneNumber") as? String
                                {
                                    self.txtPhone.text = phn
                                }
                                if let exp:String = dictData.value(forKey: "Expertise") as? String
                                {
                                    self.txtExpertise.text = exp
                                }
                                if let lic:String = dictData.value(forKey: "LicenseNumber") as? String
                                {
                                    self.txtLicenseNo.text = lic
                                }
                                if let ser:String = dictData.value(forKey: "ServiceType") as? String
                                {
                                    self.txtServiceType.text = ser
                                }
                                if let add:String = dictData.value(forKey: "Address") as? String
                                {
                                    self.address = add
                                }
                                if dictData.value(forKey: "location") != nil
                                {
                                    self.location = (dictData.value(forKey: "location") as? Dictionary)!
                                }
                                if let st:Bool = dictData.value(forKey: "WorkStatus") as? Bool
                                {
                                    self.status = st
                                }
                                if let arr = dictData.value(forKey: "Document") as? NSArray
                                {
                                    for dict in arr
                                    {
                                        let dicDat = dict as! NSDictionary
                                        if let str1 = dicDat.value(forKey: "DocumentImageString") as? String
                                        {
                                        self.dicDocument.add(str1)
                                        }
                                    }
                                }
                                
                                if let img1 = self.dicDocument[0] as? String
                                {
                                    let placeholderImage = UIImage(named: "zapco_logo")!
                                    let profileUrl = NSURL(string: img1)
                                    if profileUrl != nil
                                    {
//                                        DispatchQueue.main.async(execute: { () -> Void in
//                                            let data = try? Data(contentsOf: profileUrl! as URL) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//                                            self.imageVDoc1.image = UIImage(data: data!)
//                                        })
                                        self.imageVDoc1.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
                                    }
                                    else
                                    {
                                        self.imageVDoc1.image = placeholderImage
                                    }
//                                    if let imgData = try? Data.init(contentsOf: profileUrl! as URL)
//                                    {
//                                        self.dicDocument.updateValue(self.imageConvert(img: UIImage(data: imgData)!), forKey: "DocumentImageString")
//                                    }
//                                    else
//                                    {
//                                        self.dicDocument.updateValue(self.imageConvert(img: self.imageVDoc1.image!), forKey: "DocumentImageString")
//                                    }
                                   }
                                
                                
                                if let img2 = self.dicDocument[1] as? String
                                {
                                    let placeholderImage = UIImage(named: "zapco_logo")!
                                    let profileUrl = NSURL(string: img2)
                                    if profileUrl != nil
                                    {
//                                        DispatchQueue.main.async(execute: { () -> Void in
//                                            let data = try? Data(contentsOf: profileUrl! as URL) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//                                            self.imageVDoc2.image = UIImage(data: data!)
//                                        })
                                        self.imageVDoc2.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
                                    }
                                    else
                                    {
                                        self.imageVDoc2.image = placeholderImage
                                    }
//                                    if let imgData = try? Data.init(contentsOf: profileUrl! as URL)
//                                    {
//                                        self.dicDocument.updateValue(self.imageConvert(img: UIImage(data: imgData)!), forKey: "PermitImagestring")
//                                    }
//                                    else
//                                    {
//                                        self.dicDocument.updateValue(self.imageConvert(img: self.imageVDoc2.image!), forKey: "PermitImagestring")
//                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    AlertControl.alert(appmassage: "Try Again Later!", view: self)
                }
            }}
        else
        {
            return
        }
    }
    
    
    
    func saveAccount()
    {
        var strStatus = String()
        if status == true
        {
           strStatus = "true"
        }
        else
        {
            strStatus = "false"
        }
        
        var dicDoc:[String:Any] = ["DocumentImageString":"","PermitImagestring":""]
        let arrDic = NSMutableArray()
        dicDoc.updateValue(self.imageConvert(img: self.imageVDoc1.image!), forKey: "DocumentImageString")
        arrDic.add(dicDoc)
        dicDoc.updateValue(self.imageConvert(img: self.imageVDoc2.image!), forKey: "PermitImagestring")
        arrDic.add(dicDoc)
        print(arrDic)

        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(strStatus, forKey: "WorkStatus")
        param.setValue(self.txtName.text, forKey: "Name")
        param.setValue(self.txtEmail.text, forKey: "Email")
        param.setValue(self.txtPhone.text, forKey: "PhoneNumber")
        param.setValue(self.txtCity.text, forKey: "City")
        param.setValue(self.txtLicenseNo.text, forKey: "LicenseNumber")
        param.setValue(self.txtServiceType.text, forKey: "ServiceType")
        param.setValue(self.txtExpertise.text, forKey: "Expertise")
        param.setValue(self.address, forKey: "Address")
        param.setValue(self.location, forKey: "location")
        param.setValue(dicDoc, forKey: "documents")
       // param.setValue(("F_" + txtName.text!), forKey: "ImageName")
        param.setValue((""), forKey: "ImageName")
        param.setValue(self.imageConvert(img: self.imgVProfile.image!), forKey: "ImageString")
        
        let Url = ServiceUrl.freelanceUrl + "editProfile"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    self.getData()
                }
            }
            else{
                AlertControl.alert(appmassage: "Try Again Later!", view: self)
            }
        }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    func imageConvert(img:UIImage)->String
    {
        let imageData: NSData = UIImageJPEGRepresentation(img, 0.4)! as NSData
        let imageStr = imageData.base64EncodedString(options:.lineLength64Characters)
        return imageStr
    }

    
    func updateWorkStatus()
    {
        if switchStatus.isOn == true
        {
            status = true
        }
        else
        {
            status = false
        }
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(status, forKey: "workStatus")
           
        let Url = ServiceUrl.freelanceUrl + "updateWorkStatus"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                }
            }
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    

    func changePassword()
    {
        self.view.endEditing(true)
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(txtOldPasswd.text, forKey: "oldPassword")
        param.setValue(txtNewPasswd.text, forKey: "newPassword")
        
        let Url = ServiceUrl.CommonUrl + "changePassword"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                    self.viewBg.isHidden = true
                }
            }
            else{
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    
    func logout()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let Url = ServiceUrl.CommonUrl + "logout"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success)
            {
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    AlertControl.alert(appmassage: response["Message"] as! String , view: self)
                    ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                    
                    UserDefaults.standard.removeObject(forKey: "userData")
                    UserDefaults.standard.removeObject(forKey: stringConstants.tokenData)
                    UserDefaults.standard.removeObject(forKey: stringConstants.userData)
                    let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                    rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "landingNavigation")
                    
                }
            }
            else{
                AlertControl.alert(appmassage: "Try Again Later!", view: self)
            }
        }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    
    
    
    
    func getCategory()
    {
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        
        let tokenUrl = ServiceUrl.CommonUrl + "category"
        // let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : ""){
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    self.arrCategoryID.removeAllObjects()
                    let data = response["Data"] as! NSArray
                    self.arrData = data
                    for dict in data
                    {
                        let catDict = dict as! NSDictionary
                        if  let category = catDict.value(forKey: "Category") as? String{
                            self.arrSkills.add(category)
                            self.arrCategoryID.add(catDict.value(forKey: "CategoryId") as! NSInteger)
                        }
                        if self.arrData.count == 0
                        {
                            self.arrSkills.add("")
                        }
                        self.myPicker.reloadAllComponents()
                    }
                    
                }
            }
            }}
        else
        {
           return
        }
        
    }
    
    func getSubCategory()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        let tokenUrl = ServiceUrl.CommonUrl + "subcategory"
        // let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : ""){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                    let data = response["Data"] as! NSArray
                    self.arrSubData = data
                }
            }
            }}
        else
        {
           return
        }
    }
    
    
}
