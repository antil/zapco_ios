//
//  ReqDetailsViewController.swift
//  ZAPCO
//
//  Created by vinove on 12/26/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class ReqDetailsViewController: UIViewController
{
    
    //MARK: - Variables
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblDescr: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblReqFor: UILabel!
    @IBOutlet weak var lblReqFrom: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var viewDetails: UIView!
    
    var reqType = ""
    var arrShowData = NSMutableArray()
    var reqId = NSNumber()
    var dictReq = FreeHistoryData()
    var lat = NSNumber()
    var lng = NSNumber()
    var notificationReqId:String?
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Shadow.textShadow(view: self.viewTop)
        Shadow.textShadow(view: self.viewDetails)
        
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
        
        self.reqType = "scheduled"  //completed
        
       
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.navigationBar.isHidden = false
         setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if notificationReqId != nil
        {
            notificationReqId = nil
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //GetAddressFromLocation
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String)
    {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    var addressString : String = ""
                    if pm.subThoroughfare != nil {
                        addressString = addressString + pm.subThoroughfare! + " "
                    }
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(addressString)
                    self.lblAddress.text = addressString
                }
        })
        
    }
   
    
    
    //MARK: - IBActions
    @IBAction func btnAccept(_ sender: Any)
    {
        if  UserDefaults.standard.value(forKey: "balanceServices") as! String != "0"
        {
            acceptRequest()
        }else {
            AlertControl.alert(appmassage: "Voucher Code Services Expire.", view: self)
        }
    }
    
    @IBAction func btnReject(_ sender: Any)
    {
        rejectRequest()
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    
    //MARK: - WebServiceMethod
    func getData()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            
            let param = NSMutableDictionary()
            param.setValue(notificationReqId, forKey: "RequestId")
           
            let tokenUrl = ServiceUrl.customerUrl + "NotificationrequestDetail"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                        let dictData = response["Data"] as! NSDictionary
                        
                        let serviceData = FreeHistoryData()
                        if let dictRequester:NSDictionary = dictData.value(forKey: "requester") as? NSDictionary
                        {
                            if let lat:NSNumber = dictRequester.value(forKey: "Latitude") as? NSNumber{
                                serviceData.latitude = lat
                            }else { serviceData.latitude = 0 }
                            if let lon:NSNumber = dictRequester.value(forKey: "Longitude") as? NSNumber{
                                serviceData.longitude = lon
                            }else { serviceData.longitude = 0 }
                            
                            if let nim:NSString = dictRequester.value(forKey: "RequesterName") as? NSString
                            {
                                serviceData.name = nim
                            }
                            
                            if let im = dictRequester.value(forKey: "RequesterName") as? String{
                                
                                serviceData.image = im as NSString
                            }
                            if let id = dictRequester.value(forKey: "requesterId") as? NSNumber{
                                serviceData.Id = id
                            }else{
                                serviceData.Id = 0
                            }
                            
                        }
                        if let amount = dictData.value(forKey: "Amount") as? NSNumber{
                            
                            serviceData.amount = amount
                        }
                        if let cat:NSString = dictData.value(forKey: "Category") as? NSString
                        {
                            serviceData.category = cat
                        }
                        if let com:NSString = dictData.value(forKey: "Comments") as? NSString
                        {
                            serviceData.comments = com
                        }
                        if let dtm:NSString = dictData.value(forKey: "Datetime") as? NSString
                        {
                            serviceData.dateTime = dtm
                        }
                        
                        //                        if let eta = dictData.value(forKey: "Eta") as? NSString{
                        //                            serviceData.eta = eta
                        //                        }
                        //
                        //                        if let rating = dictData.value(forKey: "Rating") as? NSInteger{
                        //                            serviceData.rating = rating
                        //                        }else { serviceData.rating = 0 }
                        //                        if let reqId = dictData.value(forKey: "RequestID") as? NSInteger{
                        //                            serviceData.reqID = reqId
                        //                        }
                        //                        if let sts = dictData.value(forKey: "Status") as? NSInteger{
                        //                            serviceData.status = sts
                        //                        }
                        if let tsk = dictData.value(forKey: "Task") as? NSString{
                            serviceData.task = tsk
                        }
                        
                        self.dictReq = serviceData
                        self.updateUI()
                        
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    func setupUI() {
        if self.notificationReqId != nil {
            self.reqId = NumberFormatter().number(from: notificationReqId!)!
            getData()
        }
        else{
            updateUI()
        }
    }
    func updateUI() {
        if (dictReq.latitude as NSNumber) == 0 || (dictReq.longitude as NSNumber) == 0
        {
            print("location not found")
        }
        else
        {
            getAddressFromLatLon(pdblLatitude: "\(dictReq.latitude)", withLongitude: "\(dictReq.longitude)")
        }
        
        self.lblReqFor.text = String(describing: dictReq.category )
        var tim = "00:00:00"
        if dictReq.time != ""{
            tim = dictReq.time as String
        }
        self.lblDate.text = ShowTime.dateConvert(date: dictReq.dateTime as String) + " & " + ShowTime.showTime(time: tim)
        self.lblReqFrom.text = String(describing: dictReq.name )
        self.lblDescr.text = String(describing: dictReq.task )
        
        if dictReq.image != ""
        {
            let imgUrl = URL.init(string: dictReq.image as String)
            let placehoderImage = UIImage.init(named: "user-1")
            if imgUrl != nil
            {
                self.imgUser.af_setImage(withURL: imgUrl! as URL, placeholderImage: placehoderImage)
            }
            else{
                self.imgUser.image = placehoderImage
            }
        }
        }
    
    func rejectRequest()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            //param.setValue(String(describing: reqData.value(forKey: "RequestId")!), forKey: "requestId")
            
            param.setValue(self.reqId, forKey: "requestId")
            param.setValue("3", forKey: "ResuestStatus")
            
            let Url = ServiceUrl.freelanceUrl + "acceptRequest"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                        // AlertControl.alert(appmassage: "request rejected", view: self)
                        self.navigationController?.popViewController(animated: true)

                        
                    }
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    func acceptRequest()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            //param.setValue(String(describing: reqData.value(forKey: "RequestId")!), forKey: "requestId")
            param.setValue(self.reqId, forKey: "requestId")
            param.setValue("1", forKey: "ResuestStatus")
            
            let Url = ServiceUrl.freelanceUrl + "acceptRequest"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                        //AlertControl.alert(appmassage: "request accepted", view: self)
                        self.navigationController?.popViewController(animated: true)
                        //self.getReqList()
                    }
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    
    

    
    
}

