//
//  RequestsViewController.swift
//  ZAPCO
//
//  Created by vinove on 12/26/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import MapKit
import Toast_Swift

class FreeReqViewController: UIViewController, CLLocationManagerDelegate {

    //MARK: - Variables
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var tableViewList: UITableView!
    var reqData = NSData()
    var arrRequestList = NSMutableArray()
    var style = ToastStyle()
    var reqType = ""
    var reqId = NSNumber()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
       super.viewDidLoad()
       self.tableViewList.delegate = self
       self.tableViewList.dataSource = self
       Shadow.textShadow(view: self.viewTop)
        reqType = "0"
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if CLLocationManager.locationServicesEnabled()
        {
            switch CLLocationManager.authorizationStatus()
            {
            case .notDetermined, .restricted, .denied:
                showLocationPopUp()
                print("Denied")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        }
        else
        {
            print("Location services are not enabled")
        }
        getReqList()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func showLocationPopUp()
    {
        let alert = UIAlertController(title: "Device Location Off!", message: "Enable Location?", preferredStyle: UIAlertControllerStyle.alert)
        alert.setValue(NSAttributedString(string: alert.message!, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 16, weight: UIFontWeightMedium), NSForegroundColorAttributeName : UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)]), forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            self.enableLocation()
        }))
        alert.addAction(UIAlertAction(title: "NO!", style: UIAlertActionStyle.default, handler: nil))
        //alert.view.tintColor = UIColor.init(red: 35.0/255.0, green: 15.0/255.0, blue: 120.0/255.0, alpha: 1.0)
        alert.view.tintColor = UIColor.black
        self.present(alert, animated: true, completion: nil)
    }
    
    func enableLocation()
    {
        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.sid.ZAPCO")
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    

    //MARK: - IBActions
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    @IBAction func btnAccept(_ sender: UIButton)
    {
        if  UserDefaults.standard.value(forKey: "balanceServices") as! String != "0"
        {
            acceptRequest()
        }else {
            AlertControl.alert(appmassage: "Voucher Code Services Expire.", view: self)
        }
    }
    @IBAction func btnReject(_ sender: UIButton)
    {
        rejectRequest()
    }
    
   //MARK: - WebServiceMethods
    func rejectRequest()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
       ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
           let param = NSMutableDictionary()
            //param.setValue(String(describing: reqData.value(forKey: "RequestId")!), forKey: "requestId")
            
            param.setValue(self.reqId, forKey: "requestId")
            param.setValue("3", forKey: "ResuestStatus")
            
            let Url = ServiceUrl.freelanceUrl + "acceptRequest"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                        
                       // AlertControl.alert(appmassage: "request rejected", view: self)
                        self.getReqList()
                        
                    }
                }
            }
            }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    func acceptRequest()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        let param = NSMutableDictionary()
        //param.setValue(String(describing: reqData.value(forKey: "RequestId")!), forKey: "requestId")
            param.setValue(self.reqId, forKey: "requestId")
            param.setValue("1", forKey: "ResuestStatus")
           
        let Url = ServiceUrl.freelanceUrl + "acceptRequest"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                   // AlertControl.alert(appmassage: "request accepted", view: self)
        
                    self.getReqList()
                }
            }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    func getReqList()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            arrRequestList.removeAllObjects()
            let param = NSMutableDictionary()
            param.setValue("", forKey: "StartDate")
            param.setValue("", forKey: "EndDate")
            param.setValue(self.reqType, forKey: "RequestHistoryType")
            param.setValue("0", forKey: "Offset")
            param.setValue("15", forKey: "Limit")
            
            let Url = ServiceUrl.freelanceUrl + "getHistory"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        if  let arrData = response["Data"] as! NSArray?
                        {
                            for dict in arrData
                            {
                                let dictData = dict as! NSDictionary
                                let historyData = FreeHistoryData()
                                
                                if let amount = dictData.value(forKey: "amount") as? NSNumber
                                {
                                    historyData.amount = amount as NSNumber
                                }
                                if let cat:NSString = dictData.value(forKey: "Category") as? NSString
                                {
                                    historyData.category = cat
                                }
                                if let dt:NSString = dictData.value(forKey: "Datetime") as? NSString
                                {
                                    historyData.dateTime = dt
                                }
                                if let time:NSString = dictData.value(forKey: "Time") as? NSString
                                {
                                    historyData.time = time
                                }
                                if let id:NSNumber = dictData.value(forKey: "Id") as? NSNumber
                                {
                                    historyData.Id = id
                                }
                                else
                                {
                                    historyData.Id = 0
                                }
                                if let nam:NSString = dictData.value(forKey: "Name") as? NSString
                                {
                                    historyData.name = nam
                                }
                                if let lat:NSNumber = dictData.value(forKey: "Latitude") as? NSNumber
                                {
                                    historyData.latitude = lat
                                }else { historyData.latitude = 0 }
                                if let lon:NSNumber = dictData.value(forKey: "Longitude") as? NSNumber
                                {
                                    historyData.longitude = lon
                                }else { historyData.longitude = 0 }
                                if let img:NSString = dictData.value(forKey: "Image") as? NSString
                                {
                                    historyData.image = img
                                }
                                if let amt:NSNumber = dictData.value(forKey: "Amount") as? NSNumber
                                {
                                    historyData.amount = amt
                                }
                                else
                                {
                                    historyData.amount = 0
                                }
                                if let ri:NSNumber = dictData.value(forKey: "RequestId") as? NSNumber
                                {
                                    historyData.reuestId = ri
                                }else { historyData.reuestId = 0 }
                                if let task:NSString = dictData.value(forKey: "Task") as? NSString
                                {
                                    historyData.task = task
                                }
                                if let st:NSNumber = dictData.value(forKey: "Status") as? NSNumber
                                {
                                    historyData.status = st
                                }else { historyData.status = 0 }
                                self.arrRequestList.add(historyData)
                            }
                            
                            if self.arrRequestList.count > 0
                            {
                                self.tableViewList.reloadData()
                            }
                            else
                            {
                                self.view.makeToast("Data not found", duration: 0.5, position: .center, style: self.style)
                                self.tableViewList.reloadData()
                            }
                            
                        }
                    }
                }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
                
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
}

extension FreeReqViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrRequestList.count
    }
    
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FreeReqTableViewCell") as! FreeReqTableViewCell
         if (self.arrRequestList.count > 0 && self.arrRequestList.count > indexPath.row)
         {
        let objects = arrRequestList[indexPath.row] as! FreeHistoryData
        
        cell.lblUserName.text = objects.name as String
        cell.lblDate.text = ShowTime.dateConvert(date:objects.dateTime as String)
      //  cell.lblTime.text = ShowTime.timeConvert(date:objects.dateTime as String)
        
        if objects.time != ""{
            cell.lblTime.text = ShowTime.showTime(time:objects.time as String)
        }

        
        if objects.image != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.image as String)
            if profileUrl != nil
            {
                cell.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.imgUser.image = placeholderImage
            }
            
        }
        cell.lblCategory.text = objects.category as String
        
        cell.btnAccept.tag = indexPath.row
        cell.btnReject.tag = indexPath.row
        reqId = objects.reuestId
        
        cell.btnAccept.addTarget(self, action: #selector(btnAccept(_:)), for: .touchUpInside)
        cell.btnReject.addTarget(self, action: #selector(btnReject(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let objects = arrRequestList[indexPath.row] as! FreeHistoryData
        let control = self.storyboard?.instantiateViewController(withIdentifier: "ReqDetailsViewController") as! ReqDetailsViewController
        control.dictReq = objects
        control.reqId = self.reqId
        self.navigationController?.pushViewController(control, animated: true)
    }
    
}
    

