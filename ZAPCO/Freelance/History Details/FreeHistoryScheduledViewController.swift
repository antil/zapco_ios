//
//  FreeHistoryScheduledViewController.swift
//  ZAPCO
//
//  Created by vinove on 12/27/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Alamofire

class FreeHistoryScheduledViewController: UIViewController, UITextFieldDelegate
{
    
    //MARK: - Variables
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewDetails: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblReqFor: UILabel!
    
    @IBOutlet weak var btnPaymentStatus: customButton!
    @IBOutlet weak var lblCostEstimate: UILabel!
    @IBOutlet weak var lblTaskDescr: UILabel!
    @IBOutlet weak var lblReqNo: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var txtTotalCost: UITextField!
    
    @IBOutlet weak var openBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var pendingBtn: UIButton!
    
    @IBOutlet weak var viewCost: CustomView!
    var reqId = NSNumber()
    var status = NSNumber()
    var historyDetails = FreeHistoryData()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Shadow.textShadow(view: self.viewTop)
        Shadow.textShadow(view: self.viewDetails)
        Shadow.textShadow(view: self.viewCost)
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
        status = 1
        txtTotalCost.delegate = self
        pendingBtn.isSelected = false
        pendingBtn.setImage(UIImage(named:"blue_tick"), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.lblReqNo.text = String(describing: historyDetails.reuestId )
        var tim = "00:00:00"
        if historyDetails.time != ""{
            tim = historyDetails.time as String
        }
        self.lblDateTime.text = ShowTime.dateConvert(date: historyDetails.dateTime as String) + " & " + ShowTime.showTime(time: tim)
//        self.lblDateTime.text = ShowTime.dateConvert(date: historyDetails.dateTime as String) + " & " + ShowTime.timeConvert(date: historyDetails.dateTime as String)
        //ShowTime.showTime(time: historyDetails.time as String)
        self.lblName.text = String(describing: historyDetails.name )
        self.lblReqFor.text = String(describing: historyDetails.category )
        self.reqId = historyDetails.reuestId
        if historyDetails.amount != 0
        {
             self.lblCostEstimate.text = String(describing: historyDetails.amount )
            self.txtTotalCost.text = String(describing: historyDetails.amount )
        }
        self.lblTaskDescr.text = String(describing: historyDetails.task )
        
        if (historyDetails.latitude as NSNumber) == 0 || (historyDetails.longitude as NSNumber) == 0
        {
            print("location not found")
        }
        else
        {
            _ = historyDetails.latitude as NSNumber
            _ = historyDetails.longitude as NSNumber
        }
       
        
        if historyDetails.paymentStatus == "<null>"
        {
           btnPaymentStatus.titleLabel?.text = "Pending"
        }
        else
        {
           btnPaymentStatus.titleLabel?.text = "Paid"
        }
        
        
        
        if historyDetails.image != ""
        {
            let placehoderImage = UIImage.init(named: "user-1")
            let imgUrl = URL.init(string: historyDetails.image as String)
            
            if imgUrl != nil
            {
                self.imgUser.af_setImage(withURL: imgUrl! as URL, placeholderImage: placehoderImage)
            }
            else{
                self.imgUser.image = placehoderImage
            }
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    //MARK: - UITextfieldDelegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtTotalCost
        {
            addDoneButtonOnKeyboard()
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    //MARK: - KeyboardCustomization
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width: 320, height:50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        self.txtTotalCost.inputAccessoryView = doneToolbar
    }
    func doneButtonAction()
    {
        self.txtTotalCost.resignFirstResponder()
    }
    
    
    //MARK: - IBActions
    @IBAction func btnSubmit(_ sender: Any)
    {
        confirmScheduledRequest()
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOpen(_ sender: UIButton)
    {
        if sender.isSelected == false{
            sender.setImage(UIImage(named:"blue_tick"), for: .normal)
            sender.isSelected = true
            closeBtn.isSelected = false
            closeBtn.setImage(UIImage(named:"unsel"), for: .normal)
            pendingBtn.isSelected = false
            pendingBtn.setImage(UIImage(named:"unsel"), for: .normal)
            status = 1
        }else{
            sender.setImage(UIImage(named:"unsel"), for: .normal)
            sender.isSelected = false
        }
}
    
    @IBAction func btnPending(_ sender: UIButton)
    {
        if sender.isSelected == false{
            sender.setImage(UIImage(named:"blue_tick"), for: .normal)
            sender.isSelected = true
            closeBtn.isSelected = false
            closeBtn.setImage(UIImage(named:"unsel"), for: .normal)
            openBtn.isSelected = false
            openBtn.setImage(UIImage(named:"unsel"), for: .normal)
            status = 0
        }else{
            sender.setImage(UIImage(named:"unsel"), for: .normal)
            sender.isSelected = false
        }
    }
    
    @IBAction func btnClose(_ sender: UIButton)
    {
        if sender.isSelected == false{
            sender.setImage(UIImage(named:"blue_tick"), for: .normal)
            sender.isSelected = true
            openBtn.isSelected = false
            openBtn.setImage(UIImage(named:"unsel"), for: .normal)
            pendingBtn.isSelected = false
            pendingBtn.setImage(UIImage(named:"unsel"), for: .normal)
            status = 2
        }else{
            sender.setImage(UIImage(named:"unsel"), for: .normal)
            sender.isSelected = false
        }
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    //MARK: - WebServiceMethods
    func confirmScheduledRequest()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let param = NSMutableDictionary()
        param.setValue(reqId, forKey: "requestId")
        param.setValue(txtTotalCost.text, forKey: "cost")
        param.setValue(status, forKey: "status")
        
        let Url = ServiceUrl.freelanceUrl + "confirmRequest"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                    //AlertControl.alert(appmassage: "request confirmed", view: self)
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }}
            
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
}

