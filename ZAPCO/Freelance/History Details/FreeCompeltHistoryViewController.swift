//
//  FreeCompeltHistoryViewController.swift
//  ZAPCO
//
//  Created by vinove on 12/27/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class FreeCompeltHistoryViewController: UIViewController
{
    
      //MARK: - Variables
        @IBOutlet weak var viewTop: UIView!
        @IBOutlet weak var viewDetails: UIView!
        @IBOutlet weak var imgUser: UIImageView!
        @IBOutlet weak var lblName: UILabel!
        @IBOutlet weak var lblReqFor: UILabel!
        @IBOutlet weak var lblCostEstimate: UILabel!
        @IBOutlet weak var lblTaskDescr: UILabel!
        @IBOutlet weak var lblReqNo: UILabel!
        @IBOutlet weak var lblDateTime: UILabel!
        @IBOutlet weak var lblStatus: UILabel!
        @IBOutlet weak var lblTotalCost: UILabel!
        var historyDetails = FreeHistoryData()
        
    @IBOutlet weak var btnPaymentStatus: customButton!
    //MARK: - ViewLifeCycle
        override func viewDidLoad()
        {
            super.viewDidLoad()
            Shadow.textShadow(view: self.viewTop)
            Shadow.textShadow(view: self.viewDetails)
            
            self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
            self.imgUser.layer.masksToBounds = true
            
            self.lblReqNo.text = String(describing: historyDetails.reuestId )
            var tim = "00:00:00"
            if historyDetails.time != ""{
                tim = historyDetails.time as String
            }
            self.lblDateTime.text = ShowTime.dateConvert(date: historyDetails.dateTime as String) + " & " + ShowTime.showTime(time: tim)
//            self.lblDateTime.text = ShowTime.dateConvert(date: historyDetails.dateTime as String) + " & " + ShowTime.timeConvert(date: historyDetails.dateTime as String)
            self.lblName.text = String(describing: historyDetails.name )
            self.lblReqFor.text = String(describing: historyDetails.name )
            if historyDetails.amount != 0
            {
               self.lblCostEstimate.text = String(describing: historyDetails.amount )
            }
//            if historyDetails.paymentStatus == "<null>"
//            {
//                btnPaymentStatus.titleLabel?.text = "Pending"
//            }
//            else
//            {
//                btnPaymentStatus.titleLabel?.text = "Paid"
//            }
            
            
            self.lblTaskDescr.text = String(describing: historyDetails.task )
            
            if historyDetails.image != ""
            {
                let placehoderImage = UIImage.init(named: "user-1")
                let imgUrl = URL.init(string: historyDetails.image as String)
                
                if imgUrl != nil
                {
                    self.imgUser.af_setImage(withURL: imgUrl! as URL, placeholderImage: placehoderImage)
                }
                else{
                    self.imgUser.image = placehoderImage
                }
            }
        }
        
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    
    //MARK: - IBActions
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
        
        
    
}

    
   
