//  AppDelegate.swift
//  ZAPCO
//
//  Created by vinove on 9/13/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import DropDown
import UserNotifications
import GoogleMaps
import GooglePlaces
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        self.perform(#selector(showAlertForLowBattery), with: nil, afterDelay: 1)
        
        FirebaseApp.configure()
        
        UIApplication.shared.registerForRemoteNotifications()
        
        if #available(iOS 10.0, *)
        {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        else
        {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        Messaging.messaging().delegate = self
        GMSServices.provideAPIKey("AIzaSyAdOf4-zR0LvsVUQ-U4bPasyT-HCWNXclA")
        GMSPlacesClient.provideAPIKey("AIzaSyAdOf4-zR0LvsVUQ-U4bPasyT-HCWNXclA")
        
        LocationManager.manager.updateLocation()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.init(red: 23.0/255.0, green: 19.0/255.0, blue: 67.0/255.0, alpha: 1.0)
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        DropDown.startListeningToKeyboard()
        GIDSignIn.sharedInstance().clientID = "349316153841-1mv5t6ki2o8jt7iauhruoktm9d82p5or.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
//        let userDefaults = UserDefaults.standard
//
//        if userDefaults.object(forKey: "ApplicationIdentifier") == nil
//        {
//            let UUID = NSUUID().uuidString
//            userDefaults.set(UUID, forKey: "ApplicationIdentifier")
//            userDefaults.synchronize()
//        }
//        print(UserDefaults.standard.value(forKey: "ApplicationIdentifier")!)
        
        if (launchOptions != nil)
        {
            let dictionary = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
            
            print(launchOptions as Any)
            
//            let alert = UIAlertController.init(title: "this is notification", message: launchOptions?.description, preferredStyle: .alert)
//            let action = UIAlertAction.init(title: "ok", style: .default, handler: nil)
//            alert.addAction(action)
//            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
            if (dictionary != nil)
            {
                print("launched from push notification")
                        guard let requestId = dictionary?.value(forKey: "requestid") as? String
                            else{
                                return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
                        }
                        print(requestId)
                        self.perform(#selector(self.notificationCheck(reqId:)), with: requestId, afterDelay: 1.0)
            }
        }
       
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        // Persist it in your backend in case it's new
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    

    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        print("Push notification received: \(data)")
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        
        if (UIApplication.shared.applicationState == .background) {
            return
        }
       
        if (UIApplication.shared.applicationState == .active) {
            return
        }
        guard let requestId = data[AnyHashable("requestid")] as? String
            else{
                return
        }
        print(requestId)
      self.perform(#selector(self.notificationCheck(reqId:)), with: requestId, afterDelay: 1.0)
     
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        UIDevice.current.isBatteryMonitoringEnabled = true
        print(messaging.apnsToken ?? 00)
        let userDefaults = UserDefaults.standard
        
        if userDefaults.object(forKey: "ApplicationUniqueIdentifier") == nil {
            userDefaults.set(fcmToken, forKey: "ApplicationUniqueIdentifier")
            userDefaults.synchronize()
        }
        print(userDefaults.object(forKey: "ApplicationUniqueIdentifier") as Any)
    }
    
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
        
    }
    
    @available(iOS 10, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        guard let requestId = userInfo[AnyHashable("requestid")] as? String
            else{
                return
        }
        print(requestId)
        
        self.perform(#selector(self.notificationCheck(reqId:)), with: requestId, afterDelay: 1.0)
        // Print full message.
        print("tap on on forground app",userInfo)
        
        completionHandler()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - GoogleSignInMethods
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        print("Sign In From Google Account")
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,
                                                                annotation: annotation)
        
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
        
        return googleDidHandle || facebookDidHandle
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        let sourceApplication =  options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        let annotation = options[UIApplicationOpenURLOptionsKey.annotation]
        
        let googleHandler = GIDSignIn.sharedInstance().handle(
            url,
            sourceApplication: sourceApplication,
            annotation: annotation )
        
        let facebookHandler = FBSDKApplicationDelegate.sharedInstance().application (
            app,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation )
        
        return googleHandler || facebookHandler
    }
    
    
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        if (error == nil) {
            
            
            // Perform any operations on signed in user here.
            //            let userId = user.userID                  // For client-side use only!
            //            let idToken = user.authentication.idToken // Safe to send to the server
            //            let fullName = user.profile.name
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            //            let email = user.profile.email
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    
    
    func showAlertForLowBattery()
    {
        if UIDevice.current.batteryLevel == -1
        {
            return
        }
        else if UIDevice.current.batteryLevel < 0.21
        {
            let alert = UIAlertController(title: "ZAPCO", message: "LOW BATTERY", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func notificationCheck(reqId:String)  {
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
        if let userData:Data  = UserDefaults.standard.value(forKey: stringConstants.userData) as? Data {
            let dictionary: NSDictionary = (NSKeyedUnarchiver.unarchiveObject(with: userData) as? NSDictionary)!
            let uData = Userdata()
            uData.userData = dictionary
            Shared.sharedInstance.userDetails = uData
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            switch dictionary["RoleId"] as! Int{
            case 2: //customer
                let controller = storyboard.instantiateViewController(withIdentifier: "ServiceHistoryDetailsViewController") as! ServiceHistoryDetailsViewController
                controller.notificationReqId = reqId
                UserDefaults.standard.set(false, forKey: "showRateView")
                UserDefaults.standard.synchronize()
                let rootView = self.window!.rootViewController as! UITabBarController
                print(rootView.selectedViewController as Any)
                let rootViewController = rootView.selectedViewController as! UINavigationController
                rootViewController.pushViewController(controller, animated: true)
                break
            case 4: //company technician
                let controller = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
                controller.notificationReqId = reqId
                //let rootViewController = self.window!.rootViewController as! UINavigationController
                self.window?.rootViewController?.navigationController?.pushViewController(controller, animated: true)
                break
            case 5: //freelanceTechnician
                let controller = storyboard.instantiateViewController(withIdentifier: "ReqDetailsViewController") as! ReqDetailsViewController
                controller.notificationReqId = reqId
                let rootView = self.window!.rootViewController as! UITabBarController
                print(rootView.selectedViewController as Any)
                let rootViewController = rootView.selectedViewController as! UINavigationController
                rootViewController.pushViewController(controller, animated: true)
                break
            default:
                break
            }
            
            
        }
        
        
    }
    
}

