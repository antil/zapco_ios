//
//  QuotesTableViewCell.swift
//  ZAPCO
//
//  Created by vinove on 10/23/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class QuotesTableViewCell: UITableViewCell {
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
   
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnView: customButton!
    @IBOutlet weak var lblRequstID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewCompletedAmt: UIView!
    
    @IBOutlet weak var lblAmount: UILabel!
    
    override func layoutSubviews() {
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
        Shadow.textShadow(view: viewCell)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
