//
//  SignUpViewController.swift
//  ZAPCO
//
//  Created by vinove on 9/14/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class SignUpViewController: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate
{

    
    @IBOutlet weak var imgVProfile: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    var locationValue = ["Latitude":"","Longitude":""]
    var imagePicker = UIImagePickerController()
    

    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        self.txtName.delegate = self
        self.txtConfirmPassword.delegate = self
        self.txtPhone.delegate = self
        
        imgVProfile.layer.cornerRadius = imgVProfile.frame.size.height/2.0
        imgVProfile.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.imgVProfile.contentMode = .scaleAspectFill
        self.locationValue.updateValue(String(LocationManager.manager.currentLocation.coordinate.latitude), forKey: "Latitude")
        self.locationValue.updateValue(String(LocationManager.manager.currentLocation.coordinate.longitude), forKey: "Longitude")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: - UITextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == txtName)
        {
            txtEmail.becomeFirstResponder()
        }
        else if (textField == txtEmail)
        {
            txtPassword.becomeFirstResponder()
        }
        else if(textField == txtPassword)
        {
            txtConfirmPassword.becomeFirstResponder()
        }
        else if(textField == txtConfirmPassword)
        {
            txtPhone.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return true;
    }
   
    
    func textFieldDidBeginEditing(_ textField: UITextField)
     {
            if textField == self.txtPhone
            {
                self.addDoneButtonOnKeyboard()
            }
    }
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width: 320, height:50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        self.txtPhone.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.txtPhone.resignFirstResponder()
    }
 
    
    //MARK: - IBActions
    
    @IBAction func btnUploadImage(_ sender: Any)
    {
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSignUp(_ sender: Any)
    {
        if txtName.text == ""{
            txtName.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter your Name", view: self)
            
        }
        else if txtEmail.text == ""{
            txtEmail.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Email ID", view: self)
            
        }
            
        else if txtPassword.text == ""{
             txtPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Password", view: self)
           
        }
        else if txtConfirmPassword.text == ""{
             txtConfirmPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Confirm Password", view: self)
           
        }
        else if txtConfirmPassword.text != txtPassword.text{
             txtPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Password Are Not Matched", view: self)
           
        }
        else if txtPhone.text == ""{
             txtPhone.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Phone Number", view: self)
           
        }
            
        else if (txtPassword.text?.count)! < 6 || (txtConfirmPassword.text?.count)! < 6
        {
            txtPassword.becomeFirstResponder()
            AlertControl.alert(appmassage: "Password Must Contain atlest 6 Characters", view: self)
            
        }
        
        else if isValidEmail(testStr: txtEmail.text!) == false
        {
            txtEmail.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Valid Email Id", view: self)
            
        }
       
        else if (txtPhone.text?.count)! < 10 || (txtPhone.text?.count)! > 12
        {
            txtPhone.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Valid Phone Number", view: self)
            
        }
        
        else
        {
        self.view.endEditing(true)
        self.getSignUp()
        }
    }
    
    @IBAction func btnLogIn(_ sender: Any)
    {
      self.navigationController?.popViewController(animated: true)
      
    }
    
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - UploadImageMethods
    func openGallery()
    {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - ImagePickerDelegates
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:  [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imgVProfile.image = chosenImage
        self.imgVProfile.contentMode = .scaleAspectFill
        dismiss(animated: true, completion: nil)
    }
    
    
    
    //MARK: - EmailValidations
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK: - Webservices
    func getSignUp()
    {
            //{"userType":"", "name":"", "email":"", "password":"", "phoneNumber":"", "profileImage":"", deviceType:"web/iOS/Android",deviceToken:"",deviceOsVersion:""}
        let strImageName = "C_" + txtName.text!
        self.locationValue.updateValue(String(LocationManager.manager.currentLocation.coordinate.latitude), forKey: "Latitude")
        self.locationValue.updateValue(String(LocationManager.manager.currentLocation.coordinate.longitude), forKey: "Longitude")
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            
            let deviceId = UIDevice.current.identifierForVendor!.uuidString
            let systemVersion = UIDevice.current.systemVersion
            let token = UserDefaults.standard.value(forKey: "ApplicationUniqueIdentifier") as! String
            
            let param = NSMutableDictionary()
            param.setValue("2", forKey: "roleId")
            param.setValue(self.txtName.text, forKey: "name")
            param.setValue(self.txtEmail.text, forKey: "email")
            param.setValue(self.txtPassword.text, forKey: "password")
            param.setValue(self.txtPhone.text, forKey: "phoneNumber")
            param.setValue(systemVersion, forKey: "deviceOsVersion")
            param.setValue("iOS", forKey: "deviceType")
            param.setValue(token, forKey: "deviceId")
            param.setValue(strImageName, forKey: "profileImageName")
            param.setValue(self.imageConvertMethod(img: self.imgVProfile.image!), forKey: "profileImageString")
            param.setValue(self.locationValue, forKey: "location")
           
            let tokenUrl = ServiceUrl.signUpURL + "signUp"
            
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : ""){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success)
                {
                    if (response["Code"] as AnyObject).integerValue == 200
                    {
                        guard let data:NSDictionary = response["Data"] as? NSDictionary else { return }
                        let uData = Userdata()
                        uData.userData = data
                        Shared.sharedInstance.userDetails = uData
                        let userDta: Data = NSKeyedArchiver.archivedData(withRootObject: uData.userData)
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(userDta, forKey: stringConstants.userData)
                        self.getToken()
                    }
                    else
                    {
                        AlertControl.alert(appmassage: response["Message"] as! String, view: self)
                    }
                }
            }
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
        
    }
    
    func getToken()
    {
        self.view.endEditing(true)
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        
        let param = NSMutableDictionary()
        param.setValue("password", forKey: "grant_Type")
        param.setValue(self.txtEmail.text!, forKey: "username")
        param.setValue(self.txtPassword.text!, forKey: "password")
        
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        let tokenUrl = ServiceUrl.baseUrl + "/token"
        VTAlmofireHelper.sharedInstance.post(with: URL(string:tokenUrl)!, method: "POST", params: param as? Dictionary<String, Any>, complition: { (response, error) -> (Void) in
            print(response ?? 00)
            
            if let error:String = response!["error_description"] as? String
            {
                if error == "The user name or password is incorrect."
                {
                    AlertControl.alert(appmassage:error, view: self)
                    DispatchQueue.main.sync {
                        ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                        self.txtPassword.text = ""
                        self.txtEmail.text = ""
                    }
                    return
                }
                
            }
            
            let dict = response! as NSDictionary
            let tokenDATA = TokenData()
            tokenDATA.tokenType = dict.value(forKey: "token_type") as! NSString
            tokenDATA.accesstoken = dict.value(forKey: "access_token") as! NSString
            tokenDATA.userID = dict.value(forKey: "userID") as! NSString
            Shared.sharedInstance.token_Data = tokenDATA
            Shared.sharedInstance.tokenDict = dict as! [String : Any]
            let tokenDta: Data = NSKeyedArchiver.archivedData(withRootObject: Shared.sharedInstance.tokenDict)
            let userDefaults = UserDefaults.standard
            userDefaults.setValue(tokenDta, forKey: stringConstants.tokenData)
             DispatchQueue.main.sync
                {
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "tabController")
            appDelegate.window?.rootViewController = initialViewController
            appDelegate.window?.makeKeyAndVisible()
            AlertControl.alert(appmassage: "Registration is Done Successfully!", view: self)
            }
            
        })
        }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
        
    }
    
    func imageConvertMethod(img:UIImage)->String{
        let imageData: NSData = UIImageJPEGRepresentation(img, 0.4)! as NSData
        let imageStr = imageData.base64EncodedString(options:.lineLength64Characters)
        
        return imageStr
    }
        
}
    
