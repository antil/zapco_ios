//
//  landingViewController.swift
//  ZAPCO
//
//  Created by vinove on 9/13/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class landingViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        var initialViewController = UIViewController()
        
        if let tokenDict:Data = UserDefaults.standard.value(forKey: stringConstants.tokenData) as? Data {
            let dict: Dictionary? = NSKeyedUnarchiver.unarchiveObject(with: tokenDict) as? [String : Any]
            let tokenDATA = TokenData()
            tokenDATA.tokenType = dict!["token_type"] as! NSString
            tokenDATA.accesstoken = dict!["access_token"] as! NSString
            tokenDATA.userID = dict!["userID"] as! NSString
            Shared.sharedInstance.token_Data = tokenDATA
        }
        
        if let userData:Data  = UserDefaults.standard.value(forKey: stringConstants.userData) as? Data {
            
            let dictionary: NSDictionary = (NSKeyedUnarchiver.unarchiveObject(with: userData) as? NSDictionary)!
            
            let uData = Userdata()
            uData.userData = dictionary
            Shared.sharedInstance.userDetails = uData
            
            switch dictionary["RoleId"] as! Int{
                
            case 2:
                initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "tabController")
                break
            case 4:
                 initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "NavTechnicianController")
                break
            case 5:
                 initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "freeTabbar")
                  break
            default:
                break
            }
            
            appDelegate.window?.rootViewController = initialViewController
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnCustomer(_ sender: Any) {
        
        let control =  self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        control.loginType = "Customer"
        self.navigationController?.pushViewController(control, animated: true)
        
    }
    
    @IBAction func btnCompanyTech(_ sender: Any) {
        let control =  self.storyboard?.instantiateViewController(withIdentifier: "TechLoginViewController") as! TechLoginViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func btnFreeLance(_ sender: Any) {
        
        let control =  self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        control.loginType = "Freelance"
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func btnClickLink(_ sender: Any)
    {
        guard let url = URL(string: "http://www.zapcopmc.com") else
        {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
}

