//
//  AddQuotesViewController.swift
//  ZAPCO
//
//  Created by vinove on 10/23/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import DropDown
import MapKit
import GooglePlaces

class AddQuotesViewController: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate, GMSAutocompleteResultsViewControllerDelegate
{
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var txtSelectTask: UITextField!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var txtDescr: UITextField!
    @IBOutlet weak var txtSelectReq: UITextField!
    @IBOutlet weak var reqNo: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtClientName: UITextField!
    @IBOutlet weak var txtDateService: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    var reqData = NSDictionary()
    var datePicker = UIDatePicker()
    var timeField = Bool()
    var imagePicker = UIImagePickerController()
    var strReqN0: String?
    
    var myPicker = UIPickerView()
    var txtfieldPicker = UITextField()
    var arrPicker = NSMutableArray()

    var arrRequests = NSMutableArray()
    var arrCategoryID = NSMutableArray()
    var arrTasks = NSMutableArray()
    var arrData = NSArray()
    var catID = NSInteger()
    var isTypeService = Bool()
    var arrSubData = NSArray()
    var subCatID = NSInteger()
    var arrSubCat = NSMutableArray()
    var selectDate = Bool()
    var requestorId:String!
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?

    
    //handle Address
    var locationManager = CLLocationManager()
    
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
        self.txtSelectReq.delegate = self
        self.txtDescr.delegate = self
        self.txtTime.delegate = self
        self.txtClientName.delegate = self
        self.txtDateService.delegate = self
        self.txtAddress.delegate = self
        self.txtSelectTask.delegate = self
        
        Shadow.textShadow(view: viewTop)
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
        txtAddress.addGestureRecognizer(tap)
        txtAddress.resignFirstResponder()
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        definesPresentationContext = true
        searchController?.hidesNavigationBarDuringPresentation = false
    
        getCategory()
        getSubCategory()
        
        txtTime = TextfieldImage.textfieldImageRight(txt: txtTime)
        txtSelectReq = TextfieldImage.textfieldImageRight(txt: txtSelectReq)
        txtSelectTask = TextfieldImage.textfieldImageRight(txt: txtSelectTask)
        
        print(Shared.sharedInstance.userDetails.userData)
        //reqNo.text = "\(Shared.sharedInstance.userDetails.userData.value(forKey: "UserId")!)"
        reqNo.text = strReqN0
        let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 17, height: 17))
        let image = UIImage(named: "calender")
        imgV.image = image
        txtDateService.rightViewMode = UITextFieldViewMode.always
        txtDateService.rightView = imgV
    }
    
    func handleTap()
    {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    func  setUpView()
    {
       // reqNo.text = self.reqData.value(forKey: "RequestId") as? String
        
    }
    
    
    //MARK: - UITextfieldDelegates
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == self.txtTime
        {
            isTypeService = false
            selectDate = false
            self.pickUpDate(self.txtTime)
            self.timeField = true
        }
        if textField == self.txtDateService
        {
            isTypeService = false
            selectDate = true
            self.pickUpDate(self.txtDateService)
            self.timeField = false
        }
        if (textField == txtSelectTask)
        {
            isTypeService = false
            arrTasks.removeAllObjects()
            if txtSelectReq.text == ""
            {
                AlertControl.alert(appmassage: "Please select the request", view: self)
            }
            else
            {
                for dict in self.arrSubData
                {
                    let catDict = dict as! NSDictionary
                    if let subCat = catDict.value(forKey: "CategoryId") as? NSInteger{
                        print(subCat)
                        if self.catID == catDict.value(forKey: "CategoryId") as! NSInteger
                        {
                            self.arrTasks.add(catDict.value(forKey: "SubCategory")!)
                            self.arrSubCat.add(catDict.value(forKey: "SubCategoryId")!)
                        }
                    }
                    if self.arrSubData.count == 0
                    {
                        self.arrTasks.add("")
                    }
                }
                txtfieldPicker = txtSelectTask
                arrPicker = arrTasks
                self.pickUp(txtfieldPicker)
            }
        }
        
        if (textField == txtSelectReq)
        {
            isTypeService = true
            txtfieldPicker = txtSelectReq
            arrPicker = arrRequests
            self.pickUp(txtfieldPicker)
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        textField.resignFirstResponder()
        myPicker.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    //MARK: - DatePickerMethods
    func pickUpDate(_ textField : UITextField)
    {
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        textField.inputView = self.datePicker
        if selectDate == true
        {
          self.datePicker.datePickerMode = UIDatePickerMode.date
        }
        else
        {
            self.datePicker.datePickerMode = UIDatePickerMode.time
        }
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func doneClick()
    {
        let dateFormatter1 = DateFormatter()
        if timeField == true
        {
            dateFormatter1.dateFormat = "HH:mm:ss"
            self.txtTime.text = dateFormatter1.string(from: datePicker.date)
            self.txtTime.resignFirstResponder()
        }
        else{
            dateFormatter1.dateFormat = "MM-dd-yyyy"
            self.txtDateService.text = dateFormatter1.string(from: datePicker.date)
            self.txtDateService.resignFirstResponder()
            
        }
        
    }
    func cancelClick()
    {
        self.txtTime.resignFirstResponder()
        self.txtDateService.resignFirstResponder()
        
    }
    
    //MARK: - UIPickerViewMethods
    func pickUp(_ textField : UITextField)
    {
        // UIPickerView
        myPicker = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        myPicker.delegate = self
        myPicker.dataSource = self
        myPicker.backgroundColor = UIColor.white
        textField.inputView = myPicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.doneClick1))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick1))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick1()
    {
        self.txtfieldPicker.resignFirstResponder()
    }
    @objc func cancelClick1()
    {
        self.txtfieldPicker.resignFirstResponder()
    }
    
    
    //MARK: - UIPickerViewDatasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        if arrPicker.count == 0
        {
            return 0
        }
        else
        {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if arrPicker.count == 0
        {
            return 0
        }
        else
        {
            return arrPicker.count
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if !(arrPicker.count == 0)
        {
            return arrPicker[row] as? String
        }
        else
        {
            return ""
        }
    }
    
    
    //MARK: - UIPickerviewDelegates
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent  component: Int)
    {
        txtfieldPicker.text = arrPicker[row] as? String
        // let dictCat = arrData[row] as! NSDictionary
        if isTypeService{
            self.catID = arrCategoryID[row] as! NSInteger
        }
        else if arrSubCat.count != 0
        {
            if let scatID = arrSubCat[row] as? NSInteger {
                self.subCatID = scatID
            }
        }
    }
    
    
    
    //MARK: - IBActions
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReset(_ sender: Any)
    {
        txtDateService.text = ""
        txtTime.text = ""
        txtSelectTask.text = ""
        txtSelectReq.text = ""
        txtDescr.text = ""
        txtAddress.text = ""
        txtClientName.text = ""
        imgUser.image = UIImage.init(named: "user-1")
        
    }
    
    @IBAction func btnSubmit(_ sender: Any)
    {
       
        if txtClientName.text == "" || txtAddress.text == "" || txtTime.text == "" || txtDescr.text == "" || txtSelectReq.text == "" || txtSelectTask.text == "" || txtDateService.text == ""
        {
            AlertControl.alert(appmassage: "Kindly fill all fields", view: self)
        }
        else
        {
        self.callService()
        }
    }
    
    
    @IBAction func btnReqclick(_ sender: Any)
    {
        let searchResult = self.storyboard?.instantiateViewController(withIdentifier: "RequestListViewController") as! RequestListViewController
        self.navigationController?.pushViewController(searchResult, animated: true)
    }
    
    
    @IBAction func btnAccClick(_ sender: Any)
    {
        let searchResult = self.storyboard?.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
        self.navigationController?.pushViewController(searchResult, animated: true)
    }
    
    
    @IBAction func btnQuotesClick(_ sender: Any)
    {
        
        let searchResult = self.storyboard?.instantiateViewController(withIdentifier: "QuotesViewController") as! QuotesViewController
        self.navigationController?.pushViewController(searchResult, animated: true)
    }
    
    
    @IBAction func btnCamera(_ sender: Any)
    {
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    //MARK: - ImagePickerMMehods
    func openGallary()
    {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:  [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imgUser.contentMode = .scaleAspectFill
        self.imgUser.image = chosenImage
        dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - WebServiceMethods
    func getCategory()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        let tokenUrl = ServiceUrl.CommonUrl + "category"
        // let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : ""){
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                    let data = response["Data"] as! NSArray
                    self.arrData = data
                    for dict in data
                    {
                        let catDict = dict as! NSDictionary
                        if  let category = catDict.value(forKey: "Category") as? String{
                            self.arrRequests.add(category)
                            self.arrCategoryID.add(catDict.value(forKey: "CategoryId") as! NSInteger)
                        }
                        if self.arrData.count == 0
                        {
                            self.arrRequests.add("")
                        }
                        self.myPicker.reloadAllComponents()
                    }
                    
                }
            }
            }}
        else
        {
            return
        }
        
    }
    
    func getSubCategory()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        let tokenUrl = ServiceUrl.CommonUrl + "subcategory"
        // let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : ""){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                    let data = response["Data"] as! NSArray
                    self.arrSubData = data
                }
            }
            }}
        else
        {
            return
        }
    }
    
    
    func callService()
    {
        ///{"RequestId":1231, "Category":"Electrician_edit", "SubCategory":"Site Construction sdfsdf", "Description":"Testing","TimeLog":"05:22:00", "RequesterImageName":"Tester","RequesterImageString":"","RequesterId":"1231", "RequesterName":"Tester", "RequesterAddress":"Udyog Nagar, Delhi","DateOfService":"2018-05-22"}
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        var imageStr = String()
        if (imgUser.image != nil)
        {
            let imageData: NSData = UIImageJPEGRepresentation(imgUser.image!, 0.4)! as NSData
            imageStr = imageData.base64EncodedString(options:.lineLength64Characters)
        }
        let param = NSMutableDictionary()
        param.setValue(reqNo.text, forKey: "RequestId")
        param.setValue(txtSelectReq.text, forKey: "Category")
        param.setValue(txtSelectTask.text, forKey: "SubCategory")
        param.setValue(self.txtDescr.text, forKey: "Description")
        param.setValue(self.txtTime.text, forKey: "TimeLog")
        param.setValue(imageStr, forKey: "RequesterImageString")
        param.setValue("pics", forKey: "RequesterImageName")
        param.setValue(strReqN0, forKey: "RequesterId")
        param.setValue(self.txtClientName.text, forKey: "RequesterName")
        param.setValue(self.txtAddress.text, forKey: "RequesterAddress")
        param.setValue(self.txtDateService.text, forKey: "DateOfService")
        
        let Url = ServiceUrl.companyTechnicianUrl + "addQuotes"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: Url, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    //AlertControl.alert(appmassage:(response["Message"] as? String)! , view: self)
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    AlertControl.alert(appmassage:(response["Message"] as? String)!, view: self)
                }
            }
            else
            {
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
            }}
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace)
    {}
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {}
    
}


extension AddQuotesViewController: GMSAutocompleteViewControllerDelegate
{
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        txtAddress.text = place.formattedAddress
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


