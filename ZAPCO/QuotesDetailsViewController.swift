//
//  QuotesDetailsViewController.swift
//  ZAPCO
//
//  Created by vinove on 10/23/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class QuotesDetailsViewController: UIViewController {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var requestedFrom: UILabel!
    @IBOutlet weak var requestedFor: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblDescr: UILabel!
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var lblReqNumber: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblEstCost: UILabel!
    @IBOutlet weak var lblTotalCost: UILabel!
    var quotesDetails = NSDictionary()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print(quotesDetails)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.setUpViw()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func setUpViw(){
        
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height/2.0
        self.imgUser.layer.masksToBounds = true
        self.imgUser.layer.borderWidth = 1.0
        self.imgUser.layer.borderColor = UIColor.lightGray.cgColor
        Shadow.textShadow(view: self.viewTop)
        Shadow.textShadow(view: self.viewBase)
        
        self.requestedFrom.text = (self.quotesDetails.value(forKey: "Name") as! String)
        self.requestedFor.text = (self.quotesDetails.value(forKey: "Category") as! String)
        if let ps = self.quotesDetails.value(forKey: "PaymentStatus") as? String
        {
            self.lblPaymentStatus.text = ps
        }
        
        if let amt = self.quotesDetails.value(forKey: "Amount") as? String{
            
            self.lblEstCost.text = "$ " + amt
            self.lblTotalCost.text = "$ " + amt
        }
        else{
            self.lblEstCost.text = "Not Applicable"
            self.lblTotalCost.text = "Not Applicable"
        }
        self.lblReqNumber.text = "\(self.quotesDetails.value(forKey: "RequestId")!)"
        
        
        if let dat = self.quotesDetails.value(forKey: "DateTime") as? String
        {
            self.lblDate.text = ShowTime.dateConvert(date: dat) as String
            
        }
        if let tym = self.quotesDetails.value(forKey: "TimeLog") as? String
        {
            self.lblTime.text = ShowTime.showTime(time: tym) as String
        }
        
        if let decr = self.quotesDetails.value(forKey: "Description") as? NSString{
            self.lblDescr.text = decr as String
        }
        else{
            self.lblDescr.text = ""
        }

        if let img = self.quotesDetails.value(forKey: "RequesterImagePath") as? String
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: img)
            if profileUrl != nil
            {
                self.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                self.imgUser.image = placeholderImage
            }
        }
        else{
            self.imgUser.image = UIImage(named:"user-1")
        }
        
    }
    
    @IBAction func btnRequst(_ sender: Any) {
        let searchResult = self.storyboard?.instantiateViewController(withIdentifier: "RequestListViewController") as! RequestListViewController
        self.navigationController?.pushViewController(searchResult, animated: true)
    }
    
    @IBAction func btnQuotesClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    
    @IBAction func btnAccClick(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

}
