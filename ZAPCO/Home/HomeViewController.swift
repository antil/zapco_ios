//
//  HomeViewController.swift
//  ZAPCO
//
//  Created by vinove on 9/14/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit
import Foundation
import DropDown
import Toast_Swift
import Reachability
import GoogleMaps

class HomeViewController: UIViewController,UITextFieldDelegate, CLLocationManagerDelegate, locationDelegates
{
    
    @IBOutlet var mapV: GMSMapView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var txtTechnicianType: UITextField!
    @IBOutlet weak var txtManPower: UITextField!
    @IBOutlet weak var txtSubCategry: UITextField!
    @IBOutlet weak var txtSelectDistance: UITextField!
    
    @IBOutlet weak var viewMapList: CustomView!
    
    let ddTechnician = DropDown()
    let ddCategory = DropDown()
    let ddSubCategory = DropDown()
    let ddDistance = DropDown()
    let arrDistance = ["1 KM","5 KM","10 KM","15 KM","20 KM"]
    var currentDate = String()
    var currentTime = String()
    var locationM = CLLocationManager()
    let arrTechnician = ["Company","Freelancer"]
    var arrCategory = NSMutableArray()
    var arrCategoryID = NSMutableArray()
    var arrData = NSArray()
    var catID = NSInteger()
    
    var arrSubCategory = NSMutableArray()
    var arrSubData = NSArray()
    var subCatID = NSInteger()
    
    var location = ["Latitude":"28.980955", "Longitude":"77.180955" ]
    var technicianType = NSInteger()
    
    var arrListData = NSMutableArray()
    var arrLocations = NSMutableArray()
    var style = ToastStyle()
    var marker = GMSMarker()
    var cameraPosition = GMSCameraPosition()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Shadow.textShadow(view: self.viewTop)
        Shadow.buttonShadow(view: btnMap)
        Shadow.buttonShadow(view: btnList)
        Shadow.textShadow(view: viewMapList)
    
        self.btnMap.isSelected = true
        locationM.delegate = self
        
        self.tableViewList.delegate = self
        self.tableViewList.dataSource = self
        self.tableViewList.isHidden = true
        self.txtTechnicianType.delegate = self
        self.txtManPower.delegate = self
        self.txtSubCategry.delegate = self
        self.txtSelectDistance.delegate = self
    
        txtManPower = setImageTextfield(txt: txtManPower)
        txtTechnicianType = setImageTextfield(txt: txtTechnicianType)
        txtSubCategry = setImageTextfield(txt: txtSubCategry)
        txtSelectDistance = setImageTextfield(txt: txtSelectDistance)
        
        self.perform(#selector(showUserLocation(currentLocation:)), with: nil, afterDelay: 1.0)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("PayUResponse"), object: nil)
        
    }
    
    @IBAction func tapDirection(_ sender: UIButton)
    {
//        if sender.isSelected == true
//        {
//            sender.isSelected = false
//            mapV.animate(toZoom: 15.0)
//        }
//        else
//        {
//            sender.isSelected = true
//            mapV.animate(toZoom: 40.0)
//        }
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(Float(LocationManager.manager.currentLocation.coordinate.latitude)),\(Float(LocationManager.manager.currentLocation.coordinate.longitude))&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
        }
        else
        {
            print("Can't use com.google.maps://")
        }
    }
    
    func setImageTextfield(txt: UITextField) -> UITextField
    {
        let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        let image = UIImage(named: "dropdown")
        imgV.image = image
        txt.rightViewMode = UITextFieldViewMode.always
        txt.rightView = imgV
        txt.attributedPlaceholder = NSAttributedString(string: txt.placeholder!,
                                                                     attributes: [NSForegroundColorAttributeName:UIColor.init(red: 71.0/255.0, green: 71.0/255.0, blue: 71.0/255.0, alpha: 1.0)])
        return txt
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        txtSubCategry.text = ""
        //self.arrListData.removeAllObjects()
        //self.tableViewList.reloadData()
        self.navigationController?.navigationBar.isHidden = true
        LocationManager.manager.delegates = self
        if CLLocationManager.locationServicesEnabled()
        {
            switch CLLocationManager.authorizationStatus()
            {
            case .notDetermined, .restricted, .denied:
                showLocationPopUp()
                print("Denied")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        }
        else
        {
            print("Location services are not enabled")
        }
        
        self.getCategory()
        self.getSubCategory()
        self.dropPopUp()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    
    func showUserLocation(currentLocation: CLLocation)
    {
       cameraPosition =  GMSCameraPosition.camera(withLatitude: LocationManager.manager.currentLocation.coordinate.latitude, longitude: LocationManager.manager.currentLocation.coordinate.longitude, zoom: 15.0, bearing: 0.0, viewingAngle: 0.0)
        mapV.camera = cameraPosition
        
        marker.position = CLLocationCoordinate2D(latitude: LocationManager.manager.currentLocation.coordinate.latitude, longitude:LocationManager.manager.currentLocation.coordinate.longitude )
        marker.title = "My location"
        marker.icon = UIImage.init(named: "location_marker")
        mapV.isMyLocationEnabled = true
        mapV.settings.compassButton = true
        marker.map = mapV
        
        let directionButton = UIButton.init(frame: CGRect(x: self.view.frame.size.width - 40, y: self.view.frame.size.height - 100, width: 30, height: 30))
        directionButton.setImage(UIImage.init(named: "direction"), for: .normal)
        directionButton.backgroundColor = UIColor.red
        directionButton.addTarget(self, action: #selector(tapDirection(_:)), for: .touchUpInside)
        self.mapV.addSubview(directionButton)

    }
   
    func updatedAddress(currentLocation: CLLocation)
    {
        marker.position = CLLocationCoordinate2D(latitude: LocationManager.manager.currentLocation.coordinate.latitude, longitude:LocationManager.manager.currentLocation.coordinate.longitude )
        marker.title = "My location"
        marker.icon = UIImage.init(named: "location_marker")
        mapV.isMyLocationEnabled = true
        mapV.settings.compassButton = true
        marker.map = mapV
    }
    
    func showLocationPopUp()
    {
        let alert = UIAlertController(title: "Device Location Off!", message: "Enable Location?", preferredStyle: UIAlertControllerStyle.alert)
        alert.setValue(NSAttributedString(string: alert.message!, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 16, weight: UIFontWeightMedium), NSForegroundColorAttributeName : UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)]), forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in
            self.enableLocation()
        }))
        alert.addAction(UIAlertAction(title: "NO!", style: UIAlertActionStyle.default, handler: nil))
        //alert.view.tintColor = UIColor.init(red: 35.0/255.0, green: 15.0/255.0, blue: 120.0/255.0, alpha: 1.0)
        alert.view.tintColor = UIColor.black
        self.present(alert, animated: true, completion: nil)
    }
    
    func enableLocation()
    {
        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.sid.ZAPCO")
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    
    //MARK: - DropdownMethods
    func dropPopUp()
    {
        txtSelectDistance.resignFirstResponder()
        txtManPower.resignFirstResponder()
        txtSubCategry.resignFirstResponder()
        txtTechnicianType.resignFirstResponder()
        ddTechnician.direction = .bottom
        ddCategory.direction = .bottom
        DropDown.appearance().textColor = UIColor.lightGray
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 15)
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.white
        DropDown.appearance().cellHeight = 50
        self.ddTechnician.anchorView = self.txtTechnicianType
        self.ddTechnician.topOffset = CGPoint(x: 0, y: (self.ddTechnician.anchorView?.plainView.bounds.height)!)
        self.ddTechnician.dataSource = self.arrTechnician
        self.ddTechnician.selectionAction = { [unowned self] (index, item) in
            self.txtTechnicianType.text = item
            if item == "Company" {
              self.technicianType = 4
            }else{
                self.technicianType = 5
            }
        }
        self.txtTechnicianType.text = self.ddTechnician.selectedItem
        self.txtTechnicianType.resignFirstResponder()
        
        self.ddDistance.anchorView = self.txtSelectDistance
        self.ddDistance.topOffset = CGPoint(x: 0, y: (self.ddDistance.anchorView?.plainView.bounds.height)!)
        self.ddDistance.dataSource = self.arrDistance
        self.ddDistance.selectionAction = { [unowned self] (index, item) in
            self.txtSelectDistance.text = item
            
        }
        self.txtSelectDistance.text = self.ddDistance.selectedItem
        self.txtSelectDistance.resignFirstResponder()
        btnCornerChange.changeCorner(button:self.btnMap)
        btnCornerChange.changeCorner2(button:self.btnList)
        
    }
    
    
    
    //MARK: - WebServiceMethods
    func getCategory(){
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        let tokenUrl = ServiceUrl.CommonUrl + "category"
        //"Content-type": "application/json"
        //"Authorization":token_type + access_token
        
          let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    self.arrCategory.removeAllObjects()
                    self.arrCategoryID.removeAllObjects()
                    let data = response["Data"] as! NSArray
                    self.arrData = data
                    for dict in data{
                        let catDict = dict as! NSDictionary
                        
                       if  let categry = catDict.value(forKey: "Category") as? String{
                        self.arrCategory.add(categry)
                        self.arrCategoryID.add(catDict.value(forKey: "CategoryId") as! NSInteger)
                    }
               
                    }
                    
                    self.ddCategory.anchorView = self.txtManPower
                    self.ddCategory.topOffset = CGPoint(x: 0, y: (self.ddCategory.anchorView?.plainView.bounds.height)!)
                    self.ddCategory.dataSource = self.arrCategory as! [String]
                    self.ddCategory.selectionAction = { [unowned self] (index, item) in
                        self.txtManPower.text = item
                        print(self.arrData[index])
                        self.catID = self.arrCategoryID.object(at: index) as! NSInteger
                        
                    }
                    self.txtManPower.text = self.ddCategory.selectedItem
                   
                }
            }
            
            else{
                AlertControl.alert(appmassage: "Try Again Later!", view: self)
            }
        }
        }
        else
        {
            return
        }
        
    }
    
    func getSubCategory()
    {
 
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        
        let tokenUrl = ServiceUrl.CommonUrl + "subcategory"
        //"Content-type": "application/json"
        //"Authorization":token_type + access_token
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "GET",jsonRequest: [:] ,  url: tokenUrl, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200{
                    
                    let data = response["Data"] as! NSArray
                    self.arrSubData = data
                }
            }
            else{
                AlertControl.alert(appmassage: "Try Again Later!", view: self)
            }
        }
        }
        else
        {
            //AlertControl.alert(appmassage: "No Internet Connection!", view: self)
            return
        }
    }
    
    
    func technicianRequest()
    {
        //{"CategoryID":"1","Date":"04-25-2018","distance":"30 KM","Location":{"Latitude":"28.6810685","Longitude":"77.0853913"},"SubCategoryID":"20","technicianType":"2","Time":"05:19:14"}
        
        self.location.updateValue(String(LocationManager.manager.currentLocation.coordinate.latitude), forKey: "Latitude")
        self.location.updateValue(String(LocationManager.manager.currentLocation.coordinate.longitude), forKey: "Longitude")
        
        if  VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            
            self.arrListData.removeAllObjects()
            self.arrLocations.removeAllObjects()
        let param = NSMutableDictionary()
        param.setValue(self.technicianType, forKey: "technicianType")
        param.setValue(self.catID, forKey: "CategoryID")
        param.setValue(self.subCatID, forKey: "SubCategoryID")
        param.setValue(self.currentDate, forKey: "Date")
        param.setValue(self.currentTime, forKey: "Time")
        param.setValue(self.txtSelectDistance.text, forKey: "distance")
        param.setValue(self.location, forKey: "Location")
      
        let tokenUrl = ServiceUrl.customerUrl + "technicianRequest"
        let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
        
        VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
            
            (response:Dictionary,success:Bool) in
            print(response)
            
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
            if(success){
                if (response["Code"] as AnyObject).integerValue == 200
                {
                    let arrData =  response["Data"] as! NSArray
                    for dict in arrData{
                        let dictData = dict as! NSDictionary
                        let reqList = CustomerRequest()
                        reqList.requestDict = dictData
                        reqList.CatID = dictData.value(forKey: "CategoryID") as! NSInteger
            
                        if let  reviews = dictData.value(forKey: "CustomerReviewList") as? NSArray
                        {
                            reqList.customerReviewList = reviews
                        }
                        
                        if let distance =  dictData.value(forKey: "Distance") as? NSString{
                            reqList.distance = distance
                        }else{
                            reqList.distance  = "0"
                        }
                        if let  license = dictData.value(forKey: "LicenseNumber") as? NSString{
                            reqList.licenseNumber = license
                        }
                        if let  license = dictData.value(forKey: "PhoneNumber") as? NSString{
                            reqList.phone = license
                        }
                        if let name = dictData.value(forKey: "Name") as? NSString{
                            reqList.name = name
                        }
                        if let userId = dictData.value(forKey: "UserId") as? NSNumber{
                            reqList.userId = userId
                        }
                        if let Latitude = dictData.value(forKey: "Latitude") as? NSNumber
                        {
                            reqList.lat = Latitude
                            reqList.long = dictData.value(forKey: "Longitude") as! NSNumber
                
                            reqList.location.updateValue(Double(Latitude), forKey: "Latitude")
                            reqList.location.updateValue(Double(dictData.value(forKey: "Longitude") as! NSNumber), forKey: "Longitude")
                            self.arrLocations.add(reqList.location)
                            print(self.arrLocations)
                        }
                       
                        if let image = dictData.value(forKey: "ProfileImagePath") as? NSString{
                            reqList.imageStr = image
                        }
                        if let rating = dictData.value(forKey: "Rating") as? NSInteger{
                            reqList.rating = rating
                        }
                        reqList.subCat = dictData.value(forKey: "SubCategoryID") as! NSInteger
                        reqList.techID = dictData.value(forKey: "TechnicianId") as! NSInteger
                        reqList.techType = dictData.value(forKey: "TechnicianType") as! NSString
                        self.arrListData.add(reqList)
                    }
                
                    if self.arrListData.count > 0
                    {
                        self.tableViewList.reloadData()
                        
                        if self.arrLocations.count > 0
                        {
                            for i in 0..<self.arrLocations.count
                            {
                                let dic = self.arrLocations[i] as! NSDictionary
                                let camera = GMSCameraPosition.camera(withLatitude: dic.value(forKey: "Latitude") as! CLLocationDegrees, longitude: dic.value(forKey: "Longitude") as! CLLocationDegrees, zoom: 15.0)
                                 self.mapV.camera = camera
                                
                                let marker1 = GMSMarker()
                                marker1.position = CLLocationCoordinate2D(latitude: dic.value(forKey: "Latitude") as! CLLocationDegrees, longitude:dic.value(forKey: "Longitude") as! CLLocationDegrees )
                                marker1.title = "Technician \(i + 1)"
                                marker1.icon = UIImage.init(named: "location_marker")
                                marker1.map = self.mapV
                            }
                        }
                    }

                    else
                    {
                        self.view.makeToast("No Technician Available", duration: 0.5, position: .center, style: self.style)
                        self.tableViewList.reloadData()
                    }
            }
                else{
                    AlertControl.alert(appmassage: "Try again later!", view: self)
                }
            }
        }
    }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
    
   
     func viewTechnician(_ sender:UIButton)
     {
        let objects = self.arrListData.object(at: sender.tag) as! CustomerRequest
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "TechnicianDetailsViewController") as! TechnicianDetailsViewController
        
        controller.techDetails = objects.requestDict
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func bookTechnician(_ sender:UIButton)
    {
        let objects = self.arrListData.object(at: sender.tag) as! CustomerRequest
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let viewPopUp = BookTechnician(frame: frame)
        viewPopUp.backgroundColor = UIColor.clear
        viewPopUp.technicianID = objects.techID
        viewPopUp.categoryID = objects.CatID
        viewPopUp.subCatID = objects.subCat
        viewPopUp.userLoc.updateValue(LocationManager.manager.currentLocation.coordinate.latitude, forKey: "Latitude")
        viewPopUp.userLoc.updateValue(LocationManager.manager.currentLocation.coordinate.longitude, forKey: "Longitude")
        viewPopUp.strBookFrom = "HomeView"
        viewPopUp.obj2 = self
        self.view.addSubview(viewPopUp)
    }
    
    
    //MARK: - TextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        ddTechnician.hide()
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.resignFirstResponder()
        if textField == txtTechnicianType
        {
            ddTechnician.show()
        }
        if textField == txtManPower
        {
            ddCategory.show()
        }
        
        if textField == self.txtSubCategry
        {
            self.arrSubCategory.removeAllObjects()
            
        if txtManPower.text == ""{
     AlertControl.alert(appmassage: "Please select the category", view: self)
            }else{
            
            for dict in self.arrSubData{
                let catDict = dict as! NSDictionary
        if let subCat = catDict.value(forKey: "CategoryId") as? NSInteger{
            
            print(subCat)
                
                if self.catID == catDict.value(forKey: "CategoryId") as! NSInteger{
                    
                    self.arrSubCategory.add(catDict.value(forKey: "SubCategory")!)
                }
            
                }
            }
            
            self.ddSubCategory.anchorView = self.txtSubCategry
            self.ddSubCategory.topOffset = CGPoint(x: 0, y: (self.ddSubCategory.anchorView?.plainView.bounds.height)!)
            self.ddSubCategory.dataSource = self.arrSubCategory as! [String]
            self.ddSubCategory.selectionAction = { [unowned self] (index, item) in
                self.txtSubCategry.text = item
                for dict in self.arrSubData{
                    let dictData = dict as! NSDictionary
                    if item == dictData.value(forKey: "SubCategory") as! String{
                        
                        self.subCatID = dictData.value(forKey: "SubCategoryId") as! NSInteger
                    }
    
                }
            }
            self.txtSubCategry.text = self.ddSubCategory.selectedItem
                ddSubCategory.show()
            }
        }
         if textField == self.txtSelectDistance{
            self.ddDistance.show()
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        if textField  == txtManPower
        {
            txtSubCategry.text = ""
        }
        ddTechnician.hide()
        ddCategory.hide()
        ddSubCategory.hide()
        ddDistance.hide()
    }
    
   
    
    
    
    //MARK: - IBActions
    @IBAction func btnSearchClick(_ sender: Any)
    {
        txtManPower.resignFirstResponder()
        txtSubCategry.resignFirstResponder()
        txtTechnicianType.resignFirstResponder()
        txtSelectDistance.resignFirstResponder()
        
        self.getDateTime()
        if txtTechnicianType.text == ""
        {
            AlertControl.alert(appmassage: "Please select techinician type", view: self)
        }
        else if txtManPower.text == ""
        {
            AlertControl.alert(appmassage: "Please select a category", view: self)
        }
        else if txtSelectDistance.text == ""
        {
            AlertControl.alert(appmassage: "Please select distance", view: self)
        }
        else if txtSubCategry.text == ""{
            AlertControl.alert(appmassage: "Please select a subcategory", view: self)
        }
        else
        {
           self.technicianRequest()
        }
        
        
    }
    
    func getDateTime(){
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        let timeFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MM-dd-yyyy"
        timeFormatter.dateFormat = "hh:mm:ss"
        let convertedDate: String = dateFormatter.string(from: currentDate)
        let timeStr: String = timeFormatter.string(from: currentDate)
       
        self.currentTime = timeStr
        self.currentDate = convertedDate
    }
    
    @IBAction func btnMapViewClick(_ sender: UIButton)
    {
        self.tableViewList.isHidden = true
        self.mapV.isHidden = false
        sender.backgroundColor = UIColor.init(red: 39.0/255.0, green: 26.0/255.0, blue: 117.0/255.0, alpha: 1.0)
        self.btnMap.setImage(UIImage.init(named: "location_white"), for: .normal)
        sender.setTitleColor(UIColor.white, for: .normal)
        sender.isSelected = true
        self.btnList.isSelected = false
        self.btnList.backgroundColor = UIColor.white
        self.btnList.setTitleColor(UIColor.init(red: 39.0/255.0, green: 26.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
    }
    
    @IBAction func btnListViewClick(_ sender: UIButton)
    {
        self.tableViewList.isHidden = false
        self.mapV.isHidden = true
        sender.backgroundColor = UIColor.init(red: 39.0/255.0, green: 26.0/255.0, blue: 117.0/255.0, alpha: 1.0)
        sender.setTitleColor(UIColor.white, for: .normal)
        sender.isSelected = true
        self.btnMap.isSelected = false
        self.btnMap.backgroundColor = UIColor.white
        self.btnMap.setTitleColor(UIColor.init(red: 39.0/255.0, green: 26.0/255.0, blue: 117.0/255.0, alpha: 1.0), for: .normal)
        self.btnMap.setImage(UIImage.init(named: "location_blue"), for: .normal)
        self.tableViewList.reloadData()
    }
    
    
    @IBAction func btnNotificationClick(_ sender: Any)
    {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
            as! NotificationViewController
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TechenicianTableViewCell") as! TechenicianTableViewCell
        let objects =  self.arrListData[indexPath.row] as! CustomerRequest
        
        cell.lblName.text = objects.name as String
        cell.rating.rating = Double(objects.rating as NSNumber)
        
        let x = Double(objects.distance as String)
        let y = Double(round(100*x!)/100)
        cell.lblDistance.text = String(y) + " miles away"
        
        if objects.imageStr != ""
        {
            let placeholderImage = UIImage(named: "user-1")!
            let profileUrl = NSURL(string: objects.imageStr as String)
            if profileUrl != nil
            {
                cell.imgUser.af_setImage(withURL: profileUrl! as URL, placeholderImage: placeholderImage)
            }
            else
            {
                cell.imgUser.image = placeholderImage
            }
        }
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(viewTechnician(_:)), for: .touchUpInside)
        cell.btnBook.tag = indexPath.row
        cell.btnBook.addTarget(self, action: #selector(bookTechnician(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let objects = self.arrListData.object(at: indexPath.row) as! CustomerRequest
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "TechnicianDetailsViewController") as! TechnicianDetailsViewController
        
        controller.techDetails = objects.requestDict
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func methodOfReceivedNotification(notification: Notification){

        paymentDone()
        //api/customer/paymentSuccsefull
//        {
//            "RequestId":"1351",
//            "Amount":"1366",
//            "TransactionId":"122sdfsadf",
//            "TransactionTime":"2018-05-05 12:20"
//        }
    }
    
    func paymentDone()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            let param = NSMutableDictionary()
            param.setValue(requestIdForPayment, forKey: "RequestId")
            param.setValue(ammountForPayment, forKey: "Amount")
            param.setValue(transactionId, forKey: "TransactionId")
            param.setValue(transactionTime, forKey: "TransactionTime")
            
            let tokenUrl = ServiceUrl.customerUrl + "paymentSuccsefull"
            let auth = "bearer " + String(Shared.sharedInstance.token_Data.accesstoken)
            VTAlmofireHelper.sharedInstance.webHelper(method: "POST",jsonRequest: param ,  url: tokenUrl, authentication : auth){
                
                (response:Dictionary,success:Bool) in
                print(response)
                
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if(success){
                    if (response["Code"] as AnyObject).integerValue == 200{
                       AlertControl.alert(appmassage:( response["Message"] as! String), view: self)
                }
                    else{
                        AlertControl.alert(appmassage:( response["Message"] as! String), view: self)

                    }
                }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
            
}
        
        
        
    }
}
