//
//  forgotViewController.swift
//  ZAPCO
//
//  Created by vinove on 9/14/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

class forgotViewController: UIViewController,MoveToControllerDelegate, UITextFieldDelegate
{
    
    //MARK: - Variables
    @IBOutlet weak var txtEmail: UITextField!
    var userType = String()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        txtEmail.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - UITextfieldDelegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        UIView.animate(withDuration: 0.2, delay: 0.2, options: UIViewAnimationOptions.transitionCurlUp, animations: { () -> Void in
            self.view.frame.origin.y -= 150.0
        }, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.2, delay: 0.2, options: UIViewAnimationOptions.transitionCurlDown, animations: { () -> Void in
            self.view.frame.origin.y += 150.0
        }, completion: nil)
    }
    
    
    
    //MARK: - IBActions
    @IBAction func btnBack(_ sender: Any)
    { self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSubmit(_ sender: Any)
    {
        if txtEmail.text == ""
        {
             txtEmail.becomeFirstResponder()
            AlertControl.alert(appmassage: "Please Enter Your Registered Email", view: self)
        }
        else if isValidEmail(testStr: txtEmail.text!)  == false
        {
            txtEmail.becomeFirstResponder()
            AlertControl.alert(appmassage: "Invalid Email", view: self)
        }
        else
        {
            forgotPassword()
        }
    }
    
    //MARK: - EmailValidations
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func getPopup()
    {
        let window = UIApplication.shared.keyWindow
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let viewPopUp = forgotPopup(frame: frame)
        viewPopUp.backgroundColor = UIColor.clear
        viewPopUp.delegate = self
        self.view.addSubview(viewPopUp)
        window?.addSubview(viewPopUp)
    }
    
    
    func moveController()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getPasswordAgain()
    {
        forgotPassword()
    }
    
    
    func forgotPassword()
    {
        self.view.endEditing(true)
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
        ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        //{"grant_Type":"password", "username":"", "password":""}
        let param = NSMutableDictionary()
        param.setValue(self.userType, forKey: "userType")
        param.setValue(self.txtEmail.text, forKey: "email")
        let tokenUrl = ServiceUrl.CommonUrl + "forgotPassword"
        
        VTAlmofireHelper.sharedInstance.post(with: URL(string:tokenUrl)!, method: "POST", params: param as? Dictionary<String, Any>, complition: { (response, error) -> (Void) in
            print(response ?? 00)
           
            if let responseDict:NSDictionary = response!["Response"] as? NSDictionary
            {
                if (responseDict["StatusCode"] as AnyObject).integerValue == 200
            {
                DispatchQueue.main.sync
                    {
                        ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                        let window = UIApplication.shared.keyWindow
                        let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                        let viewPopUp = forgotPopup(frame: frame)
                        viewPopUp.backgroundColor = UIColor.clear
                        viewPopUp.delegate = self
                        // self.view.addSubview(viewPopUp)
                        window?.addSubview(viewPopUp)
                        
                }
            }
            else
            {
                DispatchQueue.main.sync
                    {
                        ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                        AlertControl.alert(appmassage: "Email Id does not Exist", view: self)
                  }
                
            }
            
            }
            else
            {
                AlertControl.alert(appmassage: "Try again later!", view: self)
            }
        })
    }
        else
        {
            AlertControl.alert(appmassage: "No Internet Connection!", view: self)
        }
    }
}


