//
//  forgotPopup.swift
//  ZAPCO
//
//  Created by vinove on 10/13/17.
//  Copyright © 2017 Ankitme.brianm.BPM. All rights reserved.
//

import UIKit

protocol MoveToControllerDelegate: class {
    func moveController()
    func getPasswordAgain()
}

class forgotPopup: UIView {
     weak var delegate: MoveToControllerDelegate?
    @IBOutlet weak var viewForgotPopup: UIView!
    var nibView: UIView!
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
        
       // self.viewForgotPopup.layer.cornerRadius = 8.0
        //self.viewForgotPopup.layer.masksToBounds = true
        //       var nibContents = Bundle.main.loadNibNamed("Sort", owner: self, options: nil)
        //       self.addSubview(nibContents?[0] as! UIView)
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(viewTapped))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func instanceFromNib() -> UIView {
        return UINib(nibName: "forgotPopUp", bundle: Bundle.main).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    func setup() {
        nibView = self.instanceFromNib()
        nibView.frame = bounds
        nibView.autoresizingMask = [UIViewAutoresizing.flexibleWidth ,UIViewAutoresizing.flexibleHeight]
        //nibView.backgroundColor = UIColor.clear
        addSubview(nibView)
    }
    
    func viewTapped(){
        self.removeFromSuperview()
    }
    @IBAction func btnGologinClick(_ sender: Any) {
        self.removeFromSuperview()
        delegate?.moveController()
       
    }
    @IBAction func btnClickHere(_ sender: Any)
    {
        self.removeFromSuperview()
        delegate?.getPasswordAgain()
    }
    
}
